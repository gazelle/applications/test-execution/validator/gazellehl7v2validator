--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_home_id_seq OWNER TO gazelle;

--
-- Name: hl7_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hl7_message (
    id integer NOT NULL,
    caller_ip character varying(255),
    profile_oid character varying(255),
    test_result character varying(255),
    validation_date timestamp without time zone
);


ALTER TABLE hl7_message OWNER TO gazelle;

--
-- Name: hl7_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hl7_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hl7_message_id_seq OWNER TO gazelle;

--
-- Name: hl7_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hl7_profile (
    id integer NOT NULL,
    path varchar(255),
    java_package character varying(255),
    last_changed timestamp without time zone,
    last_modifier character varying(255),
    oid character varying(255) NOT NULL,
    imported_with_errors boolean,
    conformance_profile_format varchar(255),
    revision character varying(255)
);


ALTER TABLE hl7_profile OWNER TO gazelle;

--
-- Name: hl7_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hl7_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hl7_profile_id_seq OWNER TO gazelle;

--
-- Name: hl7_profile_resource; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hl7_profile_resource (
    resource_id integer NOT NULL,
    profile_id integer NOT NULL
);


ALTER TABLE hl7_profile_resource OWNER TO gazelle;

--
-- Name: hl7_resource; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hl7_resource (
    id integer NOT NULL,
    comment character varying(255),
    hl7_version character varying(255),
    path varchar(255),
    last_changed timestamp without time zone,
    last_modifier character varying(255),
    oid character varying(255) NOT NULL,
    weight integer,
    imported_with_errors boolean,
    revision character varying(255)
);


ALTER TABLE hl7_resource OWNER TO gazelle;

--
-- Name: hl7_resource_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hl7_resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hl7_resource_id_seq OWNER TO gazelle;

--
-- Name: hl7v3_validator_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE hl7v3_validator_usage (
    id integer NOT NULL,
    caller character varying(255),
    status character varying(255),
    type character varying(255),
    validation_date date
);


ALTER TABLE hl7v3_validator_usage OWNER TO gazelle;

--
-- Name: hl7v3_validator_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hl7v3_validator_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hl7v3_validator_usage_id_seq OWNER TO gazelle;

--
-- Name: ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE ip_address (
    id integer NOT NULL,
    ipaddress character varying(255)
);


ALTER TABLE ip_address OWNER TO gazelle;

--
-- Name: ip_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE ip_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ip_address_id_seq OWNER TO gazelle;

--
-- Name: mbv_assertion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_assertion (
    id integer NOT NULL,
    assertionid character varying(255),
    idscheme character varying(255),
    constraint_id integer
);


ALTER TABLE mbv_assertion OWNER TO gazelle;

--
-- Name: mbv_assertion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_assertion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_assertion_id_seq OWNER TO gazelle;

--
-- Name: mbv_class_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_class_type (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    name character varying(255),
    parent_identifier character varying(255),
    xml_name character varying(255),
    constraintadvanced text,
    path character varying(255),
    templateid character varying(255),
    documentation_spec_id integer,
    package_id integer
);


ALTER TABLE mbv_class_type OWNER TO gazelle;

--
-- Name: mbv_class_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_class_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_class_type_id_seq OWNER TO gazelle;

--
-- Name: mbv_constraint; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_constraint (
    id integer NOT NULL,
    description text,
    name character varying(255),
    ocl text,
    svsref bytea,
    type character varying(255),
    classtype_id integer,
    author character varying(255),
    datecreation character varying(255),
    history text,
    lastchange character varying(255),
    kind character varying(255)
);


ALTER TABLE mbv_constraint OWNER TO gazelle;

--
-- Name: mbv_constraint_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_constraint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_constraint_id_seq OWNER TO gazelle;

--
-- Name: mbv_documentation_spec; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_documentation_spec (
    id integer NOT NULL,
    description text,
    document character varying(255),
    name character varying(255),
    paragraph character varying(255)
);


ALTER TABLE mbv_documentation_spec OWNER TO gazelle;

--
-- Name: mbv_documentation_spec_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_documentation_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_documentation_spec_id_seq OWNER TO gazelle;

--
-- Name: mbv_package; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_package (
    id integer NOT NULL,
    description character varying(255),
    name character varying(255),
    package_name character varying(255),
    namespace character varying(255)
);


ALTER TABLE mbv_package OWNER TO gazelle;

--
-- Name: mbv_package_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE mbv_package_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mbv_package_id_seq OWNER TO gazelle;

--
-- Name: mbv_standards; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE mbv_standards (
    package_id integer NOT NULL,
    standards character varying(255)
);


ALTER TABLE mbv_standards OWNER TO gazelle;

--
-- Name: oid_generator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE oid_generator (
    id integer NOT NULL,
    next_value integer,
    root character varying(255),
    target character varying(255)
);


ALTER TABLE oid_generator OWNER TO gazelle;

--
-- Name: oid_generator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE oid_generator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oid_generator_id_seq OWNER TO gazelle;

--
-- Name: user_preferences; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE user_preferences (
    id integer NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE user_preferences OWNER TO gazelle;

--
-- Name: user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_preferences_id_seq OWNER TO gazelle;

--
-- Name: user_preferences_ip_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE user_preferences_ip_address (
    user_preferences_id integer NOT NULL,
    allowedipaddresses_id integer NOT NULL
);


ALTER TABLE user_preferences_ip_address OWNER TO gazelle;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_iso3_language_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_iso3_language_key UNIQUE (iso3_language);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);



--
-- Name: hl7_message hl7_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_message
    ADD CONSTRAINT hl7_message_pkey PRIMARY KEY (id);


--
-- Name: hl7_profile hl7_profile_oid_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile
    ADD CONSTRAINT hl7_profile_oid_key UNIQUE (oid);


--
-- Name: hl7_profile hl7_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile
    ADD CONSTRAINT hl7_profile_pkey PRIMARY KEY (id);


--
-- Name: hl7_profile_resource hl7_profile_resource_resource_id_profile_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile_resource
    ADD CONSTRAINT hl7_profile_resource_resource_id_profile_id_key UNIQUE (resource_id, profile_id);


--
-- Name: hl7_resource hl7_resource_oid_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_resource
    ADD CONSTRAINT hl7_resource_oid_key UNIQUE (oid);


--
-- Name: hl7_resource hl7_resource_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_resource
    ADD CONSTRAINT hl7_resource_pkey PRIMARY KEY (id);


--
-- Name: hl7v3_validator_usage hl7v3_validator_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7v3_validator_usage
    ADD CONSTRAINT hl7v3_validator_usage_pkey PRIMARY KEY (id);


--
-- Name: ip_address ip_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY ip_address
    ADD CONSTRAINT ip_address_pkey PRIMARY KEY (id);


--
-- Name: mbv_assertion mbv_assertion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT mbv_assertion_pkey PRIMARY KEY (id);


--
-- Name: mbv_class_type mbv_class_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT mbv_class_type_pkey PRIMARY KEY (id);


--
-- Name: mbv_constraint mbv_constraint_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT mbv_constraint_pkey PRIMARY KEY (id);


--
-- Name: mbv_documentation_spec mbv_documentation_spec_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_documentation_spec
    ADD CONSTRAINT mbv_documentation_spec_pkey PRIMARY KEY (id);


--
-- Name: mbv_package mbv_package_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_package
    ADD CONSTRAINT mbv_package_pkey PRIMARY KEY (id);


--
-- Name: oid_generator oid_generator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY oid_generator
    ADD CONSTRAINT oid_generator_pkey PRIMARY KEY (id);


--
-- Name: oid_generator oid_generator_target_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY oid_generator
    ADD CONSTRAINT oid_generator_target_key UNIQUE (target);


--
-- Name: user_preferences_ip_address uk_1sptn5bviirl246e6e100ktfw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences_ip_address
    ADD CONSTRAINT uk_1sptn5bviirl246e6e100ktfw UNIQUE (allowedipaddresses_id);


--
-- Name: hl7_resource uk_9euaj73lqta27faruvx5xx2k2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_resource
    ADD CONSTRAINT uk_9euaj73lqta27faruvx5xx2k2 UNIQUE (oid);


--
-- Name: hl7_profile_resource uk_dph90y8w8qg66ox2n7g58hy9y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile_resource
    ADD CONSTRAINT uk_dph90y8w8qg66ox2n7g58hy9y UNIQUE (resource_id, profile_id);


--
-- Name: oid_generator uk_g6hcghamc1dxp299xgepe055b; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY oid_generator
    ADD CONSTRAINT uk_g6hcghamc1dxp299xgepe055b UNIQUE (target);


--
-- Name: hl7_profile uk_ghie2j4ofel2dbde84cvkgnvl; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile
    ADD CONSTRAINT uk_ghie2j4ofel2dbde84cvkgnvl UNIQUE (oid);



--
-- Name: user_preferences uk_hixso8fayobhdpi7mh6a0ln74; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences
    ADD CONSTRAINT uk_hixso8fayobhdpi7mh6a0ln74 UNIQUE (username);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: user_preferences_ip_address user_preferences_ip_address_allowedipaddresses_id_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences_ip_address
    ADD CONSTRAINT user_preferences_ip_address_allowedipaddresses_id_key UNIQUE (allowedipaddresses_id);


--
-- Name: user_preferences user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (id);


--
-- Name: user_preferences user_preferences_username_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences
    ADD CONSTRAINT user_preferences_username_key UNIQUE (username);


--
-- Name: mbv_assertion fk16ce89c47778d25e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_assertion
    ADD CONSTRAINT fk16ce89c47778d25e FOREIGN KEY (constraint_id) REFERENCES mbv_constraint(id);


--
-- Name: mbv_class_type fk350aaf1f8e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f8e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: mbv_class_type fk350aaf1f9ba1deeb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_class_type
    ADD CONSTRAINT fk350aaf1f9ba1deeb FOREIGN KEY (documentation_spec_id) REFERENCES mbv_documentation_spec(id);


--
-- Name: mbv_constraint fk3afefb5bc668fe56; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_constraint
    ADD CONSTRAINT fk3afefb5bc668fe56 FOREIGN KEY (classtype_id) REFERENCES mbv_class_type(id);


--
-- Name: mbv_standards fk41dd2c188e9bb0b6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY mbv_standards
    ADD CONSTRAINT fk41dd2c188e9bb0b6 FOREIGN KEY (package_id) REFERENCES mbv_package(id);


--
-- Name: user_preferences_ip_address fk4e910377b3eacf60; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences_ip_address
    ADD CONSTRAINT fk4e910377b3eacf60 FOREIGN KEY (user_preferences_id) REFERENCES user_preferences(id);


--
-- Name: user_preferences_ip_address fk4e910377ff1d3251; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY user_preferences_ip_address
    ADD CONSTRAINT fk4e910377ff1d3251 FOREIGN KEY (allowedipaddresses_id) REFERENCES ip_address(id);


--
-- Name: hl7_message fk_h0r3uoellmv2chu04t88nyuie; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

--ALTER TABLE ONLY hl7_message
--    ADD CONSTRAINT fk_h0r3uoellmv2chu04t88nyuie FOREIGN KEY (detailedresults_id) REFERENCES hl7_message(id);


--
-- Name: hl7_profile_resource fkfd4984f06e7e8e1e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile_resource
    ADD CONSTRAINT fkfd4984f06e7e8e1e FOREIGN KEY (resource_id) REFERENCES hl7_resource(id);


--
-- Name: hl7_profile_resource fkfd4984f07c21f176; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY hl7_profile_resource
    ADD CONSTRAINT fkfd4984f07c21f176 FOREIGN KEY (profile_id) REFERENCES hl7_profile(id);


--
-- PostgreSQL database dump complete
--

