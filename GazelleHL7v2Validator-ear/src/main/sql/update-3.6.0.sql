/* Change in HL7Message
drop the columns which are not bound to attributes anymore*/

ALTER TABLE hl7_message DROP COLUMN message;
ALTER TABLE hl7_message DROP COLUMN oid;
ALTER TABLE hl7_message DROP COLUMN hl7_version;
ALTER TABLE hl7_message DROP COLUMN validation_context;
ALTER TABLE hl7_message DROP COLUMN metadata;
ALTER TABLE hl7_message DROP COLUMN encoding;
ALTER TABLE hl7_message DROP COLUMN raises_profile_exceptions;
ALTER TABLE hl7_message DROP COLUMN detailed_results;
ALTER TABLE hl7_message DROP COLUMN detailedresults_id;

/* Read HL7 message profile / resources from the file system */

ALTER TABLE hl7_profile DROP COLUMN content;
ALTER TABLE hl7_resource DROP COLUMN content;
ALTER TABLE hl7_profile ADD path varchar(255);
ALTER TABLE hl7_resource ADD path varchar(255);
ALTER TABLE hl7_profile ADD conformance_profile_format varchar(255);

/* Distinguish the format of message profiles
   Command to set the default value for all entries*/

UPDATE hl7_profile SET conformance_profile_format = 'HL7v2xConformanceProfile';
UPDATE hl7_profile SET path = '/HL7MessageProfiles/ProfilesPerOid/'|| oid || '.xml';

UPDATE hl7_resource SET path = '/HL7Tables/TablesPerOid/'|| oid || '.xml';

/*create and modify preference*/

UPDATE app_configuration SET value = '/HL7MessageProfiles/ProfilesPerOid' where variable = 'hl7_profiles_directory';
UPDATE app_configuration SET value = '/HL7Tables/TablesPerOid' where variable = 'hl7_table_directory';


INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), '/HL7IgamtProfiles/ProfilesPerOid', 'IGAMT_directory');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), '', 'hl7_directory');
INSERT INTO app_configuration(id, value, variable) VALUES (nextval('app_configuration_id_seq'), '', 'jar_path');
