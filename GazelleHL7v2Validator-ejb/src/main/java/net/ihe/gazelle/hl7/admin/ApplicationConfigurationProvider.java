package net.ihe.gazelle.hl7.admin;

import java.io.Serializable;

import javax.ejb.Remove;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to store some of the preferences in the application state instead of querying several times the database
 * 
 * @author aberge
 * 
 */

@AutoCreate
@Name("applicationConfigurationProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationProviderLocal")
public class ApplicationConfigurationProvider implements Serializable, ApplicationConfigurationProviderLocal {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5004043953931007769L;

	private Boolean applicationWorksWithoutCas;
	private Boolean ipLogin;
	private String ipLoginAdmin;
	private String applicationUrl;
	@Override
	@Create
	public void init() {
		applicationWorksWithoutCas = null;
		ipLogin = null;
		ipLoginAdmin = null;
		applicationUrl = null;
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
		init();
	}

	public static ApplicationConfigurationProviderLocal instance() {
		return (ApplicationConfigurationProviderLocal) Component.getInstance("applicationConfigurationProvider");
	}

	@Override
	public String loginByIP() {
		Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
		if ("loggedIn".equals(identity.login())) {
			return "/home.xhtml";
		} else {
			return null;
		}
	}

	@Override
	public Boolean getIpLogin() {
		if (ipLogin == null) {
			String valueAsString = ApplicationConfiguration.getValueOfVariable("ip_login");
			if (valueAsString == null) {
				ipLogin = false;
			} else {
				ipLogin = Boolean.valueOf(valueAsString);
			}
		}
		return ipLogin;
	}

	@Override
	public String getIpLoginAdmin() {
		if (ipLoginAdmin == null) {
			ipLoginAdmin = ApplicationConfiguration.getValueOfVariable("ip_login_admin");
		}
		return ipLoginAdmin;
	}

	@Override
	public String getApplicationUrl() {
		if (applicationUrl == null) {
			applicationUrl = ApplicationConfiguration.getValueOfVariable("application_url");
		}
		return applicationUrl;
	}

	@Override
	public boolean isUserAllowedAsAdmin() {
		return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
	}
}
