package net.ihe.gazelle.hl7.admin;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.model.SelectItem;

import net.ihe.gazelle.hl7.utils.HQLQueryBuilderForStatistics;
import net.ihe.gazelle.hl7.utils.HQLRestrictionNotLike;
import net.ihe.gazelle.hl7v3.validator.core.HL7V3ValidatorType;
import net.ihe.gazelle.hl7v3.validator.model.ValidatorUsage;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.international.LocaleSelector;

@Name("hl7v3StatisticsManager")
@Scope(ScopeType.PAGE)
public class HL7v3ValidationStatistics implements Serializable{

	public static final int ALL = 1;
	public static final int INCLUDE = 2;
	public static final int EXCLUDE = 3;
	public static final int YEAR = 1;
	public static final int MONTH = 2;

	private Date startDate;
	private Date endDate;
	private HQLQueryBuilderForStatistics<ValidatorUsage> queryBuilder;
	private int ipFilter;
	private String ipStartsWith;
	private int selectedInterval;
	private String selectedvalidator;

	@Create
	public void reset() {
		startDate = null;
		endDate = null;
		ipFilter = ALL;
		ipStartsWith = null;
		selectedInterval = YEAR;
		selectedvalidator = null;
		setQueryBuilder(true);
	}

	public void setQueryBuilder(boolean setRestrictionsOnDate) {
		queryBuilder = new HQLQueryBuilderForStatistics<ValidatorUsage>(ValidatorUsage.class);
		if (setRestrictionsOnDate && (startDate != null)) {
			queryBuilder.addRestriction(HQLRestrictions.ge("validationDate", startDate));
		}
		if (setRestrictionsOnDate && (endDate != null)) {
			queryBuilder.addRestriction(HQLRestrictions.le("validationDate", endDate));
		}
		if ((ipStartsWith != null) && !ipStartsWith.isEmpty()) {
			switch (ipFilter) {
			case INCLUDE:
				queryBuilder.addRestriction(HQLRestrictions.like("caller", ipStartsWith,
						HQLRestrictionLikeMatchMode.START));
				break;
			case EXCLUDE:
				queryBuilder.addRestriction(new HQLRestrictionNotLike("caller", ipStartsWith,
						HQLRestrictionLikeMatchMode.START));
				break;
			default:
			}
		}
		if (selectedvalidator != null) {
			queryBuilder.addRestriction(HQLRestrictions.eq("type", selectedvalidator));
		}
	}

	public List<Object[]> getResultsPerValidator() {
		List<Object[]> stats = queryBuilder.getStatistics("type");
		Collections.sort(stats, STATISTICS_COMPARATOR);
		return stats;
	}

	public List<Object[]> getStatisticsPerResultStatus() {
		List<Object[]> stats = queryBuilder.getStatistics("status");

		Object[] passed = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED.getStatus(), 0 };
		Object[] failed = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.FAILED.getStatus(), 0 };

		List<Object[]> statsWithAllStatus = new ArrayList<Object[]>();
		// if stats were returned for a status, take them
		for (Object[] object : stats) {
			String label = (String) object[0];
			if (label == null) {
				continue;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.FAILED.getStatus())) {
				failed = object;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED.getStatus())) {
				passed = object;
			}
		}
		statsWithAllStatus.add(passed);
		statsWithAllStatus.add(failed);

		Collections.sort(statsWithAllStatus, PIE_LABEL_COMPARATOR);
		return statsWithAllStatus;
	}

	public List<Object[]> getStatisticsPerCallerIp() {
		List<Object[]> stats = queryBuilder.getStatistics("caller");
		Collections.sort(stats, STATISTICS_COMPARATOR);
		return stats;
	}

	public SelectItem[] getValidators() {
		List<HL7V3ValidatorType> list = Arrays.asList(HL7V3ValidatorType.values());
		List<SelectItem> result = new ArrayList<SelectItem>();
		for (HL7V3ValidatorType validator : list) {
			result.add(new SelectItem(validator, validator.getLabel()));
		}
		return result.toArray(new SelectItem[result.size()]);
	}

	public List<Object[]> getStatisticsPerDate(int interval) {
		// if the startDate attribute is not valued, retrieve the date of the oldest validation for the given criteria
		if (startDate == null) {
			startDate = queryBuilder.getMinDate();
		}
		// if the endDate attribute is not values, set it as today
		if (endDate == null) {
			endDate = new Date();
		}
		// recreate a query builder without the restrictions on dates
		setQueryBuilder(false);

		List<Object[]> statistics = new ArrayList<Object[]>();

		SimpleDateFormat formatYear = new SimpleDateFormat("yyyy", LocaleSelector.instance().getLocale());
		SimpleDateFormat formatMonthYear = new SimpleDateFormat("MMM yy", LocaleSelector.instance().getLocale());

		Locale locale = LocaleSelector.instance().getLocale();

		Calendar startDateAsCalendar = Calendar.getInstance(locale);
		Calendar endDateAsCalendar = Calendar.getInstance(locale);
		Calendar intervalBegin = Calendar.getInstance(locale);
		Calendar intervalEnd = Calendar.getInstance(locale);

		// set start date with the startdate at midnight
		startDateAsCalendar.setTime(startDate);
		startDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
		startDateAsCalendar.set(Calendar.MINUTE, 0);
		startDateAsCalendar.set(Calendar.SECOND, 0);

		// set end date with the following date at midnight
		endDateAsCalendar.setTime(endDate);
		endDateAsCalendar.add(Calendar.DAY_OF_YEAR, +1);
		endDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
		endDateAsCalendar.set(Calendar.MINUTE, 0);
		endDateAsCalendar.set(Calendar.SECOND, 0);

		intervalBegin.set(startDateAsCalendar.get(Calendar.YEAR), startDateAsCalendar.get(Calendar.MONTH),
				startDateAsCalendar.get(Calendar.DAY_OF_MONTH), 0, 0);

		// interval size is month
		if (interval == MONTH) {
			intervalBegin.set(Calendar.DAY_OF_MONTH, 1);
			// first interval begining of the firstMonth at the end of the current month
			intervalEnd.set(intervalBegin.get(Calendar.YEAR), intervalBegin.get(Calendar.MONTH),
					intervalBegin.get(Calendar.DAY_OF_MONTH), 0, 0);
			if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
				intervalEnd.add(Calendar.YEAR, 1);
				intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
			} else {
				intervalEnd.add(Calendar.MONTH, 1);
			}
		}

		else if (interval == YEAR) {
			// first interval ends at the end of the current year
			intervalEnd.set(Calendar.DAY_OF_YEAR, 1);
			intervalEnd.set(Calendar.YEAR, startDateAsCalendar.get(Calendar.YEAR) + 1);
			intervalEnd.set(Calendar.HOUR_OF_DAY, 0);
			intervalEnd.set(Calendar.MINUTE, 0);
			intervalEnd.set(Calendar.SECOND, 0);
		}

		while (intervalBegin.before(endDateAsCalendar)) {
			setQueryBuilder(false);
			queryBuilder.addRestriction(HQLRestrictions.and(
					HQLRestrictions.ge("validationDate", intervalBegin.getTime()),
					HQLRestrictions.lt("validationDate", intervalEnd.getTime())));
			String dateLabel = null;
			if (interval == MONTH) {
				dateLabel = formatMonthYear.format(intervalBegin.getTime());
			} else {
				dateLabel = formatYear.format(intervalBegin.getTime());
			}
			Object[] stat = { dateLabel, queryBuilder.getCount() };
			statistics.add(stat);

			intervalBegin.set(intervalEnd.get(Calendar.YEAR), intervalEnd.get(Calendar.MONTH),
					intervalEnd.get(Calendar.DATE), intervalEnd.get(Calendar.HOUR_OF_DAY),
					intervalEnd.get(Calendar.MINUTE), intervalEnd.get(Calendar.SECOND));

			if (interval == MONTH) {
				if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
					intervalEnd.add(Calendar.YEAR, 1);
					intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
				} else {
					intervalEnd.add(Calendar.MONTH, 1);
				}
			} else {
				intervalEnd.add(Calendar.YEAR, 1);
			}
		}

		return statistics;
	}

	/**
	 * This comparator is used to sort the statistics by decreasing result
	 */
	static final Comparator<Object[]> STATISTICS_COMPARATOR = new Comparator<Object[]>() {

		@Override
		public int compare(Object[] o1, Object[] o2) {
			return (((Number) o2[1]).intValue() - ((Number) o1[1]).intValue());
		}

	};

	static final Comparator<Object[]> PIE_LABEL_COMPARATOR = new Comparator<Object[]>() {

		@Override
		public int compare(Object[] o1, Object[] o2) {
			String label1 = (String) o1[0];
			String label2 = (String) o2[0];
			if (label1 == null) {
				return 1;
			} else if (label2 == null) {
				return -1;
			} else {
				return label1.compareTo(label2);
			}
		}
	};

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public HQLQueryBuilderForStatistics<ValidatorUsage> getQueryBuilder() {
		return queryBuilder;
	}

	public void setQueryBuilder(HQLQueryBuilderForStatistics<ValidatorUsage> queryBuilder) {
		this.queryBuilder = queryBuilder;
	}

	public int getIpFilter() {
		return ipFilter;
	}

	public void setIpFilter(int ipFilter) {
		this.ipFilter = ipFilter;
	}

	public String getIpStartsWith() {
		return ipStartsWith;
	}

	public void setIpStartsWith(String ipStartsWith) {
		this.ipStartsWith = ipStartsWith;
	}

	public int getSelectedInterval() {
		return selectedInterval;
	}

	public void setSelectedInterval(int selectedInterval) {
		this.selectedInterval = selectedInterval;
	}

	public String getSelectedvalidator() {
		return selectedvalidator;
	}

	public void setSelectedvalidator(String selectedvalidator) {
		this.selectedvalidator = selectedvalidator;
	}

	public int getALL() {
		return ALL;
	}

	public int getINCLUDE() {
		return INCLUDE;
	}

	public int getEXCLUDE() {
		return EXCLUDE;
	}

	public int getYEAR() {
		return YEAR;
	}

	public int getMONTH() {
		return MONTH;
	}
}
