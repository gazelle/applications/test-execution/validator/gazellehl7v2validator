package net.ihe.gazelle.hl7.admin;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.utils.HQLQueryBuilderForStatistics;
import net.ihe.gazelle.hl7.utils.HQLRestrictionNotLike;
import net.ihe.gazelle.hl7.validator.model.ApplicationConfiguration;
import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.tf.messageprofile.Hl7MessageProfilesServiceStub;
import net.ihe.gazelle.tf.messageprofile.SOAPExceptionException;
import net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext;
import net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContextE;
import net.ihe.gazelle.tf.ws.Hl7MessageProfile;

import org.apache.axis2.AxisFault;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>StatisticsManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("statisticsManager")
@Scope(ScopeType.PAGE)
public class StatisticsManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2763978561274362828L;

	private static Logger log = LoggerFactory.getLogger(StatisticsManager.class);

	/**
	 * 
	 */
	public static final int ALL = 1;
	/** Constant <code>INCLUDE=2</code> */
	public static final int INCLUDE = 2;
	/** Constant <code>EXCLUDE=3</code> */
	public static final int EXCLUDE = 3;
	/** Constant <code>YEAR=1</code> */
	public static final int YEAR = 1;
	/** Constant <code>MONTH=2</code> */
	public static final int MONTH = 2;

	private Date startDate;
	private Date endDate;
	private Hl7MessageProfilesServiceStub stub;
	private GetListOfPossibleHL7MessageProfileForGivenContext params;
	private String profileOID;
	private Hl7MessageProfile[] selectedReferences;
	private String ipStartsWith;
	HQLQueryBuilderForStatistics<HL7v2ValidationLog> queryBuilder;
	private boolean allProfiles;
	private int ipFilter;
	private boolean limitToTop10;
	private int selectedInterval;
	private Integer totalNumberOfProfiles;
	private Integer totalNumberOfProfilesForCriteria;

	/**
	 * <p>reset.</p>
	 */
	@Create
	public void reset() {
		allProfiles = true;
		startDate = null;
		endDate = null;
		if (stub == null) {
			String endpoint = ApplicationConfiguration.getValueOfVariable("gmm_hl7messageprofile_wsdl");
			if (endpoint != null) {
				try {
					stub = new Hl7MessageProfilesServiceStub(endpoint);
					stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
					params = new GetListOfPossibleHL7MessageProfileForGivenContext();
				} catch (AxisFault e) {
					FacesMessages.instance().add(endpoint + "is not reachable");
					log.error(e.getMessage(), e);
					params = null;
				}
			} else {
				FacesMessages.instance().add(
						"gmm_hl7messageprofile_wsdl is not defined in the app_configuration table !");
				params = null;
			}
		}
		totalNumberOfProfiles = new HQLQueryBuilder<Profile>(Profile.class).getCount();
		ipFilter = ALL;
		profileOID = null;
		params = new GetListOfPossibleHL7MessageProfileForGivenContext();
		selectedReferences = null;
		ipStartsWith = null;
		limitToTop10 = false;
		totalNumberOfProfilesForCriteria = 0;
		selectedInterval = YEAR;
		setQueryBuilder(true);
	}

	/**
	 * <p>Setter for the field <code>queryBuilder</code>.</p>
	 *
	 * @param setRestrictionsOnDate a boolean.
	 */
	public void setQueryBuilder(boolean setRestrictionsOnDate) {
		queryBuilder = new HQLQueryBuilderForStatistics<HL7v2ValidationLog>(HL7v2ValidationLog.class);
		if (!allProfiles) {
			List<String> selectedProfiles = new ArrayList<String>();
			if ((profileOID != null) && !profileOID.isEmpty()) {
				selectedProfiles.add(profileOID);
			} else {
				GetListOfPossibleHL7MessageProfileForGivenContextE paramsE = new GetListOfPossibleHL7MessageProfileForGivenContextE();
				paramsE.setGetListOfPossibleHL7MessageProfileForGivenContext(params);
				try {
					selectedReferences = stub.getListOfPossibleHL7MessageProfileForGivenContext(paramsE)
							.getGetListOfPossibleHL7MessageProfileForGivenContextResponse().getReturnedMessageProfile();
					if (selectedReferences.length > 0) {
						for (Hl7MessageProfile reference : selectedReferences) {
							selectedProfiles.add(reference.getOid());
						}
					} else {
						selectedReferences = null;
						selectedProfiles = null;
					}
				} catch (RemoteException e) {
					FacesMessages.instance().add(e.getMessage());
				} catch (SOAPExceptionException e) {
					FacesMessages.instance().add(e.getFaultMessage().getSOAPException().getMessage());
				}
			}
			queryBuilder.addIn("profileOid", selectedProfiles);
		}
		if (setRestrictionsOnDate && (startDate != null)) {
			queryBuilder.addRestriction(HQLRestrictions.ge("validationDate", startDate));
		}
		if (setRestrictionsOnDate && (endDate != null)) {
			queryBuilder.addRestriction(HQLRestrictions.le("validationDate", endDate));
		}
		if ((ipStartsWith != null) && !ipStartsWith.isEmpty()) {
			switch (ipFilter) {
			case INCLUDE:
				queryBuilder.addRestriction(HQLRestrictions.like("callerIP", ipStartsWith,
						HQLRestrictionLikeMatchMode.START));
				break;
			case EXCLUDE:
				queryBuilder.addRestriction(new HQLRestrictionNotLike("callerIP", ipStartsWith,
						HQLRestrictionLikeMatchMode.START));
				break;
			default:
			}
		}
	}

	/**
	 * <p>getResultsPerProfiles.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Object[]> getResultsPerProfiles() {
		List<Object[]> stats = queryBuilder.getStatistics("profileOid");
		Collections.sort(stats, STATISTICS_COMPARATOR);
		totalNumberOfProfilesForCriteria = stats.size();
		if (limitToTop10 && (stats.size() > 9)) {
			return stats.subList(0, 9);
		} else {
			return stats;
		}
	}

	/**
	 * <p>getTotalNumberOfMessagesForCriteria.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getTotalNumberOfMessagesForCriteria() {
		return queryBuilder.getCount();
	}

	/**
	 * <p>getStatisticsPerResultStatus.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Object[]> getStatisticsPerResultStatus() {
		List<Object[]> stats = queryBuilder.getStatistics("testResult");

		Object[] passed = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED.getStatus(), 0 };
		Object[] failed = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.FAILED.getStatus(), 0 };
		Object[] aborted = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.ABORTED.getStatus(), 0 };
		Object[] invalid = { net.ihe.gazelle.hl7.validator.report.ValidationStatus.INVALID_REQUEST.getStatus(), 0 };

		List<Object[]> statsWithAllStatus = new ArrayList<Object[]>();
		// if stats were returned for a status, take them
		for (Object[] object : stats) {
			String label = (String) object[0];
			if (label == null) {
				continue;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.ABORTED.getStatus())) {
				aborted = object;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.FAILED.getStatus())) {
				failed = object;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.INVALID_REQUEST.getStatus())) {
				invalid = object;
			} else if (label.equals(net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED.getStatus())) {
				passed = object;
			}
		}
		statsWithAllStatus.add(passed);
		statsWithAllStatus.add(failed);
		statsWithAllStatus.add(aborted);
		statsWithAllStatus.add(invalid);

		Collections.sort(statsWithAllStatus, PIE_LABEL_COMPARATOR);
		return statsWithAllStatus;
	}

	/**
	 * <p>getStatisticsPerCallerIp.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<Object[]> getStatisticsPerCallerIp() {
		List<Object[]> stats = queryBuilder.getStatistics("callerIP");
		Collections.sort(stats, STATISTICS_COMPARATOR);
		if (limitToTop10 && (stats.size() > 9)) {
			return stats.subList(0, 9);
		} else {
			return stats;
		}
	}

	/**
	 * <p>getStatisticsPerDate.</p>
	 *
	 * @param interval a int.
	 * @return a {@link java.util.List} object.
	 */
	public List<Object[]> getStatisticsPerDate(int interval) {
		new SimpleDateFormat("dd/MM/yyyy");
		// if the startDate attribute is not valued, retrieve the date of the oldest validation for the given criteria
		if (startDate == null) {
			startDate = queryBuilder.getMinDate();
		}
		// if the endDate attribute is not values, set it as today
		if (endDate == null) {
			endDate = new Date();
		}
		// recreate a query builder without the restrictions on dates
		setQueryBuilder(false);

		List<Object[]> statistics = new ArrayList<Object[]>();

		SimpleDateFormat formatYear = new SimpleDateFormat("yyyy", LocaleSelector.instance().getLocale());
		SimpleDateFormat formatMonthYear = new SimpleDateFormat("MMM yy", LocaleSelector.instance().getLocale());

		Locale locale = LocaleSelector.instance().getLocale();

		Calendar startDateAsCalendar = Calendar.getInstance(locale);
		Calendar endDateAsCalendar = Calendar.getInstance(locale);
		Calendar intervalBegin = Calendar.getInstance(locale);
		Calendar intervalEnd = Calendar.getInstance(locale);

		// set start date with the startdate at midnight
		startDateAsCalendar.setTime(startDate);
		startDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
		startDateAsCalendar.set(Calendar.MINUTE, 0);
		startDateAsCalendar.set(Calendar.SECOND, 0);

		// set end date with the following date at midnight
		endDateAsCalendar.setTime(endDate);
		endDateAsCalendar.add(Calendar.DAY_OF_YEAR, +1);
		endDateAsCalendar.set(Calendar.HOUR_OF_DAY, 0);
		endDateAsCalendar.set(Calendar.MINUTE, 0);
		endDateAsCalendar.set(Calendar.SECOND, 0);

		intervalBegin.set(startDateAsCalendar.get(Calendar.YEAR), startDateAsCalendar.get(Calendar.MONTH),
				startDateAsCalendar.get(Calendar.DAY_OF_MONTH), 0, 0);

		// interval size is month
		if (interval == MONTH) {
			intervalBegin.set(Calendar.DAY_OF_MONTH, 1);
			// first interval begining of the firstMonth at the end of the current month
			intervalEnd.set(intervalBegin.get(Calendar.YEAR), intervalBegin.get(Calendar.MONTH),
					intervalBegin.get(Calendar.DAY_OF_MONTH), 0, 0);
			if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
				intervalEnd.add(Calendar.YEAR, 1);
				intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
			} else {
				intervalEnd.add(Calendar.MONTH, 1);
			}
		}

		else if (interval == YEAR) {
			// first interval ends at the end of the current year
			intervalEnd.set(Calendar.DAY_OF_YEAR, 1);
			intervalEnd.set(Calendar.YEAR, startDateAsCalendar.get(Calendar.YEAR) + 1);
			intervalEnd.set(Calendar.HOUR_OF_DAY, 0);
			intervalEnd.set(Calendar.MINUTE, 0);
			intervalEnd.set(Calendar.SECOND, 0);
		}

		while (intervalBegin.before(endDateAsCalendar)) {
			setQueryBuilder(false);
			queryBuilder.addRestriction(HQLRestrictions.and(
					HQLRestrictions.ge("validationDate", intervalBegin.getTime()),
					HQLRestrictions.lt("validationDate", intervalEnd.getTime())));
			String dateLabel = null;
			if (interval == MONTH) {
				dateLabel = formatMonthYear.format(intervalBegin.getTime());
			} else {
				dateLabel = formatYear.format(intervalBegin.getTime());
			}
			Object[] stat = { dateLabel, queryBuilder.getCount() };
			statistics.add(stat);

			intervalBegin.set(intervalEnd.get(Calendar.YEAR), intervalEnd.get(Calendar.MONTH),
					intervalEnd.get(Calendar.DATE), intervalEnd.get(Calendar.HOUR_OF_DAY),
					intervalEnd.get(Calendar.MINUTE), intervalEnd.get(Calendar.SECOND));

			if (interval == MONTH) {
				if (intervalEnd.get(Calendar.MONTH) == Calendar.DECEMBER) {
					intervalEnd.add(Calendar.YEAR, 1);
					intervalEnd.set(Calendar.MONTH, Calendar.JANUARY);
				} else {
					intervalEnd.add(Calendar.MONTH, 1);
				}
			} else {
				intervalEnd.add(Calendar.YEAR, 1);
			}
		}

		return statistics;
	}

	/**
	 * This comparator is used to sort the statistics by decreasing result
	 */
	static final Comparator<Object[]> STATISTICS_COMPARATOR = new Comparator<Object[]>() {

		@Override
		public int compare(Object[] o1, Object[] o2) {
			return (((Number) o2[1]).intValue() - ((Number) o1[1]).intValue());
		}

	};

	static final Comparator<Object[]> PIE_LABEL_COMPARATOR = new Comparator<Object[]>() {

		@Override
		public int compare(Object[] o1, Object[] o2) {
			String label1 = (String) o1[0];
			String label2 = (String) o2[0];
			if (label1 == null) {
				return 1;
			} else if (label2 == null) {
				return -1;
			} else {
				return label1.compareTo(label2);
			}
		}
	};

	/**
	 * GETTERS and SETTERS
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * <p>Setter for the field <code>startDate</code>.</p>
	 *
	 * @param startDate a {@link java.util.Date} object.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * <p>Getter for the field <code>endDate</code>.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * <p>Setter for the field <code>endDate</code>.</p>
	 *
	 * @param endDate a {@link java.util.Date} object.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * <p>Getter for the field <code>params</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
	 */
	public GetListOfPossibleHL7MessageProfileForGivenContext getParams() {
		return params;
	}

	/**
	 * <p>Setter for the field <code>params</code>.</p>
	 *
	 * @param params a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
	 */
	public void setParams(GetListOfPossibleHL7MessageProfileForGivenContext params) {
		this.params = params;
	}

	/**
	 * <p>Getter for the field <code>profileOID</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getProfileOID() {
		return profileOID;
	}

	/**
	 * <p>Setter for the field <code>profileOID</code>.</p>
	 *
	 * @param profileOID a {@link java.lang.String} object.
	 */
	public void setProfileOID(String profileOID) {
		this.profileOID = profileOID;
	}

	/**
	 * <p>Getter for the field <code>selectedReferences</code>.</p>
	 *
	 * @return an array of {@link net.ihe.gazelle.tf.ws.Hl7MessageProfile} objects.
	 */
	public Hl7MessageProfile[] getSelectedReferences() {
		return selectedReferences;
	}

	/**
	 * <p>Setter for the field <code>allProfiles</code>.</p>
	 *
	 * @param allProfiles a boolean.
	 */
	public void setAllProfiles(boolean allProfiles) {
		this.allProfiles = allProfiles;
	}

	/**
	 * <p>isAllProfiles.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isAllProfiles() {
		return allProfiles;
	}

	/**
	 * <p>Setter for the field <code>ipStartsWith</code>.</p>
	 *
	 * @param ipStartsWith a {@link java.lang.String} object.
	 */
	public void setIpStartsWith(String ipStartsWith) {
		this.ipStartsWith = ipStartsWith;
	}

	/**
	 * <p>Getter for the field <code>ipStartsWith</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIpStartsWith() {
		return ipStartsWith;
	}

	/**
	 * <p>Setter for the field <code>ipFilter</code>.</p>
	 *
	 * @param ipFilter a int.
	 */
	public void setIpFilter(int ipFilter) {
		this.ipFilter = ipFilter;
	}

	/**
	 * <p>Getter for the field <code>ipFilter</code>.</p>
	 *
	 * @return a int.
	 */
	public int getIpFilter() {
		return ipFilter;
	}

	/**
	 * <p>getALL.</p>
	 *
	 * @return a int.
	 */
	public int getALL() {
		return ALL;
	}

	/**
	 * <p>getINCLUDE.</p>
	 *
	 * @return a int.
	 */
	public int getINCLUDE() {
		return INCLUDE;
	}

	/**
	 * <p>getEXCLUDE.</p>
	 *
	 * @return a int.
	 */
	public int getEXCLUDE() {
		return EXCLUDE;
	}

	/**
	 * <p>Setter for the field <code>limitToTop10</code>.</p>
	 *
	 * @param limitToTop10 a boolean.
	 */
	public void setLimitToTop10(boolean limitToTop10) {
		this.limitToTop10 = limitToTop10;
	}

	/**
	 * <p>isLimitToTop10.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isLimitToTop10() {
		return limitToTop10;
	}

	/**
	 * <p>setInterval.</p>
	 *
	 * @param interval a int.
	 */
	public void setInterval(int interval) {
		this.selectedInterval = interval;
	}

	/**
	 * <p>getInterval.</p>
	 *
	 * @return a int.
	 */
	public int getInterval() {
		return selectedInterval;
	}

	/**
	 * <p>getYEAR.</p>
	 *
	 * @return a int.
	 */
	public int getYEAR() {
		return YEAR;
	}

	/**
	 * <p>getMONTH.</p>
	 *
	 * @return a int.
	 */
	public int getMONTH() {
		return MONTH;
	}

	/**
	 * <p>Getter for the field <code>totalNumberOfProfiles</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getTotalNumberOfProfiles() {
		return totalNumberOfProfiles;
	}

	/**
	 * <p>Getter for the field <code>totalNumberOfProfilesForCriteria</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getTotalNumberOfProfilesForCriteria() {
		return totalNumberOfProfilesForCriteria;
	}

}
