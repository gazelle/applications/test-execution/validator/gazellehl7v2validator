package net.ihe.gazelle.hl7.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hl7.validator.model.IPAddress;
import net.ihe.gazelle.hl7.validator.model.UserPreferences;
import net.ihe.gazelle.hql.HQLQueryBuilder;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

@Name("userManager")
@Scope(ScopeType.PAGE)
public class UserManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserPreferences selectedPreferences;

	private IPAddress selectedIP;

	public List<UserPreferences> getAllAvailableUsers() {
		HQLQueryBuilder<UserPreferences> builder = new HQLQueryBuilder<UserPreferences>(UserPreferences.class);
		builder.addOrder("username", true);
		return builder.getList();
	}

	public void addIpAddressToUser() {
		if (!selectedPreferences.getAllowedIPAddresses().contains(selectedIP)) {
			if(IPAddress.validate(selectedIP.getIpAddress())){
				selectedPreferences.getAllowedIPAddresses().add(selectedIP);
				selectedIP = new IPAddress();
				saveUser();
			}else{
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This IPv4 address is not valid. Valids IPv4 adress are between 0.0.0.0 and 255.255.255.255");
			}
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "This IP address is already specified for this user");
		}
	}

	public void removeAddressFromSelectedUser(IPAddress address) {

		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		selectedPreferences = entityManager.find(UserPreferences.class,this.selectedPreferences.getId());

		if (selectedPreferences.getAllowedIPAddresses().contains(address)) {
			selectedPreferences.getAllowedIPAddresses().remove(address);
			saveUser();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "This IP address is not allowed for this user");
		}
	}

	public void removeAddressFromUser(IPAddress address) {
		if (selectedPreferences.getAllowedIPAddresses().contains(address)) {
			selectedPreferences.getAllowedIPAddresses().remove(address);
			saveUser();
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.WARN, "This IP address is not allowed for this user");
		}
	}

	public void saveUser() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		selectedPreferences = entityManager.merge(selectedPreferences);
		entityManager.flush();
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Changes successfully saved");
	}

	public String editUser(UserPreferences preferences) {
		if (preferences != null) {
			return "/admin/user.seam?username=" + preferences.getUsername();
		} else {
			return "/admin/user.seam";
		}
	}

	public void deleteUser(UserPreferences preferences) {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		entityManager.remove(preferences);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preferences for user" + preferences.getUsername() + "have been properly deleted");
	}

	public void deleteSelectedUser() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		selectedPreferences = entityManager.find(UserPreferences.class,this.selectedPreferences.getId());
		entityManager.remove(selectedPreferences);
		FacesMessages.instance().add(StatusMessage.Severity.INFO, "Preferences for user" + selectedPreferences.getUsername() + "have been properly deleted");
	}

	public void displayUser() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (params.containsKey("username")) {
			selectedPreferences = UserPreferences.getPreferencesForUsername(params.get("username"));
		} else {
			selectedPreferences = new UserPreferences();
			selectedPreferences.setAllowedIPAddresses(new ArrayList<IPAddress>());
		}
		selectedIP = new IPAddress();
	}

	public UserPreferences getSelectedPreferences() {
		return selectedPreferences;
	}

	public void setSelectedPreferences(UserPreferences selectedPreferences) {
		this.selectedPreferences = selectedPreferences;
	}

	public IPAddress getSelectedIP() {
		return selectedIP;
	}

	public void setSelectedIP(IPAddress selectedIP) {
		this.selectedIP = selectedIP;
	}

}
