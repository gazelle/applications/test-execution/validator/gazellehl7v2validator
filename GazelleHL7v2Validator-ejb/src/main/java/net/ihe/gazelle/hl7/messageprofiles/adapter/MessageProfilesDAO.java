package net.ihe.gazelle.hl7.messageprofiles.adapter;

import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import net.ihe.gazelle.tf.ws.Hl7MessageProfile;

public interface MessageProfilesDAO {

    Profile queryProfileByOid(String oid);

    String generateProfileOID();

    Profile saveProfile(Profile selectedProfile);

    void saveResource(Resource selectedResource);

    String generateResourceOID();

    Profile getProfileByOid(Hl7MessageProfile profileReference);

}

