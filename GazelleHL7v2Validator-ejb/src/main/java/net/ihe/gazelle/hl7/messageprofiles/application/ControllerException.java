package net.ihe.gazelle.hl7.messageprofiles.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ControllerException extends Exception {

    private static Logger log = LoggerFactory.getLogger(ControllerException.class);

    private String key;



    private Object param;

    public ControllerException(String message, String key, Object param) {
        this(message, key);
        this.setParam(param);
    }

    public ControllerException(String message, String key) {
        super(message);
        this.setKey(key);
        log.error(message, this);


    }
    /**
     * Getter for I18n key
     * @return String
     */
    public String getKey() {
        return key;
    }

    /**
     * Setter for I18n key
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Getter for I18n param
     * @return
     */
    public Object getParam() {
        return param;
    }

    /**
     * Setter for I18n param
     * @param param
     */
    public void setParam(Object param) {
        this.param = param;
    }}