package net.ihe.gazelle.hl7.messageprofiles.model;

import java.io.*;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import javax.validation.constraints.NotNull;

import net.ihe.gazelle.preferences.PreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.NONE)
public abstract class CommonProperties implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6189770738035000500L;

    private static Logger LOG = LoggerFactory.getLogger(CommonProperties.class);

    @Column(name = "oid", unique = true)
    @NotNull
    @XmlElement(name = "oid")
    protected String oid;

    @Transient
    protected byte[] content;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    protected Date lastChanged;

    @Column(name = "last_modifier")
    protected String lastModifier;

    @Column(name = "path")
    protected String path;

    @Column(name = "imported_with_errors")
    protected boolean importedWithErrors;

    @Column(name = "revision")
    @XmlElement(name = "revision")
    protected String revision;

    public CommonProperties() {

    }

    abstract public String getDirectory() ;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public byte[] getContent() {

        File f = new File(getDirectory());
        int length = (int) f.length();

        byte[] data = new byte[length];

        BufferedInputStream in = new BufferedInputStream(null);
        try {
            in = new BufferedInputStream(new FileInputStream(f));
            in.read(data, 0, length);
        } catch (IOException e) {
            LOG.error("Cannot open file at " + getDirectory());
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;

    }

    public void setContent(byte[] content) {
        if (content != null) {
            this.content = content.clone();
        }
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public boolean isImportedWithErrors() {
        return importedWithErrors;
    }

    public void setImportedWithErrors(boolean importedWithErrors) {
        this.importedWithErrors = importedWithErrors;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public void setContent(String inContent) {
        if (inContent != null) {
            try {
                this.content = inContent.getBytes("UTF8");
            } catch (UnsupportedEncodingException e) {
                this.content = null;
                LOG.error(e.getMessage(), e);
            }
        } else {
            this.content = null;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((oid == null) ? 0 : oid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CommonProperties other = (CommonProperties) obj;
        if (oid == null) {
            if (other.oid != null) {
                return false;
            }
        } else if (!oid.equals(other.oid)) {
            return false;
        }
        return true;
    }
}
