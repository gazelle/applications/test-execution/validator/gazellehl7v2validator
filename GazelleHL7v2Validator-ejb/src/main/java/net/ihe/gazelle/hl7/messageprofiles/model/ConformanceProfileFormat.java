package net.ihe.gazelle.hl7.messageprofiles.model;

/**
 * <p>ConformanceProfileFormat enum.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */
public enum ConformanceProfileFormat {

    HL7v2xConformanceProfile,
    ConformanceProfile;




}
