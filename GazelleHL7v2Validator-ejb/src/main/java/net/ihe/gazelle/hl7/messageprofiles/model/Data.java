package net.ihe.gazelle.hl7.messageprofiles.model;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class Data {

    public ConformanceProfileFormat[] getStatuses() {
        return ConformanceProfileFormat.values();
    }
}


