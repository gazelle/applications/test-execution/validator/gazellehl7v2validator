/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.hl7.messageprofiles.view;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAO;
import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAOImpl;
import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.messageprofiles.application.HL7Application;
import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.tf.messageprofile.SOAPExceptionException;
import net.ihe.gazelle.tf.ws.Hl7MessageProfile;
import org.apache.axis2.AxisFault;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.List;


/**
 * <b>Class Description : </b>HL7MessageProfileManager<br>
 * <br>
 * This class implements the local interface HL7MessageProfileManagerLocal dedicated to the management of data for profilesList.xhtml and
 * viewProfile.xhtml pages
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, December 15th
 */

@Name("hl7MPManagerBean")
@Scope(ScopeType.PAGE)
public class HL7MessageProfileManagerView implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3930266311006597099L;

    private static Logger log = LoggerFactory.getLogger(HL7MessageProfileManagerView.class);


    @In(create = true, required = false, value = "messageProfilesDAO")
    private MessageProfilesDAO messageProfilesDAO;

    /**
     * Application Manager
     */
    private HL7Application hl7Application;

    /**
     * OID given by the user in the h:inputText component
     */
    private String selectedOid;

    /**
     * net.ihe.gazelle.tf.ws.Hl7MessageProfile is the object used in TF to reference the profile. Contains the link with domain/actor/transaction
     * and other useful informations
     */
    private Hl7MessageProfile selectedMessageProfileReference;

    /**
     * net.ihe.gazelle.repository.hl7.inria.model.Profile is the object used in HL7MPR to store the profile content along with its OID
     */
    private Profile selectedProfile;

    /**
     * List of all message profile references contained in TF
     */
    private List<Hl7MessageProfile> messageProfilesReferences;

    /**
     * List of the format of every message profile
     */
    private List<String> messageProfilesFormat;

    /**
     * List of the path of every message profile
     */
    private List<String> messageProfilesPath;

    /**
     * boolean to indicate whether we are in edit mode (reserved to admin) or not
     */
    private boolean editMode = false;

    /**
     * Constructor
     * @throws ControllerException
     */
    public HL7MessageProfileManagerView() throws ControllerException {
        this.hl7Application = new HL7Application();
        // TODO use injection
        this.hl7Application.setMessageProfilesDAO(new MessageProfilesDAOImpl());
        getAllMessageProfiles();
    }

    /**
     * initialize lists of information : format and path
     */
    private void initListInfo() {
        this.hl7Application.initListInfo();
    }


    /**
     * Getters and Setters
     */

    /**
     * Getters and Setters
     *
     * @return a {@link String} object.
     */
    public String getSelectedOid() {
        return this.hl7Application.getSelectedOid();
    }

    /**
     * <p>Setter for the field <code>selectedOid</code>.</p>
     *
     * @param selectedOid a {@link String} object.
     */
    public void setSelectedOid(String selectedOid) {
        this.hl7Application.setSelectedOid(selectedOid);
    }

    /**
     * <p>Getter for the field <code>selectedMessageProfileReference</code>.</p>
     *
     * @return a {@link Hl7MessageProfile} object.
     */
    public Hl7MessageProfile getSelectedMessageProfileReference() {
        return this.hl7Application.getSelectedMessageProfileReference();
    }

    /**
     * <p>Setter for the field <code>selectedMessageProfileReference</code>.</p>
     *
     * @param selectedMessageProfile a {@link Hl7MessageProfile} object.
     */
    public void setSelectedMessageProfileReference(Hl7MessageProfile selectedMessageProfile) {
        this.hl7Application.setSelectedMessageProfileReference(selectedMessageProfile);
    }

    /**
     * <p>Getter for the field <code>selectedProfile</code>.</p>
     *
     * @return a {@link Profile} object.
     */
    public Profile getSelectedProfile() {
        return this.hl7Application.getSelectedProfile();
    }

    /**
     * <p>Setter for the field <code>selectedProfile</code>.</p>
     *
     * @param selectedProfile a {@link Profile} object.
     */
    public void setSelectedProfile(Profile selectedProfile) {
        this.hl7Application.setSelectedProfile(selectedProfile);
    }

    /**
     * <p>Getter for the field <code>messageProfilesReferences</code>.</p>
     *
     * @return a {@link List} object.
     */
    public List<Hl7MessageProfile> getMessageProfilesReferences() {
        return this.hl7Application.getMessageProfilesReferences();
    }

    /**
     * <p>getAllMessageProfilesReferences.</p>
     *
     * @return a {@link GazelleListDataModel} object.
     */
    public GazelleListDataModel<Hl7MessageProfile> getAllMessageProfilesReferences() {
        return this.hl7Application.getAllMessageProfilesReferences();
    }

    /**
     * <p>Setter for the field <code>messageProfilesReferences</code>.</p>
     *
     * @param profiles a {@link List} object.
     */
    public void setMessageProfilesReferences(List<Hl7MessageProfile> profiles) {
        this.hl7Application.setMessageProfilesReferences(profiles);
    }

    /**
     * Used in the profile list view to complete the format column
     *
     * @param p
     * @return the format of the HL7MessageProfile : HL7v2xConformanceProfile, ConformanceProfile
     */
    public String getFormat(Hl7MessageProfile p) {
        return this.hl7Application.getFormat(p);
    }

    /**
     * used in the profile list to view profile content by clicking on the oid
     *
     * @param p
     * @return the path of the profile
     */
    public String getPath(Hl7MessageProfile p) {
        return this.hl7Application.getPath(p);
    }

    /**
     * getter and setter de messageProfilesFormat
     */
    public List<String> getMessageProfilesFormat() {
        return this.hl7Application.getMessageProfilesFormat();
    }
    public void setMessageProfilesFormat(List<String> messageProfilesFormat) {
        this.hl7Application.setMessageProfilesFormat(messageProfilesFormat);
    }

    /**
     * <p>isEditMode.</p>
     *
     * @return a boolean.
     */
    public boolean isEditMode() {
        return this.hl7Application.isEditMode();
    }

    /**
     * <p>Setter for the field <code>editMode</code>.</p>
     *
     * @param editMode a boolean.
     */
    public void setEditMode(boolean editMode) {
        this.hl7Application.setEditMode(editMode);
    }


    /**
     * Display the list of profiles registered in GMM
     */
    public void displayProfilesList() {
        this.hl7Application.setSelectedMessageProfileReference(null);
    }

    /**
     * <p>displaySelectedMessageProfile.</p>
     *
     * @param inProfile a {@link Hl7MessageProfile} object.
     * @return a {@link String} object.
     */
    public String displaySelectedMessageProfile(Hl7MessageProfile inProfile) {
        return "/browser/profile.seam?oid=" + inProfile.getOid();
    }

    /**
     * <p>displaySelectedMessageProfileInEditMode.</p>
     *
     * @param inProfile a {@link Hl7MessageProfile} object.
     * @return a {@link String} object.
     */
    public String displaySelectedMessageProfileInEditMode(Hl7MessageProfile inProfile) {
        return displaySelectedMessageProfile(inProfile) + "&editMode=true";
    }

    /**
     * <p>selectProfileByOid.</p>
     */
    public void selectProfileByOid() {
        try{
        this.hl7Application.selectProfileByOid();
        }catch (ControllerException controllerException){
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>downloadSelectedProfile.</p>
     *
     * @param inProfile a {@link Hl7MessageProfile} object.
     */
    public void downloadSelectedProfile(Hl7MessageProfile inProfile) {
        try{
            this.hl7Application.downloadSelectedProfile(inProfile);
        }catch (ControllerException controllerException){
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>downloadSelectedProfile.</p>
     */
    public void downloadSelectedProfile() {
        try{
        this.hl7Application.downloadSelectedProfile();
        }catch (ControllerException controllerException){
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>saveProfile.</p>
     */
    public void saveProfile() {
        this.hl7Application.saveProfile();
        displayInfos("Profile successfully saved !");
    }

    /**
     * <p>getMessageProfileByOid.</p>
     *
     * @return a {@link String} object.
     */
    public String getMessageProfileByOid() {
        return "/browser/profile.seam?oid=" + this.getSelectedOid();
    }

    /**
     * <p>goToFullView.</p>
     *
     * @return a {@link String} object.
     */
    public String goToFullView() {
        return "/viewProfile.seam?oid=" + this.getSelectedProfile().getOid();

    }

    /**
     * <p>refreshProfilesList.</p>
     */
    public void refreshProfilesList() {
        getAllMessageProfiles();
    }

    /**
     * Calls the web service to get the list of profiles
     */
    private void getAllMessageProfiles() {
        try {
            this.hl7Application.setMessageProfilesReferences(this.hl7Application.getAllMessageProfiles());
            displayProfilesList();
            initListInfo();
        } catch (ControllerException controllerException) {
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>displayProfileContent.</p>
     */
    public void displayProfileContent() {
        this.hl7Application.displayProfileContent();
    }

    /**
     * @throws RemoteException
     * @throws AxisFault
     * @throws SOAPExceptionException
     */
    private void getReferenceForSelectedProfile() {
        try {
            this.hl7Application.getReferenceForSelectedProfile();
        } catch (ControllerException controllerException) {
            dealControllerException(controllerException);
        }
    }

    /**
     * Display the  error message
     *
     * @param exception
     */
    void dealControllerException(ControllerException exception) {
        FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, exception.getKey(), exception.getParam());
    }

    /**
     * Display infos
     * @param infos
     */
    void displayInfos(String infos){
        FacesMessages.instance().add(infos);
    }
}
