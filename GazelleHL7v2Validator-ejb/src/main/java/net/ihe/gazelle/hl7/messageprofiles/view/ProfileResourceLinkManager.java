package net.ihe.gazelle.hl7.messageprofiles.view;

import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAOImpl;
import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.messageprofiles.application.HL7Application;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import net.ihe.gazelle.tf.messageprofile.Hl7MessageProfilesServiceStub;
import net.ihe.gazelle.tf.ws.*;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;

import java.io.Serializable;
import java.util.List;

/**
 * <p>ProfileResourceLinkManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("profileResourceLinkManager")
@Scope(ScopeType.PAGE)
public class ProfileResourceLinkManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;
    private HL7Application hl7Application;

    private String profileOid;
    private GetListOfPossibleHL7MessageProfileForGivenContext wsParams;
    private List<Resource> availableResources;
    private List<Resource> selectedResources;
    private List<Hl7MessageProfile> selectedProfiles;
    private Hl7MessageProfilesServiceStub stub;

    public ProfileResourceLinkManager() throws ControllerException {
        this.hl7Application = new HL7Application();
        this.hl7Application.setMessageProfilesDAO(new MessageProfilesDAOImpl());
    }

    /**
     * <p>getProfileReferencesFiltered.</p>
     */
    public void getProfileReferencesFiltered() {
       try{
           this.hl7Application.getProfileReferencesFiltered();
       }catch (ControllerException controllerException){
           displayInfos(StatusMessage.Severity.WARN,controllerException.getMessage());
       }
    }

    /**
     * <p>initializePage.</p>
     */
    public void initializePage() {
        this.hl7Application.initializePage();
    }

    /**
     * <p>selectResource.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public void selectResource(Resource inResource) {
        this.hl7Application.selectResource(inResource);
    }

    /**
     * <p>createLink.</p>
     */
    public void createLink() {
        if ((this.hl7Application.getSelectedProfiles() == null) || (this.hl7Application.getSelectedResources() == null)) {
            displayInfos(StatusMessage.Severity.WARN, "No profile and/or resource selected");
        } else {

            this.hl7Application.createLink(FacesMessages.instance());

        }
    }

    /**
     * <p>resetFilters.</p>
     */
    public void resetFilters() {
        this.hl7Application.resetFilters();
    }

    /**
     * <p>Getter for the field <code>profileOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProfileOid() {
        return this.hl7Application.getProfileOid();
    }

    /**
     * <p>Setter for the field <code>profileOid</code>.</p>
     *
     * @param profileOid a {@link java.lang.String} object.
     */
    public void setProfileOid(String profileOid) {
        this.hl7Application.setProfileOid(profileOid);
    }

    /**
     * <p>Getter for the field <code>wsParams</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
     */
    public GetListOfPossibleHL7MessageProfileForGivenContext getWsParams() {
        return this.hl7Application.getWsParams();
    }

    /**
     * <p>Setter for the field <code>wsParams</code>.</p>
     *
     * @param wsParams a {@link net.ihe.gazelle.tf.ws.GetListOfPossibleHL7MessageProfileForGivenContext} object.
     */
    public void setWsParams(GetListOfPossibleHL7MessageProfileForGivenContext wsParams) {
        this.hl7Application.setWsParams(wsParams);
    }

    /**
     * <p>Getter for the field <code>availableResources</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Resource> getAvailableResources() {
        return this.hl7Application.getAvailableHL7Resources();
    }

    /**
     * <p>Setter for the field <code>availableResources</code>.</p>
     *
     * @param availableResources a {@link java.util.List} object.
     */
    public void setAvailableResources(List<Resource> availableResources) {
        this.hl7Application.setAvailableResources(availableResources);
    }

    /**
     * <p>Getter for the field <code>selectedResources</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Resource> getSelectedResources() {
        return this.hl7Application.getSelectedResources();
    }

    /**
     * <p>Setter for the field <code>selectedResources</code>.</p>
     *
     * @param selectedResources a {@link java.util.List} object.
     */
    public void setSelectedResources(List<Resource> selectedResources) {
        this.hl7Application.setSelectedResources(selectedResources);
    }

    /**
     * <p>Getter for the field <code>selectedProfiles</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Hl7MessageProfile> getSelectedProfiles() {
        return this.hl7Application.getSelectedProfiles();
    }

    /**
     * <p>Setter for the field <code>selectedProfiles</code>.</p>
     *
     * @param selectedProfiles a {@link java.util.List} object.
     */
    public void setSelectedProfiles(List<Hl7MessageProfile> selectedProfiles) {
        this.hl7Application.setSelectedProfiles(selectedProfiles);
    }

    /**
     * display infos
     * @param severity
     * @param infos
     */
    void displayInfos(StatusMessage.Severity severity, String infos) {
        FacesMessages.instance().add(severity, infos);
    }

}
