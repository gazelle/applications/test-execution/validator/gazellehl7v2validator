package net.ihe.gazelle.hl7.messageprofiles.view;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hl7.messageprofiles.adapter.MessageProfilesDAOImpl;
import net.ihe.gazelle.hl7.messageprofiles.application.ControllerException;
import net.ihe.gazelle.hl7.messageprofiles.application.HL7Application;
import net.ihe.gazelle.hl7.messageprofiles.model.Resource;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.log.Log;

import java.io.IOException;
import java.io.Serializable;

/**
 * <p>ResourceDisplayManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("resourceDisplayManager")
@Scope(ScopeType.PAGE)
public class ResourceDisplayManagerView implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Logger
    private static Log log;
    private transient Filter<Resource> filter;

    /**
     * Application Manager
     */
    private HL7Application hl7Application;


    private Resource selectedResource;
    private boolean editMode;

    public ResourceDisplayManagerView() throws ControllerException {
        this.hl7Application = new HL7Application();
        this.hl7Application.setMessageProfilesDAO(new MessageProfilesDAOImpl());
    }


    /**
     * <p>getAvailableResources.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<Resource> getAvailableResources() {
        return this.hl7Application.getAvailableResources();
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
     */
    public Filter<Resource> getFilter() {
        return this.hl7Application.getFilter();
    }

    /**
     * <p>Setter for the field <code>selectedResource</code>.</p>
     *
     * @param selectedResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public void setSelectedResource(Resource selectedResource) {
        this.hl7Application.setSelectedResource(selectedResource);
    }

    /**
     * <p>Getter for the field <code>selectedResource</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public Resource getSelectedResource() {
        return this.hl7Application.getSelectedResource();
    }

    /**
     * <p>isEditMode.</p>
     *
     * @return a boolean.
     */
    public boolean isEditMode() {
        return this.hl7Application.isEditMode();
    }

    /**
     * <p>Setter for the field <code>editMode</code>.</p>
     *
     * @param editMode a boolean.
     */
    public void setEditMode(boolean editMode) {
        this.hl7Application.setEditMode(editMode);
    }


    /**
     * <p>displaySelectedResource.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     * @return a {@link java.lang.String} object.
     */
    public String displaySelectedResource(Resource inResource) {
        return "/browser/resource.seam?oid=" + inResource.getOid();
    }

    /**
     * <p>displaySelectedResourceInEditMode.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     * @return a {@link java.lang.String} object.
     */
    public String displaySelectedResourceInEditMode(Resource inResource) {
        return displaySelectedResource(inResource) + "&editMode=true";
    }

    /**
     * <p>selectResourceByOid.</p>
     */
    public void selectResourceByOid() {
        try {
            this.hl7Application.selectResourceByOid();
        } catch (ControllerException controllerException) {
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>downloadSelectedResource.</p>
     *
     * @param inResource a {@link net.ihe.gazelle.hl7.messageprofiles.model.Resource} object.
     */
    public void downloadSelectedResource(Resource inResource) throws IOException {
        try {
            this.hl7Application.downloadSelectedResource(inResource);
        } catch (ControllerException controllerException) {
            dealControllerException(controllerException);
        }
    }

    /**
     * <p>saveResource.</p>
     */
    public void saveResource() {
        this.hl7Application.saveResource();
        FacesMessages.instance().add("Resource successfully saved !");
    }

    /**
     * <p>displayResourceContent.</p>
     */
    public void displayResourceContent() {
        this.hl7Application.displayResourceContent();
    }

    /**
     * <p>goToFullView.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String goToFullView() {
        return "/viewResource.seam?oid=" + this.hl7Application.getSelectedResource().getOid();
    }

    /**
     * <p>downloadSelectedResource.</p>
     */
    public void downloadSelectedResource() throws IOException {
        downloadSelectedResource(this.hl7Application.getSelectedResource());
    }

    /**
     * Display the  error message
     *
     * @param exception
     */
    private void dealControllerException(ControllerException exception) {
        FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, exception.getKey(), exception.getParam());
    }

}
