package net.ihe.gazelle.hl7.utils;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class ValidationErrorHandler extends DefaultHandler implements ErrorHandler {

	private Integer nbOfErrors;
	private List<String> errorMessages;
	private Locator locator;

	public ValidationErrorHandler() {
		nbOfErrors = 0;
		errorMessages = new ArrayList<String>();
	}

	@Override
	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		nbOfErrors++;
		errorMessages.add("ERROR (line: " + exception.getLineNumber() + ", column: " + exception.getColumnNumber()
				+ "): " + exception.getMessage());
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		nbOfErrors++;
		errorMessages.add("FATAL (line: " + exception.getLineNumber() + ", column: " + exception.getColumnNumber()
				+ "): " + exception.getMessage());
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		// nothing to do
	}

	public Integer getNbOfErrors() {
		return nbOfErrors;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

}
