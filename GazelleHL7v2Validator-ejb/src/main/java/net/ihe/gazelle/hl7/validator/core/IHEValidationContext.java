package net.ihe.gazelle.hl7.validator.core;

import ca.uhn.hl7v2.validation.impl.ValidationContextImpl;
import net.ihe.gazelle.hl7.validator.rules.IHERuleBinding;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.hl7.validator.rules.iti.*;
import net.ihe.gazelle.hl7.validator.rules.law.*;
import net.ihe.gazelle.hl7.validator.rules.ltw.ERRSegmentInORL;
import net.ihe.gazelle.hl7.validator.rules.ltw.RESPONSEGroupInORL;
import net.ihe.gazelle.hl7.validator.rules.tro.OBRSegmentInOMI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class IHEValidationContext extends ValidationContextImpl {

	/**
	 *
	 */
	private static final long serialVersionUID = 7196272693343496546L;

	private static List<IHERuleBinding> myIHERuleBindings;

	public static final IHEValidationContext SINGLETON = new IHEValidationContext();

	private static Logger log = LoggerFactory.getLogger(IHEValidationContext.class);

	public IHEValidationContext() {
		super();
		listRules();
	}

	private static void listRules() {
		myIHERuleBindings = new ArrayList<>();

		// LTW
		ERRSegmentInORL errSegmentRule = new ERRSegmentInORL();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.111", "ORL_O22/ERR", errSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.109", "ORL_O34/ERR", errSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.110", "ORL_O36/ERR", errSegmentRule));

		RESPONSEGroupInORL responseGroupRule = new RESPONSEGroupInORL();
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.111", "ORL_O22/RESPONSE", responseGroupRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.109", "ORL_O34/RESPONSE", responseGroupRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.110", "ORL_O36/RESPONSE", responseGroupRule));

		// LAW
		PIDSegmentInOUL pidLAWRule = new PIDSegmentInOUL();
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "OUL_R22/PATIENT/PID/PID-3", pidLAWRule));

		OBXSegmentInLAW obxLAWRule = new OBXSegmentInLAW();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-2", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-5", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-6", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-18/EI-3", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-18/EI-4", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-2", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-5", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-6", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-18/EI-3", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-18/EI-4", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-2", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-5", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-6", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-18/EI-3", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-18/EI-4", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*OBX/OBX-8/CWE-3", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*OBX/OBX-8/CWE-3", obxLAWRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*OBX/OBX-8/CWE-3", obxLAWRule));

		SACSegmentInLAW sacSegmentRule = new SACSegmentInLAW();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-3", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-4", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-10", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-11", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-13", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-14", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-15", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*SAC/SAC-29", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*SAC/SAC-3", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*SAC/SAC-4", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*SAC/SAC-3", sacSegmentRule));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*SAC/SAC-4", sacSegmentRule));

		QPDSegmentInLAW qpdSegmentRule = new QPDSegmentInLAW();
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-3", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-4", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-5", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-6", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-7", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-8", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.180", "QBP_Q11/QPD/QPD-9", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-3", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-4", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-5", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-6", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-7", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-8", qpdSegmentRule));
		getIHERuleBindings()
				.add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.177", "RSP_K11/QPD/QPD-9", qpdSegmentRule));

		INVSegmentInOUL invSegmentInOUL = new INVSegmentInOUL();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*INV", invSegmentInOUL));

		OBRSegmentInOUL obrSegmentInOUL = new OBRSegmentInOUL();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/OBR/OBR-26", obrSegmentInOUL));

		OULR22RulesInLAW oulr22RulesInLAW = new OULR22RulesInLAW();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/PATIENT", oulr22RulesInLAW));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/VISIT", oulr22RulesInLAW));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/INV", oulr22RulesInLAW));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/TIMING_QTY", oulr22RulesInLAW));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/RESULT", oulr22RulesInLAW));
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/TCD", oulr22RulesInLAW));

		DatatypesInLAW datatypesInLAW = new DatatypesInLAW();
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/OBR-16/XCN-9/HD-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/OBR-16/XCN-9/HD-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/OBR-16/XCN-9/HD-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/OBX-3/CE-5", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/OBX-3/CE-6", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/OBX-3/CE-5", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/OBX-3/CE-6", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/OBX-3/CE-5", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/OBX-3/CE-6", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SAC-24/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SAC-24/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SAC-24/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/OBX-6/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/OBX-6/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/OBX-6/CE-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-3/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-3/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-3/EIP-1/EI-4", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-3/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-3/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-3/EIP-1/EI-4", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-3/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-3/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-3/EIP-1/EI-4", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-2/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-2/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/SPM-2/EIP-1/EI-4", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-2/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-2/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.182", "*/SPM-2/EIP-1/EI-4", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-2/EIP-1/EI-2", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-2/EIP-1/EI-3", datatypesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.178", "*/SPM-2/EIP-1/EI-4", datatypesInLAW));

        ORLO34RulesInLAW orlo34RulesInLAW = new ORLO34RulesInLAW();
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/ERR", orlo34RulesInLAW));
        getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.12559.11.1.1.181", "*/RESPONSE", orlo34RulesInLAW));

		// TRO
		OBRSegmentInOMI obrSegmentInOMI = new OBRSegmentInOMI();
		getIHERuleBindings().add(new IHERuleBinding("1.3.6.1.4.1.12559.11.13.8.1.3", "*OBR-20", obrSegmentInOMI));

		// ITI
		// IHE_FR
		String[] iti30 = new String[]{
				"2.16.840.1.113883.2.8.3.1.23", // ADT^A28^ADT_A05
				"2.16.840.1.113883.2.8.3.1.24", // ADT^A31^ADT_A05
				"2.16.840.1.113883.2.8.3.1.1", // ADT^A01^ADT_A01
				"2.16.840.1.113883.2.8.3.1.4", // ADT^A04^ADT_A01
				"2.16.840.1.113883.2.8.3.1.5" // ADT^A05^ADT_A05
		};
		INSValidationRules[] insrules = new INSValidationRules[] {
				new PID3RulesInITI(),
				new PID7RulesInITI(),
				new PID8RulesInITI(),
				new PID11RulesInITI(),
				new PID3RulesWarningInITI(),
		};
		for (String oid:iti30) {
			for (INSValidationRules insrule:insrules) {
				getIHERuleBindings().add(new IHERuleBinding(oid, insrule.getHl7Path(), insrule));
			}
		}

	}

	public List<IHEValidationRule> getIHEValidationRules(String profileOid,
                                                         String hl7path) {
		hl7path = hl7path.replaceAll("\\[[0-9]*\\]", "");
		hl7path = hl7path.replaceAll("\\([^\\)]*[^/]*\\)", "");
		List<IHEValidationRule> active = new ArrayList<>();
		for (IHERuleBinding binding : myIHERuleBindings) {
			if (binding.getActive() && binding.appliesToVersion(profileOid) && binding.appliesToScope(hl7path)) {
				active.add(binding.getRule());
			}
		}
		return active;
	}

	public static List<IHERuleBinding> getIHERuleBindings() {
		return myIHERuleBindings;
	}
}
