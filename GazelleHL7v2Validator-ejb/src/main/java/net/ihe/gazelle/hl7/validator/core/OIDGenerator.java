package net.ihe.gazelle.hl7.validator.core;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Entity
/**
 * <p>OIDGenerator class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("oidGenerator")
@Table(name = "oid_generator", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "target"))
@SequenceGenerator(name = "oid_generator_sequence", sequenceName = "oid_generator_id_seq", allocationSize = 1)
public class OIDGenerator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6157684006228469633L;

	/** Constant <code>PROFILE="profile"</code> */
	public static final String PROFILE = "profile";
	/** Constant <code>RESOURCE="resource"</code> */
	public static final String RESOURCE = "resource";
	/** Constant <code>MESSAGE="message"</code> */
	public static final String MESSAGE = "message";

	@Logger
	private static Log log;

	@Id
	@NotNull
	@GeneratedValue(generator = "oid_generator_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "target")
	private String target;

	@Column(name = "root")
	private String root;

	@Column(name = "next_value")
	private Integer nextValue;

	/**
	 * <p>Constructor for OIDGenerator.</p>
	 */
	public OIDGenerator() {

	}

	/**
	 * <p>generateNewOIDForProfile.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String generateNewOIDForProfile(EntityManager entityManager) {
		return generateNewOID(PROFILE, entityManager);
	}

	/**
	 * <p>generateNewOIDForResource.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String generateNewOIDForResource(EntityManager entityManager) {
		return generateNewOID(RESOURCE, entityManager);
	}

	/**
	 * <p>generateNewOIDForMessage.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String generateNewOIDForMessage(EntityManager entityManager) {
		return generateNewOID(MESSAGE, entityManager);
	}

	/**
	 * <p>generateNewOID.</p>
	 *
	 * @param target a {@link java.lang.String} object.
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String generateNewOID(String target, EntityManager entityManager) {
		HQLQueryBuilder<OIDGenerator> builder = new HQLQueryBuilder<OIDGenerator>(OIDGenerator.class);
		builder.addEq("target", target);
		OIDGenerator generator = builder.getUniqueResult();
		if (generator != null) {
			Integer nextValue = generator.getNextValue();
			String oid = generator.getRoot().concat(nextValue.toString());
			generator.setNextValue(nextValue + 1);
			entityManager.merge(generator);
			entityManager.flush();
			return oid;
		} else {
			log.error("No generator defined for target: " + target);
			return null;
		}
	}

	/**
	 * Returns true if the begining of the given OID matches the root OID stored in database
	 *
	 * @param inOid a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isOIDOwnedByIHE(String inOid) {
		HQLQueryBuilder<OIDGenerator> builder = new HQLQueryBuilder<OIDGenerator>(OIDGenerator.class);
		builder.addRestriction(HQLRestrictions.like("root_oid", inOid, HQLRestrictionLikeMatchMode.START));
		return (builder.getCount() > 0);
	}

	/**
	 * <p>Getter for the field <code>target</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * <p>Setter for the field <code>target</code>.</p>
	 *
	 * @param target a {@link java.lang.String} object.
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * <p>Getter for the field <code>root</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRoot() {
		return root;
	}

	/**
	 * <p>Setter for the field <code>root</code>.</p>
	 *
	 * @param root a {@link java.lang.String} object.
	 */
	public void setRoot(String root) {
		this.root = root;
	}

	/**
	 * <p>Getter for the field <code>nextValue</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getNextValue() {
		return nextValue;
	}

	/**
	 * <p>Setter for the field <code>nextValue</code>.</p>
	 *
	 * @param nextValue a {@link java.lang.Integer} object.
	 */
	public void setNextValue(Integer nextValue) {
		this.nextValue = nextValue;
	}

}
