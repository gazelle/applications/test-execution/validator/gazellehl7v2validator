package net.ihe.gazelle.hl7.validator.core;

enum Optionality {
	REQUIRED("R"),
	REQUIRED_OR_EMPTY("RE"),
	OPTIONAL("O"),
	BACKWARD_COMPATIBILITY("B"),
	CONDITIONAL("C"),
	CONDITIONAL_OR_EMPTY("CE"),
	NOT_SUPPORTED("X");

	private String key;

	Optionality(String key) {
		this.key = key;
	}

	/**
	 * <p>getEnum.</p>
	 *
	 * @param key a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7.validator.core.Optionality} object.
	 */
	public static Optionality getEnum(String key) {
		for (Optionality opt : values()) {
			if (opt.key.equals(key)) {
				return opt;
			} else {
				continue;
			}
		}
		return null;
	}

	/**
	 * <p>Getter for the field <code>key</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getKey() {
		return key;
	}
}
