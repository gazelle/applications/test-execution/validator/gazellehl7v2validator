package net.ihe.gazelle.hl7.validator.datamodel;

import java.util.Date;
import java.util.List;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hl7.validator.model.UserPreferences;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import org.jboss.seam.security.Identity;

/**
 * <p>HL7MessageDataModel class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7MessageDataModel extends FilterDataModel<HL7v2ValidationLog> {

	private String messageOID;
	private Date startDate;
	private Date endDate;
	private String metadataExtract;
	private String callerIP;
	private boolean onlyMessagesWithExceptions;

	/**
	 * <p>Constructor for HL7MessageDataModel.</p>
	 *
	 * @param filter a {@link net.ihe.gazelle.common.filter.Filter} object.
	 * @param messageOID a {@link java.lang.String} object.
	 * @param startDate a {@link java.util.Date} object.
	 * @param endDate a {@link java.util.Date} object.
	 * @param metadataExtract a {@link java.lang.String} object.
	 * @param callerIP a {@link java.lang.String} object.
	 * @param onlyMessagesWithExceptions a boolean.
	 */
	public HL7MessageDataModel(Filter<HL7v2ValidationLog> filter, String messageOID, Date startDate, Date endDate,
							   String metadataExtract, String callerIP, boolean onlyMessagesWithExceptions) {
		super(filter);
		this.messageOID = messageOID;
		this.startDate = startDate;
		this.endDate = endDate;
		this.metadataExtract = metadataExtract;
		this.callerIP = callerIP;
		this.onlyMessagesWithExceptions = onlyMessagesWithExceptions;
	}

	/** {@inheritDoc} */
	@Override
	public void appendFiltersFields(HQLQueryBuilder<HL7v2ValidationLog> queryBuilder) {
		if ((messageOID != null) && !messageOID.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("oid", messageOID, HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (startDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.ge("validationDate", startDate));
		}
		if (endDate != null) {
			queryBuilder.addRestriction(HQLRestrictions.le("validationDate", endDate));
		}
		if ((metadataExtract != null) && !metadataExtract.isEmpty()) {
			queryBuilder.addRestriction(HQLRestrictions.like("metadata", metadataExtract,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		// not logged in user are only allowed to view the message validate using their current IP address
		if (!Identity.instance().isLoggedIn()) {
			queryBuilder.addEq("callerIP", callerIP);
		}
		// logged in user (not admin) are allowed to see validation results from a set of defined ip
		else if (!Identity.instance().hasRole("admin_role")) {
			List<String> allowedIps = UserPreferences.getAllowedIpsForUser(Identity.instance().getCredentials()
					.getUsername());
			if ((allowedIps != null) && !allowedIps.isEmpty()) {
				queryBuilder.addIn("callerIP", allowedIps);
			} else {
				queryBuilder.addEq("callerIP", callerIP);
			}
		}
		// only admin users are allowed to view all the messages
		else if (onlyMessagesWithExceptions) {
			queryBuilder.addEq("raisesProfileExceptions", true);
		}
	}
/** {@inheritDoc} */
@Override
        protected Object getId(HL7v2ValidationLog t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
