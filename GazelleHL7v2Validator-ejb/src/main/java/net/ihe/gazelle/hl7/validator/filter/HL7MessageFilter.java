package net.ihe.gazelle.hl7.validator.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hl7.validator.criterion.CriterionToProfileOid;
import net.ihe.gazelle.hl7.validator.criterion.CriterionToTestResult;
import net.ihe.gazelle.hl7.validator.model.HL7v2ValidationLog;
import net.ihe.gazelle.hql.criterion.HQLCriterion;

public class HL7MessageFilter extends Filter<HL7v2ValidationLog> {

	private static List<HQLCriterion<HL7v2ValidationLog, ?>> defaultCriteria;

	static {
		defaultCriteria = new ArrayList<HQLCriterion<HL7v2ValidationLog, ?>>();
		defaultCriteria.add(CriterionToProfileOid.SINGLETON);
		defaultCriteria.add(CriterionToTestResult.SINGLETON);
	}

	public HL7MessageFilter(Map<String, String> requestParameterMap) {
		super(HL7v2ValidationLog.class, defaultCriteria, requestParameterMap);
	}

}
