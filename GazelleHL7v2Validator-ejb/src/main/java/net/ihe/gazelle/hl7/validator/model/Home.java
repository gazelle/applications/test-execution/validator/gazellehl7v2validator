package net.ihe.gazelle.hl7.validator.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

@Entity
@Name("home")
@Table(name = "cmn_home", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "iso3_language"))
@SequenceGenerator(name = "cmn_home_sequence", sequenceName = "cmn_home_id_seq", allocationSize = 1)
public class Home implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1218389994561421605L;

	@Id
	@GeneratedValue(generator = "cmn_home_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "iso3_language")
	private String iso3Language;

	@Column(name = "main_content")
	@Lob
	@Type(type = "text")
	private String mainContent;

	@Column(name = "home_title")
	private String homeTitle;

	public Home() {

	}

	public Home(String iso3Language) {
		this.iso3Language = iso3Language;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIso3Language() {
		return iso3Language;
	}

	public void setIso3Language(String iso3Language) {
		this.iso3Language = iso3Language;
	}

	public String getMainContent() {
		return mainContent;
	}

	public void setMainContent(String mainContent) {
		this.mainContent = mainContent;
	}

	public String getHomeTitle() {
		return homeTitle;
	}

	public void setHomeTitle(String homeTitle) {
		this.homeTitle = homeTitle;
	}

	/**
	 * 
	 * @return the saved instance
	 */
	public Home save() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		Home home = entityManager.merge(this);
		entityManager.flush();
		return home;
	}

	/**
	 * 
	 * @return the home for the given language
	 */
	public static Home getHomeForSelectedLanguage() {
		String language = org.jboss.seam.core.Locale.instance().getISO3Language();
		if ((language != null) && !language.isEmpty()) {
			HomeQuery query = new HomeQuery();
			query.iso3Language().eq(language);
			List<Home> homes = query.getList();
			if ((homes != null) && !homes.isEmpty()) {
				return homes.get(0);
			} else {
				return new Home(language);
			}
		} else {
			return null;
		}
	}
}
