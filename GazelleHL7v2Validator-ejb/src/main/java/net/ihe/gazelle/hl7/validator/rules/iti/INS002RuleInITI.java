package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.ST;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-002 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-002)
 * predicate: L'INS doit toujours être véhiculé avec sa clé.
 * Prescription level: Mandatory / Required / Shall
 * Page: 6
 * Section: 6.7.1
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:10 by aberge
 * Commentaire: Si CX-4-2 dans [1.2.250.1.213.1.4.8, 1.2.250.1.213.1.4.9, 1.2.250.1.213.1.4.10, 1.2.250.1.213.1.4.11] alors CX-1 est de longueur 15.

 * @author pdt
 *
 */
public class INS002RuleInITI extends INSValidationRule {
	private final Pattern cle = Pattern.compile("^(\\d{15})$");
	protected ValidationException[] validate(PID pid) throws HL7Exception {
		List<ValidationException> exceptions = new ArrayList<>();
		// Si CX-4-2 dans [1.2.250.1.213.1.4.8, 1.2.250.1.213.1.4.9, 1.2.250.1.213.1.4.10, 1.2.250.1.213.1.4.11] alors CX-1 est de longueur 15.
		for(CX cx : pid.getPatientIdentifierList()) {
			ST matricule = cx.getCx4_AssigningAuthority().getHd2_UniversalID();
			String identifierTypeCode = cx.getCx5_IdentifierTypeCode().getValue();
			if (matriculesINS.matcher(matricule.getValueOrEmpty()).matches() && identifierTypeCodeINS.matcher(identifierTypeCode).matches() && !cle.matcher(cx.getIDNumber().getValueOrEmpty()).matches()) {
				exceptions.add(new INSValidationException(matricule,cx.getIDNumber().getValueOrEmpty()));
			}
		}
		return exceptions.isEmpty() ? null : exceptions.toArray(new ValidationException[]{});
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.PREDICATE_FAILURE.getCode();
	}

}
