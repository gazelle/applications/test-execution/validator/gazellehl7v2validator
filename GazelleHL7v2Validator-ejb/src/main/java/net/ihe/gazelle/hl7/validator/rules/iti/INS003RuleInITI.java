package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-003 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-003)
 * predicate: Si l'identité du patient a été qualifiée, alors le nom de naissance (type L) est obligatoire.
 * Prescription level: Mandatory / Required / Shall
 * Page: 7
 * Section: 6.7.2
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:13 by aberge
 * Commentaire: Si PID-32 = VALI alors au moins une occurrence de PID-5 avec XPN-7 = L

 * @author pdt
 *
 */
public class INS003RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		if (hasMatriculeINS(pid)) {
			for (XPN xpn : pid.getPatientName()) {
				if ("L".equals(xpn.getNameTypeCode().getValueOrEmpty())) {
					return null;
				}
			}
			return new ValidationException[] {new INSValidationException()};
		}
		return null;
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
