package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-005 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-005)
 * predicate: Dans le cas où l'on véhicule l'INS et que l'identité est qualifiée, la date de naissance est obligatoire.
 * Prescription level: Mandatory / Required / Shall
 * Page: 7
 * Section: 6.7.4
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:23 by aberge
 * Commentaire: Si PID-32 = VALI alors PID-7 est présent et non vide

 * @author pdt
 *
 */
public class INS005RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		if (hasMatriculeINS(pid) && isEmpty(pid.getDateTimeOfBirth())) {
			return new ValidationException[] {new INSValidationException()};
		}
		return null;
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
