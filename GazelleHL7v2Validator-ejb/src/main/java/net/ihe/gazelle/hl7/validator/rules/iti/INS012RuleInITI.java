package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

import java.util.ArrayList;
import java.util.List;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-012 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-012)
 * predicate: Le composant "Prénom" est obligatoire dans le cas où l'on véhicule l'INS et que l'identité est qualifiée.
 * Prescription level: Mandatory / Required / Shall
 * Page: 16
 * Section: N.10.2
 * Status: to be reviewed
 * Last changed: 12/05/21 11:49:54 by aberge
 * Commentaire: Si PID-32 = VALI alors PID-5-2 est obligatoire quand PID-5-7 = L

 * @author pdt
 *
 */
public class INS012RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		List<ValidationException> exceptions = new ArrayList<>();
		if (hasMatriculeINS(pid)) {
			for (XPN xpn : pid.getPatientName()) {
				// PID-5-2 est obligatoire quand PID-5-7 = L
				if ("L".equals(xpn.getNameTypeCode().getValueOrEmpty()) && isEmpty(xpn.getGivenName())) {
					exceptions.add(new INSValidationException(xpn.toString()));
				}
			}
		}
		return exceptions.isEmpty() ? null : exceptions.toArray(new ValidationException[]{});
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.REQUIRED_FIELD_MISSING.getCode();
	}

}
