package net.ihe.gazelle.hl7.validator.rules.iti;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v25.datatype.ST;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.pam.fr.hl7v2.model.v25.segment.PID;

import java.util.ArrayList;
import java.util.List;

/**
 * ID Scheme: INS (https://interop.esante.gouv.fr/AssertionManagerGui/idSchemes/index.seam?idScheme=INS)

 * Assertion ID : INS-013 (https://interop.esante.gouv.fr/AssertionManagerGui/assertions/show.seam?idScheme=INS&assertionId=INS-013)
 * predicate: Quand le champ PID-32 est égal à "VALI" et que l'INS est présent, les noms et prénoms sont en caractères majuscules non accentués, sans signe diacritique, les tirets et apostrophes doivent être conservés.
 * Prescription level: Mandatory / Required / Shall
 * Page: 39
 * Section: 6.6.14
 * Status: to be reviewed
 * Last changed: 3/28/22 4:37:42 PM by NicolasBailliet
 * Commentaire: Quand le champ PID-32 est égal à "VALI" et que l'INS est présent, les noms et prénoms sont en caractères majuscules non accentués, sans signe diacritique, les tirets et apostrophes doivent être conservés.

 * @author nbt
 *
 */
public class INS013RuleInITI extends INSValidationRule {

	protected ValidationException[] validate(PID pid) throws HL7Exception {
		List<ValidationException> exceptions = new ArrayList<>();
		if (hasMatriculeINS(pid) && isIdentiteQualifiee(pid)) {
			String regexPattern = "[A-Z \\-']+";
			for (XPN xpn : pid.getPatientName()) {
				ST givenName = xpn.getGivenName();
				ST familyName = xpn.getFamilyName().getSurname();
				ST secondGivenNames = xpn.getSecondAndFurtherGivenNamesOrInitialsThereof();
				ST spouseFamilyName = xpn.getFamilyName().getSurnameFromPartnerSpouse();
				if ((!givenName.isEmpty() && !givenName.toString().matches(regexPattern)) || (!familyName.isEmpty() && !familyName.toString().matches(regexPattern))
				|| (!secondGivenNames.isEmpty() && !secondGivenNames.toString().matches(regexPattern)) || (!spouseFamilyName.isEmpty() && !spouseFamilyName.toString().matches(regexPattern))) {
					exceptions.add(new INSValidationException(xpn.toString()));
				}
			}
		}
		return exceptions.isEmpty() ? null : exceptions.toArray(new ValidationException[]{});
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.CONDITIONAL.getCode();
	}
}
