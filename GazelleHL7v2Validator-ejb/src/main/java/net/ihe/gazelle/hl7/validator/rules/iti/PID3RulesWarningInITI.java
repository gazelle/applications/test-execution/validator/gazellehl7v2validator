package net.ihe.gazelle.hl7.validator.rules.iti;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;

/**
 *  INS rules bound on PID-3-4-2 hl7path
 *
 * @author nbt
 *
 */
public class PID3RulesWarningInITI extends INSValidationRules {

	@Override
	protected INSValidationRule[] getRules() {
		return new INSValidationRule[] {
				new INS013RuleInITI(),
		};
	}

	@Override
	public String getHl7Path() { return "*/PID/PID-3/CX-4/HD-2"; }

	@Override
	public int getSeverity() {
		return GazelleErrorCode.CONDITIONAL.getCode();
	}

	@Override
	public String getSectionReference() {
		return "Extension du profil PAM en France, "+msg.get("section");
	}

}
