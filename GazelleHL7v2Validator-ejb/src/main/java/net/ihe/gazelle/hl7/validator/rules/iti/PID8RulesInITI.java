package net.ihe.gazelle.hl7.validator.rules.iti;

/**
 * INS rules bound on PID-8 hl7path
 *
 * @author pdt
 *
 */
public class PID8RulesInITI extends INSValidationRules {

	@Override
	protected INSValidationRule[] getRules() {
		return new INSValidationRule[] {
				new INS006RuleInITI(),
				new INS007RuleInITI()
		};
	}

	@Override
	public String getHl7Path() { return "*/PID/PID-8"; }
}
