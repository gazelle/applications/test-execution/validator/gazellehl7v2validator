package net.ihe.gazelle.hl7.validator.rules.law;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.group.OUL_R22_SPECIMEN;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.OUL_R22;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.validation.ValidationException;

public class INVSegmentInOUL extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1098387785377466454L;

	@Override
	public ValidationException[] test(Message msg) {
		return null;
	}

	@Override
	public ValidationException[] test(Message msg, final String hl7path) {
		String path = formatHl7Path(hl7path);
		int begin = path.indexOf("SPECIMEN(");
		int end = path.indexOf(')', begin);
		String specimenGroupRep = path.substring(begin + 9, end);
		if (msg instanceof OUL_R22) {
			OUL_R22 oulMessage = (OUL_R22) msg;
			OUL_R22_SPECIMEN specimenGroup = oulMessage.getSPECIMEN(Integer.decode(specimenGroupRep));
			try {
				if (!specimenGroup.getSPM().getSpm11_SpecimenRole().getCwe1_Identifier().encode().equals("Q")
						&& !specimenGroup.getCONTAINER().getINV().isEmpty()) {
					ValidationException exception = new ValidationException(getDescription());
					ValidationException[] exceptions = { exception };
					return exceptions;
				}
			} catch (HL7Exception e) {
				ValidationException exception = new ValidationException(e.getMessage());
				ValidationException[] exceptions = { exception };
				return exceptions;
			}
		}
		return null;
	}

	@Override
	public String getDescription() {
		return "The INV segment is not supported when SPM-11 is not set to the value of 'Q'";
	}

	@Override
	public String getSectionReference() {
		return "PaLM TF-2b Rev. 9.0, Section 3.29.4.1.2.1 OUL^R22 Message Static Definition";
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
	}

}
