package net.ihe.gazelle.hl7.validator.rules.law;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Composite;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

public class OBXSegmentInLAW extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8388235260628347453L;
	private static final int OBX2 = 2;
	private static final int OBX5 = 5;
	private static final int OBX6 = 6;
	private static final int OBX18 = 18;
	private static final int OBX8 = 8;
	private static final int FIRST_REPEAT = 0;
	private static final int OBX11 = 11;

	private static Logger log = LoggerFactory.getLogger(OBXSegmentInLAW.class);

	@Override
	public ValidationException[] test(Message msg, final String hl7path) {
		String pathToTest = formatHl7Path(hl7path);
		Terser terser = new Terser(msg);
		String shortenPath = pathToTest.substring(7, pathToTest.lastIndexOf('/'));
		ValidationException exception = null;
		try {
			if (!pathToTest.contains("EI-") && !pathToTest.contains("CWE-")) {
				Segment obx = terser.getSegment(shortenPath);
				if (pathToTest.contains("OBX-2")) {
					testOBX2(obx);
				} else if (pathToTest.contains("OBX-5")) {
					testOBX5(obx);
				} else if (pathToTest.contains("OBX-6")) {
					testOBX6(obx);
				}
			} else {
				String pathToObx = shortenPath.substring(0, shortenPath.lastIndexOf('/'));
				Segment obx = terser.getSegment(pathToObx);
				if (pathToTest.contains("OBX-18")) {
					testOBX18(obx, pathToTest);
				} else if (pathToTest.contains("OBX-8")){
					testOBX8(obx, shortenPath);
				}
			}
		} catch (HL7Exception e) {
			exception = new ValidationException("Error when parsing message for validation: "
					+ e.getMessage());
			log.debug(exception.getMessage());
			return new ValidationException[] { exception };
		} catch (ValidationException e) {
			log.debug(e.getMessage());
			return new ValidationException[] { e };
		}
		return null;
	}

	private void testOBX2(Segment obx) throws ValidationException {
		try {
			// this first case may raise an error at message parsing and thus this point of the validation will never be reached
			if ((obx.getField(OBX5).length > 0) && obx.getField(OBX2, FIRST_REPEAT).encode().isEmpty()) {
				throw new ValidationException("Usage of OBX-2 is mandatory since OBX-5 is populated");
			} else if ((obx.getField(OBX5).length == 0) && !obx.getField(OBX2, FIRST_REPEAT).encode().isEmpty()) {
				throw new ValidationException("Usage of OBX-2 is not supported since OBX-5 is not populated");
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
		}
	}

	private void testOBX5(Segment obx) throws ValidationException {
		try {
			if (obx.getField(OBX11, FIRST_REPEAT).encode().equals("D") && (obx.getField(OBX5).length > 0)) {
				throw new ValidationException("Usage of OBX-5 is not supported since the value of OBX-11 is 'D'");
			} else if (!obx.getField(OBX11, FIRST_REPEAT).encode().equals("D") && (obx.getField(OBX5).length == 0)) {
				throw new ValidationException("Usage of OBX-5 is mandatory since the value of OBX-11 is not 'D'");
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
		}
	}

	private void testOBX6(Segment obx) throws ValidationException, HL7Exception {
		if ((obx.getField(OBX2, FIRST_REPEAT).encode().equals("NM") || obx.getField(OBX2, FIRST_REPEAT).encode().equals("SN"))
				&& obx.getField(OBX6, FIRST_REPEAT).encode().isEmpty()) {
			throw new ValidationException("Usage of OBX-6 is mandatory when OBX-2 is valued with either 'NM' or 'SN'");
		} else if (!(obx.getField(OBX2, FIRST_REPEAT).encode().equals("NM") || obx.getField(OBX2, FIRST_REPEAT).encode().equals("SN"))
				&& !obx.getField(OBX6, FIRST_REPEAT).encode().isEmpty()) {
			throw new ValidationException("Usage of OBX-6 is not supported when OBX-2 is not valued with 'NM' or 'SN'");
		}
	}

	private void testOBX18(Segment obx, String hl7path) throws ValidationException {
		try {
			String eiComponentAsString = hl7path.substring(hl7path.lastIndexOf('-') + 1);
			Integer eiComponent = Integer.decode(eiComponentAsString);
			String obx18repAsString = hl7path.substring(hl7path.indexOf("OBX-18(") + 7, hl7path.lastIndexOf(')'));
			Integer obx18rep = Integer.decode(obx18repAsString);
			Composite equipmentInstanceIdentifier = (Composite) obx.getField(OBX18, obx18rep);
			// start counting components from 0 ==> EI-3 getComponent(2) and EI-4 getComponent(3)
			if ((eiComponent == 3) && (obx18rep > 0) && !equipmentInstanceIdentifier.getComponent(2).encode().isEmpty()) {
				throw new ValidationException(
						"Universal ID (EI-3) is only supported in the first repeat of OBX-18; it is not supported in subsequent repeats");
			} else if ((eiComponent == 4) && (obx18rep > 0)
					&& !equipmentInstanceIdentifier.getComponent(3).encode().isEmpty()) {
				throw new ValidationException(
						"Universal ID Type (EI-4) is only supported in the first repeat of OBX-18; it is not supported in subsequent repeats");
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
		}
	}
	
	private void testOBX8(Segment obx, String hl7path) throws ValidationException {
		String obx8repAsString = hl7path.substring(hl7path.indexOf("OBX-8(") + 6, hl7path.lastIndexOf(')'));
		Integer obx8rep = Integer.decode(obx8repAsString);
		try {
			Composite interpretationCode = (Composite) obx.getField(OBX8, obx8rep);
			if (interpretationCode.getComponent(0).encode().equals("\"\"") && !interpretationCode.getComponent(2).encode().isEmpty()){
				throw new ValidationException("OBX-8-3 is not supported since OBX-8-1 is set to NULL");
			} else if (!interpretationCode.getComponent(0).encode().equals("\"\"") && interpretationCode.getComponent(2).encode().isEmpty()){
				throw new ValidationException("OBX-8-3 is required since OBX-8-1 is not NULL");
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage());
		}
	}

	@Override
	public ValidationException[] test(Message msg) {
		return null;
	}

	@Override
	public String getDescription() {
		return "Rules that apply to OBX segment in the context of the LAW profile";
	}

	@Override
	public String getSectionReference() {
		return "PaLM TF-2b, Rev. 9.0, Section C.7 OBX Segment";
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.USAGE.getCode();
	}

}
