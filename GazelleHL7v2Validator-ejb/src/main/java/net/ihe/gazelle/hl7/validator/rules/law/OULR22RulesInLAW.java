/*
 * Apache2 license - Copyright (c) IHE-Europe 2015
 *
 */

package net.ihe.gazelle.hl7.validator.rules.law;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aberge on 05/06/15.
 */
public class OULR22RulesInLAW extends IHEValidationRule {

    protected static final String RESULT = "RESULT";
    private static Logger log = LoggerFactory.getLogger(OULR22RulesInLAW.class);

    @Override
    public int getSeverity() {
        return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
    }

    @Override
    public ValidationException[] test(Message msg) {
        return null;
    }

    @Override
    public ValidationException[] test(Message msg, String hl7path) {
        Terser terser = new Terser(msg);
        String path = formatHl7Path(hl7path);
        try {
           if (hl7path.contains("RESULT")){
                testResultGroup(terser, path);
            }
        } catch (ValidationException e) {
            return new ValidationException[] { e };
        }
        return null;
    }

    private void testResultGroup(Terser terser, String path) throws ValidationException{
        try {
            Segment obx = terser.getSegment(path.substring(BEGIN_INDEX) + "/OBX(0)");
            String orcPath = path.substring(BEGIN_INDEX, path.indexOf(RESULT)) + "ORC(0)";
            Segment orc = terser.getSegment(orcPath);
            if (!orc.isEmpty()){
                String orc5 = orc.getField(5, 0).encode();
                if ((orc5.equals("CM") || orc5.equals("IP")) && obx.isEmpty()){
                    throw new ValidationException("Usage of RESULT group is mandatory when ORC-5 is set to " + orc5);
                } else if (!(orc5.equals("CM") || orc5.equals("IP")) && !obx.isEmpty()){
                    throw new ValidationException("Usage of RESULT group is forbidden when ORC-5 is neither set to CM nor IP");
                }
            }
        }catch (HL7Exception e){
            log.debug(e.getMessage());
        }
    }

    @Override
    public String getDescription() {
        return "The usage of a segments and groups in ORL_O42 messages exchanged in LAB-29";
    }

    @Override
    public String getSectionReference() {
        return "PaLM TF-2b, Rev. 9.0, Section 3.29.4.1.2 Message Semantics";
    }
}
