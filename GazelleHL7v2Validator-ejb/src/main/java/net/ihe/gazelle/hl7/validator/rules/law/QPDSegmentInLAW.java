package net.ihe.gazelle.hl7.validator.rules.law;

import ca.uhn.hl7v2.model.Composite;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.QPD;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

/**
 * <p>QPDSegmentInLAW class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class QPDSegmentInLAW extends IHEValidationRule {

	/**
	 *
	 */
	private static final long serialVersionUID = 499183817459654894L;
	/** Constant <code>WOS="WOS"</code> */
	public static final String WOS = "WOS";
	/** Constant <code>WOS_BY_ISOLATE="WOS_BY_ISOLATE"</code> */
	public static final String WOS_BY_ISOLATE = "WOS_BY_ISOLATE";
	/** Constant <code>WOS_BY_RACK="WOS_BY_RACK"</code> */
	public static final String WOS_BY_RACK = "WOS_BY_RACK";
	/** Constant <code>WOS_BY_TRAY="WOS_BY_TRAY"</code> */
	public static final String WOS_BY_TRAY = "WOS_BY_TRAY";

	private static Logger log = LoggerFactory.getLogger(QPDSegmentInLAW.class);

	/** {@inheritDoc} */
	@Override
	public ValidationException[] test(Message msg, String hl7path) {
		Terser terser = new Terser(msg);
		try {
			QPD qpd = (QPD) terser.getSegment("QPD");
			Composite qpd1 = (Composite) qpd.getField(1, 0);
			if (qpd1.encode().isEmpty() || qpd1.getComponent(1).isEmpty()){
				throw new ValidationException("Cannot check usage of " + hl7path + " because QPD-1 is not correctly populated");
			}else {
				String queryName = qpd1.getComponent(0).encode();
				log.debug(queryName);
				if (hl7path.contains("QPD-3")) {
					testQPD3(qpd, queryName);
				} else if (hl7path.contains("QPD-4")) {
					testQPDField(qpd, queryName, 4, WOS_BY_RACK);
				} else if (hl7path.contains("QPD-5")) {
					testQPDField(qpd, queryName, 5, WOS_BY_RACK);
				} else if (hl7path.contains("QPD-6")) {
					testQPDField(qpd, queryName, 6, WOS_BY_TRAY);
				} else if (hl7path.contains("QPD-7")) {
					testQPDField(qpd, queryName, 7, WOS_BY_TRAY);
				} else if (hl7path.contains("QPD-8")) {
					testQPD8(qpd, queryName);
				} else if (hl7path.contains("QPD-9")) {
					testQPDField(qpd, queryName, 9, WOS_BY_ISOLATE);
				}
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
			return null;
		} catch (ValidationException e) {
			log.debug(e.getMessage());
			return new ValidationException[] { e };
		}
		return null;
	}

	private void testQPD3(QPD qpdSegment, String queryName) throws ValidationException {
		try {
			log.debug(qpdSegment.getField(3, 0).encode());
			if ((queryName.equals(WOS) || queryName.equals(WOS_BY_ISOLATE))
					&& qpdSegment.getField(3, 0).encode().isEmpty()) {
				throw new ValidationException(
						"When QPD-1.1 is set to " + queryName+ ", QPD-3 is mandatory");
			} else if (!queryName.equals(WOS) && !queryName.equals(WOS_BY_ISOLATE)
					&& !qpdSegment.getField(3, 0).encode().isEmpty()) {
				throw new ValidationException(
						"When QPD-1.1 is set to " + queryName + " usage of QPD-3 is not supported");
			}
		} catch (HL7Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	private void testQPDField(QPD qpdSegment, String queryName, Integer fieldNumber, String expectedQueryName) throws ValidationException {
		try {
			if (queryName.equals(expectedQueryName) && qpdSegment.getField(fieldNumber).length == 0) {
				throw new ValidationException(
						"When QPD-1.1 is set to" + queryName+ ", usage of QPD-" + fieldNumber + " is mandatory");
			} else if (!queryName.equals(expectedQueryName) && qpdSegment.getField(fieldNumber).length > 0) {
				throw new ValidationException(
						"When QPD-1.1 is set to " + queryName +", usage of QPD-" + fieldNumber + " is not supported");
			}
		} catch (HL7Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	private void testQPD8(QPD qpdSegment, String queryName) throws ValidationException {
		try {
			if (!queryName.equals(WOS_BY_RACK) && !queryName.equals(WOS_BY_TRAY) && (qpdSegment.getField(8).length > 0)) {
				throw new ValidationException(
						"Usage of QPD-8 is not supported when not querying by tray or rack");
			}
		} catch (HL7Exception e) {
			log.warn(e.getMessage(), e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public ValidationException[] test(Message msg) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public String getDescription() {
		return "The usage of a given QPD field is related to other fields in the segment";
	}

	/** {@inheritDoc} */
	@Override
	public String getSectionReference() {
		return "PaLM TF-2b, Rev. 9.0, Section 3.27.4.1.2.3 QPD Segment Static Definition";
	}

	/** {@inheritDoc} */
	@Override
	public int getSeverity() {
		return GazelleErrorCode.USAGE.getCode();
	}

}
