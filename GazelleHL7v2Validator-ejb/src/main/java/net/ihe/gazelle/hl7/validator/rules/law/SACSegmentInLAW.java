package net.ihe.gazelle.hl7.validator.rules.law;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Composite;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

public class SACSegmentInLAW extends IHEValidationRule {

	/**
	 *
	 */
	private static final long serialVersionUID = 5141452897448437819L;

	private static Logger log = LoggerFactory.getLogger(SACSegmentInLAW.class);

	@Override public ValidationException[] test(Message msg) {
		return null;
	}

	@Override public String getDescription() {
		return "The usage of a given SAC field is related to other fields in the segment";
	}

	@Override public String getSectionReference() {
		return "PaLM TF-2b, Rev. 9.0, Section C.11 SAC Segment";
	}

	@Override public int getSeverity() {
		return GazelleErrorCode.USAGE.getCode();
	}

	@Override public ValidationException[] test(Message msg, final String hl7path) {
		String path = formatHl7Path(hl7path);
		Terser terser = new Terser(msg);
		try {
			Segment sac = terser.getSegment(path.substring(7, path.lastIndexOf('/')));
			if (path.endsWith("SAC-3") || path.endsWith("SAC-4")) {
				testSAC3and4(sac);
			} else if (path.endsWith("SAC-10") || path.endsWith("SAC-13")) {
				testSAC10and13(sac);
			} else if (path.endsWith("SAC-11")) {
				testSAC11(sac);
			} else if (path.endsWith("SAC-14")) {
				testSAC14(sac);
			} else if (path.endsWith("SAC-15")) {
				testSAC15(sac);
			} else if (path.endsWith("SAC-29")) {
				testSAC29Num2(sac);
			}
		} catch (ValidationException e) {
			ValidationException[] exceptions = { e };
			log.debug(e.getMessage());
			return exceptions;
		} catch (HL7Exception e) {
			log.debug(e.getMessage());
			ValidationException exception = new ValidationException(
					"Error when parsing message for validation: " + e.getMessage());
			return new ValidationException[] { exception };
		}
		return null;
	}

	private void testFieldIsEmpty(Segment sac, Integer fieldNumber) throws ValidationException {
		try {
			String field = sac.getField(fieldNumber, 0).encode();
			if (!field.isEmpty()) {
				throw new ValidationException("Usage of Field SAC-" + fieldNumber
						+ " is forbidden when the LAW_CONTAINER option is not supported");
			}
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
		}
	}

	private void testSAC3and4(Segment sacSegment) throws ValidationException {
		try {
			if (sacSegment.getField(3, 0).encode().isEmpty() && sacSegment.getField(4, 0).encode().isEmpty()) {
				throw new ValidationException("Either SAC-3 or SAC-4 or both SHALL be populated");
			}
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private void testSAC10and13(Segment sacSegment) throws ValidationException {
		try {
			if (sacSegment.getField(3, 0).encode().isEmpty() && !sacSegment.getField(4, 0).encode().isEmpty()
					&& sacSegment.getField(10, 0).encode().isEmpty() && sacSegment.getField(13, 0).encode().isEmpty()) {
				throw new ValidationException(
						"As SAC-3 is not populated and SAC-4 is populated, then SAC-10 or SAC-13 SHALL be populated");
			} else if (sacSegment.getField(4, 0).encode().isEmpty() && (!sacSegment.getField(10, 0).encode().isEmpty()
					|| !sacSegment.getField(13, 0).encode().isEmpty())) {
				throw new ValidationException("As SAC-4 is not populated, then SAC-10 and SAC-13 are not supported");
			}
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private void testSAC11(Segment sacSegment) throws ValidationException {
		try {
			if (!sacSegment.getField(10, 0).encode().isEmpty() && sacSegment.getField(11, 0).encode().isEmpty()) {
				throw new ValidationException("As SAC-10 is populated, then SAC-11 SHALL be populated");
			} else if (sacSegment.getField(10, 0).encode().isEmpty() && !sacSegment.getField(11, 0).encode()
					.isEmpty()) {
				throw new ValidationException("As SAC-10 is not populated, then SAC-11 is not supported");
			}
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private void testSAC14(Segment sacSegment) throws ValidationException {
		try {
			if (!sacSegment.getField(13, 0).encode().isEmpty() && sacSegment.getField(14, 0).encode().isEmpty()) {
				throw new ValidationException("As SAC-13 is populated, then SAC-14 SHALL be populated");
			} else if (sacSegment.getField(13, 0).encode().isEmpty() && !sacSegment.getField(14, 0).encode()
					.isEmpty()) {
				throw new ValidationException("As SAC-13 is not populated, then SAC-14 is not supported");
			}
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private void testSAC15(Segment sacSegment) throws ValidationException {
		try {
			if (sacSegment.getField(10, 0).encode().isEmpty() && sacSegment.getField(13, 0).encode().isEmpty()
					&& !sacSegment.getField(15, 0).encode().isEmpty()) {
				throw new ValidationException("As neither SAC-10 nor SAC-13 is present, then SAC-15 is not supported");
			}
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private void testSAC29Num2(Segment sacSegment) throws ValidationException {
		try {
			Composite dilutionFactor = (Composite) sacSegment.getField(29, 0);
			if (!dilutionFactor.encode().isEmpty()) {
				// Num2 is the 4th component of SN type
				String value = dilutionFactor.getComponent(3).encode();
				if (value != null) {
					Integer intValue = Integer.decode(value);
					if ((intValue < 2) || (intValue > 999)) {
						throw new ValidationException("Num2 SHALL be an Integer between 2 and 999");
					}
				} else {
					throw new ValidationException("Num2 SHALL be present");
				}
			}
		} catch (NumberFormatException e) {
			throw new ValidationException("Num2 SHALL be an integer between 2 and 999", e);
		} catch (HL7Exception e) {
			log.debug(e.getMessage(), e);
		}
	}
}
