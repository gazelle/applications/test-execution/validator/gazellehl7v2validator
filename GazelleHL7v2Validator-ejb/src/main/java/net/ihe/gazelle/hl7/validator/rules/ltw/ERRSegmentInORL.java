package net.ihe.gazelle.hl7.validator.rules.ltw;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;

public class ERRSegmentInORL extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2908527572315894911L;

	public ERRSegmentInORL() {
		super();
	}

	@Override
	public String getDescription() {
		return "The ERR segment shall be used in case of negative acknowledgement (when MSA-1= AE or AR)";
	}

	@Override
	public String getSectionReference() {
		return "Laboratory TF-2 - Section 4.5.4";
	}

	@Override
	public ValidationException[] test(Message msg) {
		Terser terser = new Terser(msg);
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			String ack = terser.get("/.MSA-1");
			if ((ack != null) && (ack.equals("AE") || ack.equals("AR"))) {
				Structure[] errorSegments = msg.getAll("ERR");
				if ((errorSegments == null) || (errorSegments.length == 0)) {
					exceptions.add(new ValidationException("The ERR segment shall be present in this message"));
				}
			} else {
				return null;
			}
		} catch (HL7Exception e) {
			exceptions.add(new ValidationException(e.getMessage()));
		}
		return exceptions.toArray(new ValidationException[exceptions.size()]);
	}

	@Override
	public int getSeverity() {
		return GazelleErrorCode.SEGMENT_SEQUENCE_ERROR.getCode();
	}
}
