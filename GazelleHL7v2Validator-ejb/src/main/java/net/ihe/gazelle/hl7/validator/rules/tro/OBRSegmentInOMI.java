package net.ihe.gazelle.hl7.validator.rules.tro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.util.Terser;
import ca.uhn.hl7v2.validation.ValidationException;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;

/**
 * <p>OBRSegmentInOMI class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class OBRSegmentInOMI extends IHEValidationRule {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2226992100543433809L;

	private static Logger log = LoggerFactory.getLogger(OBRSegmentInOMI.class);

	/** {@inheritDoc} */
	@Override
	public ValidationException[] test(Message msg) {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public ValidationException[] test(Message msg, final String hl7path) {
		String path = formatHl7Path(hl7path);
		Terser terser = new Terser(msg);
		try {
			Segment obr = terser.getSegment(path.substring(7, path.lastIndexOf('/')));
			// remove OBR from path to get the ORC segment instead
			String pathToORC = path.substring(7, path.lastIndexOf("OBR")).replace("OBR", "ORC(0)");
			Segment orc = terser.getSegment(pathToORC);
			if (orc.getField(5, 0).encode().equals("DC") && obr.getField(20, 0).isEmpty()) {
				ValidationException exception = new ValidationException(
						"Filler Field 1 (Abort reason) SHALL be populated when ORC-5 = DC");
				ValidationException[] exceptions = { exception };
				return exceptions;
			} else {
				return null;
			}
		} catch (HL7Exception e) {
			ValidationException exception = new ValidationException("Error when parsing message for validation: "
					+ e.getMessage());
			log.debug(e.getMessage());
			ValidationException[] exceptions = { exception };
			return exceptions;
		}
	}

	/** {@inheritDoc} */
	@Override
	public String getDescription() {
		return "If the field ORC-5 Order Status conveys the value 'DC' (i.e., aborted), the OBR-20 Filler Field 1 " 
				+ "SHALL convey a coded value from a list of typical reasons to describe the reason the order was aborted.";
	}

	/** {@inheritDoc} */
	@Override
	public String getSectionReference() {
		return "TRO-124";
	}

	/** {@inheritDoc} */
	@Override
	public int getSeverity() {
		return GazelleErrorCode.USAGE.getCode();
	}

}
