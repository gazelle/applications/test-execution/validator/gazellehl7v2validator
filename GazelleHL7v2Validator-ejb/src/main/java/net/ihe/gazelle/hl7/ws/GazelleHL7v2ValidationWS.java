/*
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.ihe.gazelle.hl7.ws;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hl7.validator.core.HL7Validator;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.SOAPException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

@Stateless
@Name("gazelleHL7v2ValidationWS")
@WebService(name = "gazelleHL7v2ValidationWS", serviceName = "gazelleHL7v2ValidationWSService")
@GenerateInterface(value = "GazelleHL7v2ValidationWSRemote", isLocal = false, isRemote = true)
public class GazelleHL7v2ValidationWS implements GazelleHL7v2ValidationWSRemote {

    MetadataServiceProvider metadataServiceProvider;
    @Resource
    WebServiceContext jaxwsContext;

    /**
     * Validates HL7v2.x messages using the HAPI validation mechanism
     */
    @Override
    @WebMethod
    @WebResult(name = "result")
    public String validateMessage(@WebParam(name = "xmlValidationMetadata") String xmlValidationMetadata,
                                  @WebParam(name = "xmlValidationContext") String xmlValidationContext,
                                  @WebParam(name = "messageToValidate") String messageToValidate) throws SOAPException {
        System.setProperty(DOMImplementationRegistry.PROPERTY, org.apache.xerces.dom.DOMImplementationSourceImpl.class.getCanonicalName());
        if ((xmlValidationContext == null) || xmlValidationContext.isEmpty()) {
            throw new SOAPException("You must provide the message context (XML string)");
        }
        if ((messageToValidate == null) || messageToValidate.isEmpty()) {
            throw new SOAPException("You must specify a message to validate");
        }
        Lifecycle.beginCall();
        HttpServletRequest hRequest = (HttpServletRequest) jaxwsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        HL7Validator validator = new HL7Validator(hRequest.getRemoteAddr(), xmlValidationMetadata, messageToValidate,
                xmlValidationContext, getMetadataServiceProvider());
        String result = validator.validate();
        Lifecycle.endCall();
        return result;
    }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String toolVersion = getMetadataServiceProvider().getMetadata().getVersion();
        StringBuilder about = new StringBuilder();
        about.append("GazelleHL7v2Validator");
        if (toolVersion != null) {
            about.append(" (");
            about.append(toolVersion);
            about.append(")");
        }
        about.append(" is a validation service for HL7v2.x messages, based on HAPI libraries, read more on http://gazelle.ihe.net");
        return about.toString();
    }

    private MetadataServiceProvider getMetadataServiceProvider() {
        if (metadataServiceProvider == null) {
            metadataServiceProvider = (MetadataServiceProvider) Component.getInstance("metadataServiceProvider");
        }
        return metadataServiceProvider;
    }

}
