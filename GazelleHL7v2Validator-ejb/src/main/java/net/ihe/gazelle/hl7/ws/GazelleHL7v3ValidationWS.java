package net.ihe.gazelle.hl7.ws;

import net.ihe.gazelle.hl7v3.validator.core.GazelleHL7v3Validator;
import net.ihe.gazelle.hl7v3.validator.core.HL7V3ValidatorType;
import net.ihe.gazelle.hl7v3.validator.core.SAXParserFactoryProvider;
import net.ihe.gazelle.hl7v3.validator.core.SAXParserFactoryProviderLocal;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.validator.xcpd.plq.GazelleXCPDPLQValidator;
import net.ihe.gazelle.validator.xcpd.plq.XCPDPLQValidatorType;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS", serviceName = "ModelBasedValidationWSService", portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class GazelleHL7v3ValidationWS extends AbstractModelBasedValidation {

    MetadataServiceProvider metadataServiceProvider;


    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
        DetailedResult result = new DetailedResult();
        XSDMessage warning = new XSDMessage();
        warning.setLineNumber(0);
        warning.setMessage(e.getMessage());
        result.setDocumentValidXSD(new DocumentValidXSD());
        result.getDocumentValidXSD().getXSDMessage().add(warning);
        result.getDocumentValidXSD().setResult("FAILED");
        GazelleHL7v3Validator.addResultHeader(result, (HL7V3ValidatorType) selectedValidator);
        return GazelleHL7v3Validator.getDetailedResultAsString(result);
    }

    @Override
    protected String executeValidation(String theDocument, ValidatorDescription validatorDescription, boolean extracted) throws GazelleValidationException {
        if (validatorDescription instanceof HL7V3ValidatorType) {
            HL7V3ValidatorType validatorType = (HL7V3ValidatorType) validatorDescription;
            Lifecycle.beginCall();
            SAXParserFactoryProviderLocal factoryProvider = SAXParserFactoryProvider.instance();
            DetailedResult result = null;
            GazelleHL7v3Validator hl7v3Validator = null;
            GazelleXCPDPLQValidator xcpdplqValidator;
            switch (validatorType) {
                case PDQV3_QUERY:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case PDQV3_RESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case CH_PDQCONTINUATION:
                case PDQV3_CONTINUATION:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getQuqiIN000003UV01Factory(),
                            factoryProvider.getQuqiIN000003UV01Xsd());
                    break;
                case CH_PDQACKNOWLEDGEMENT:
                case PDQV3_ACKNOWLEDGEMENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getMcciIN000002UV01Factory(),
                            factoryProvider.getMcciIN000002UV01Xsd());
                    break;
                case CH_PDQCANCELLATION:
                case PDQV3_CANCELLATION:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getQuqiIN000003UV01Factory(),
                            factoryProvider.getQuqiIN000003UV01Xsd());
                    break;
                case XCPD_QUERYREQUEST:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case XCPD_QUERYDEFERRED:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case KSA_KPDQV3QUERY:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case KSA_KPDQV3QUERYRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case PIXV3_ADDPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201301UV02Factory(),
                            factoryProvider.getPrpaIN201301UV02Xsd());
                    break;
                case PIXV3_MERGEPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201304UV02Factory(),
                            factoryProvider.getPrpaIN201304UV02Xsd());
                    break;
                case PIXV3_QUERY:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201309UV02Factory(),
                            factoryProvider.getPrpaIN201309UV02Xsd());
                    break;
                case PIXV3_QUERYRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201310UV02Factory(),
                            factoryProvider.getPrpaIN201310UV02Xsd());
                    break;
                case PIXV3_REVISEPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201302UV02Factory(),
                            factoryProvider.getPrpaIN201302UV02Xsd());
                    break;
                case PIXV3_ITI44ACK:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getMcciIN000002UV01Factory(),
                            factoryProvider.getMcciIN000002UV01Xsd());
                    break;
                case CH_PIXITI46ACK:
                case PIXV3_ITI46ACK:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getMcciIN000002UV01Factory(),
                            factoryProvider.getMcciIN000002UV01Xsd());
                    break;
                case CH_PIXUPDATENOTIFICATION:
                case PIXV3_UPDATENOTIFICATION:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201302UV02Factory(),
                            factoryProvider.getPrpaIN201302UV02Xsd());
                    break;
                case CH_XCPDACK:
                case XCPD_ACK:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getMcciIN000002UV01Factory(),
                            factoryProvider.getMcciIN000002UV01Xsd());
                    break;
                case XCPD_QUERYRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case CH_PDQQUERY:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case CH_PDQRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case CH_PIXFEED_ACKNOWLEDGEMENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getMcciIN000002UV01Factory(),
                            factoryProvider.getMcciIN000002UV01Xsd());
                    break;
                case CH_PIXFEED_ADDPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201301UV02Factory(),
                            factoryProvider.getPrpaIN201301UV02Xsd());
                    break;
                case CH_PIXFEED_REVISEPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201302UV02Factory(),
                            factoryProvider.getPrpaIN201302UV02Xsd());
                    break;
                case CH_PIXFEED_MERGEPATIENT:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201304UV02Factory(),
                            factoryProvider.getPrpaIN201304UV02Xsd());
                    break;
                case CH_PIXQUERY:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201309UV02Factory(),
                            factoryProvider.getPrpaIN201309UV02Xsd());
                    break;
                case CH_PIXQUERYRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201310UV02Factory(),
                            factoryProvider.getPrpaIN201310UV02Xsd());
                    break;
                case CH_XCPDQUERYREQUEST:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case CH_XCPDQUERYDEFERRED:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case CH_XCPDQUERYRESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case EHDSI_XCPD_REQUEST:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201305UV02Factory(),
                            factoryProvider.getPrpaIN201305UV02Xsd());
                    break;
                case EHDSI_XCPD_RESPONSE:
                    hl7v3Validator = new GazelleHL7v3Validator(factoryProvider.getPrpaIN201306UV02Factory(),
                            factoryProvider.getPrpaIN201306UV02Xsd());
                    break;
                case XCPD_PLQ_REQUEST:
                    xcpdplqValidator = new GazelleXCPDPLQValidator(
                            factoryProvider.getXcpdPlqXsdFactory(), factoryProvider.getXcpdPlqXsd());
                    result = xcpdplqValidator.validate(theDocument, XCPDPLQValidatorType.XCPD_PLQ_REQUEST);
                    break;
                case XCPD_PLQ_RESPONSE:
                    xcpdplqValidator = new GazelleXCPDPLQValidator(
                            factoryProvider.getXcpdPlqXsdFactory(), factoryProvider.getXcpdPlqXsd());
                    result = xcpdplqValidator.validate(theDocument, XCPDPLQValidatorType.XCPD_PLQ_RESPONSE);
                    break;
                default:
                    // this case should not appear
                    return null;
            }
            if (hl7v3Validator != null && result == null) {
                result = hl7v3Validator.validate(theDocument, validatorType);
            }
            Lifecycle.endCall();
            if (result != null) {
                if (extracted){
                    // If the result has been extracted from a document, we need to report it. The line numbers might not match
                    String nodeName = validatorDescription.getNamespaceURI() + ":" + validatorDescription.getRootElement();
                    Warning warning = new Warning();
                    warning.setTest("root_element_qname");
                    warning.setLocation("/");
                    warning.setDescription("The validation process has only been applied on the " + nodeName +
                            " element. Line numbers and XPath in this report might differ from the actual number in the submitted file");
                    result.getMDAValidation().getWarningOrErrorOrNote().add(0, warning);
                }
                String stringResult = GazelleHL7v3Validator.getDetailedResultAsString(result);
                Lifecycle.beginCall();
                addValidatorUsage(validatorType.getLabel(), result.getValidationResultsOverview().getValidationTestResult());
                Lifecycle.endCall();
                return stringResult;
            } else {
                throw new GazelleValidationException(
                        "The validation has ended in an unexpected way, please contact the tool administrator if the error persists");
            }
        }
        return null;
    }

    @Override
    protected ValidatorDescription getValidatorByOidOrName(String oidOrName) {
        return HL7V3ValidatorType.getValidatorTypeByLabel(oidOrName);
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        HL7V3ValidatorType[] validators = HL7V3ValidatorType.values();
        List<ValidatorDescription> availableValidators = new ArrayList<ValidatorDescription>();
        for (HL7V3ValidatorType validatorType : validators) {
            if (descriminator == null || descriminator.isEmpty() || descriminator.equals(validatorType.getDescriminator())) {
                availableValidators.add(validatorType);
            }
        }
        return availableValidators;
    }

    @Override
    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String toolVersion = getMetadataServiceProvider().getMetadata().getVersion();
        StringBuilder about = new StringBuilder();
        about.append("GazelleHL7v3Validator");
        if (toolVersion != null) {
            about.append(" (" + toolVersion + ")");
        }
        about.append(" is a model-based validation service for HL7v3 messages, read more on https://gazelle.ihe.net");
        return about.toString();
    }

    private MetadataServiceProvider getMetadataServiceProvider() {
        if (metadataServiceProvider == null) {
            metadataServiceProvider = (MetadataServiceProvider) Component.getInstance("metadataServiceProvider");
        }
        return metadataServiceProvider;
    }

}
