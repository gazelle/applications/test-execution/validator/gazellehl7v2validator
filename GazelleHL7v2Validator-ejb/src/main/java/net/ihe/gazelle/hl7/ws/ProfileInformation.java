package net.ihe.gazelle.hl7.ws;

import net.ihe.gazelle.hl7.messageprofiles.model.Profile;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Name("ProfileInformation")
public class ProfileInformation implements ProfileInformationLocal {

	@Override
	public String getJavaPackageForProfileByOID(String profileOID) {
		if ((profileOID != null) && !profileOID.isEmpty()) {
			Lifecycle.beginCall();
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			Profile profile = Profile.getProfileByOID(profileOID, entityManager);
			Lifecycle.endCall();
			if (profile != null) {
				return profile.getJavaPackage();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
