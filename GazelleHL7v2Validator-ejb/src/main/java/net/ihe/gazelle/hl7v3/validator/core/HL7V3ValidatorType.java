package net.ihe.gazelle.hl7v3.validator.core;

import net.ihe.gazelle.hl7v3.mcciin000002UV01.MCCIIN000002UV01Type;
import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.prpain201304UV02.PRPAIN201304UV02Type;
import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01CancelType;
import net.ihe.gazelle.hl7v3.quqiin000003UV01.QUQIIN000003UV01Type;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryRequestType;
import net.ihe.gazelle.xcpd.plq.PatientLocationQueryResponseType;


/**
 * <p>ValidatorType class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum HL7V3ValidatorType implements ValidatorDescription {

    PDQV3_QUERY("[ITI-47] Patient Demographics Query", "IHE", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),
    PDQV3_RESPONSE("[ITI-47] Patient Demographics Query Response", "IHE", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),
    PDQV3_CONTINUATION("[ITI-47] Patient Demographics Query HL7V3 Continuation", "IHE", QUQIIN000003UV01Type.class, "QUQI_IN000003UV01"),
    PDQV3_ACKNOWLEDGEMENT("[ITI-47] Accept Acknowledgement", "IHE", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    PDQV3_CANCELLATION("[ITI-47] Patient Demographics Query HL7V3 Cancellation", "IHE", QUQIIN000003UV01CancelType.class, "QUQI_IN000003UV01_Cancel"),
    PIXV3_ADDPATIENT("[ITI-44] Patient Identity Feed HL7V3 - Add Patient Record", "IHE", PRPAIN201301UV02Type.class, "PRPA_IN201301UV02"),
    PIXV3_REVISEPATIENT("[ITI-44] Patient Identity Feed HL7V3 - Revise Patient Record", "IHE", PRPAIN201302UV02Type.class, "PRPA_IN201302UV02"),
    PIXV3_MERGEPATIENT("[ITI-44] Patient Identity Feed HL7V3 - Patient Identity Merge", "IHE", PRPAIN201304UV02Type.class, "PRPA_IN201304UV02"),
    PIXV3_ITI44ACK("[ITI-44] Patient Identity Feed HL7V3 - Acknowledgement", "IHE", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    PIXV3_QUERY("[ITI-45] PIXV3 Query", "IHE", PRPAIN201309UV02Type.class, "PRPA_IN201309UV02"),
    PIXV3_QUERYRESPONSE("[ITI-45] PIXV3 Query Response", "IHE", PRPAIN201310UV02Type.class, "PRPA_IN201310UV02"),
    PIXV3_UPDATENOTIFICATION("[ITI-46] PIXV3 Update Notification", "IHE", PRPAIN201302UV02Type.class, "PRPA_IN201302UV02"),
    PIXV3_ITI46ACK("[ITI-46] PIXV3 Update Notification acknowledgement", "IHE", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    XCPD_QUERYREQUEST("[ITI-55] Cross Gateway Patient Discovery Request", "IHE", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),
    XCPD_QUERYDEFERRED("[ITI-55] Cross Gateway Patient Discovery Request (Deferred option)", "IHE", PRPAIN201305UV02Type.class,
            "PRPA_IN201305UV02"),
    XCPD_QUERYRESPONSE("[ITI-55] Cross Gateway Patient Discovery Response", "IHE", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),
    XCPD_ACK("[ITI-55] Accept Acknowledgement", "IHE", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),

    KSA_KPDQV3QUERY("KPDQ - Patient Demographics Query", "KSA", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),
    KSA_KPDQV3QUERYRESPONSE("KPDQ - Patient Demographics Query Response", "KSA", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),

    CH_PDQQUERY("[CH:ITI-47] - Patient Demographics Query", "CH", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),
    CH_PDQRESPONSE("[CH:ITI-47] - Patient Demographics Query Response", "CH", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),
    CH_PDQCONTINUATION("[CH:ITI-47] - Patient Demographics Query HL7V3 Continuation", "CH", QUQIIN000003UV01Type.class, "QUQI_IN000003UV01"),
    CH_PDQACKNOWLEDGEMENT("[CH:ITI-47] - Accept Acknowledgement", "CH", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    CH_PDQCANCELLATION("[CH:ITI-47] - Patient Demographics Query HL7V3 Cancellation", "CH", QUQIIN000003UV01CancelType.class,
            "QUQI_IN000003UV01_Cancel"),
    CH_PIXFEED_ACKNOWLEDGEMENT("[CH:ITI-44] - Patient Identity Feed HL7V3 - Acknowledgement", "CH", MCCIIN000002UV01Type.class,
            "MCCI_IN000002UV01"),
    CH_PIXFEED_ADDPATIENT("[CH:ITI-44] - Patient Identity Feed HL7V3 - Add Patient Record", "CH", PRPAIN201301UV02Type.class, "PRPA_IN201301UV02"),
    CH_PIXFEED_REVISEPATIENT("[CH:ITI-44] - Patient Identity Feed HL7V3 - Patient Revise Record", "CH", PRPAIN201302UV02Type.class,
            "PRPA_IN201302UV02"),
    CH_PIXFEED_MERGEPATIENT("[CH:ITI-44] - Patient Identity Feed HL7V3 - Patient Identity Merge", "CH", PRPAIN201304UV02Type.class,
            "PRPA_IN201304UV02"),
    CH_PIXQUERY("[CH:ITI-45] - PIX Query", "CH", PRPAIN201309UV02Type.class, "PRPA_IN201309UV02"),
    CH_PIXQUERYRESPONSE("[CH:ITI-45] - PIX Query Response", "CH", PRPAIN201310UV02Type.class, "PRPA_IN201310UV02"),
    CH_PIXUPDATENOTIFICATION("[CH:ITI-46] - PIX Update Notification", "CH", PRPAIN201302UV02Type.class, "PRPA_IN201302UV02"),
    CH_PIXITI46ACK("[CH:ITI-46] - PIX Update Notification acknowledgement", "CH", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    CH_XCPDQUERYREQUEST("[CH:ITI-55] - Cross Gateway Patient Discovery Request", "CH", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),
    CH_XCPDQUERYDEFERRED("[CH:ITI-55] - Cross Gateway Patient Discovery Request (Deferred option)", "CH", PRPAIN201305UV02Type.class,
            "PRPA_IN201305UV02"),
    CH_XCPDQUERYRESPONSE("[CH:ITI-55] - Cross Gateway Patient Discovery Response", "CH", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),

    EHDSI_XCPD_REQUEST("[eHDSI HL7v3] - Identification Service Request", "EHDSI", PRPAIN201305UV02Type.class, "PRPA_IN201305UV02"),

    EHDSI_XCPD_RESPONSE("[eHDSI HL7v3] - Identification Service Response", "EHDSI", PRPAIN201306UV02Type.class, "PRPA_IN201306UV02"),
    CH_XCPDACK("[CH:ITI-55] - Accept Acknowledgement", "CH", MCCIIN000002UV01Type.class, "MCCI_IN000002UV01"),
    XCPD_PLQ_REQUEST("[ITI-56] Patient Location Query Request", "IHE", PatientLocationQueryRequestType.class, "PatientLocationQueryRequest"),
    XCPD_PLQ_RESPONSE("[ITI-56] Patient Location Query Response", "IHE", PatientLocationQueryResponseType.class, "PatientLocationQueryResponse");

    String label;
    String descriminator;
    Class<?> typeClass;
    String rootElement;

    /**
     * <p>Getter for the field <code>typeClass</code>.</p>
     *
     * @return a {@link java.lang.Class} object.
     */
    public Class<?> getTypeClass() {
        return typeClass;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    /**
     * <p>Getter for the field <code>descriminator</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescriminator() {
        return descriminator;
    }

    HL7V3ValidatorType(String label, String descriminator, Class<?> clazz, String rootElement) {
        this.label = label;
        this.typeClass = clazz;
        this.descriminator = descriminator;
        this.rootElement = rootElement;
    }


    public String getName() {
        return this.label;
    }

    public String getOid() {
        // TODO assigned OIDs to validators
        return null;
    }

    public String getRootElement() {
        return this.rootElement;
    }

    public boolean extractPartToValidate() {
        return true;
    }

    public String getNamespaceURI() {
        return "urn:hl7-org:v3";
    }

    /**
     * <p>getValidatorTypeByLabel.</p>
     *
     * @param inLabel a {@link java.lang.String} object.
     *
     * @return a {@link HL7V3ValidatorType} object.
     */
    public static HL7V3ValidatorType getValidatorTypeByLabel(String inLabel) {
        HL7V3ValidatorType[] validatorTypes = HL7V3ValidatorType.values();
        for (HL7V3ValidatorType validatorType : validatorTypes) {
            if (validatorType.getLabel().equalsIgnoreCase(inLabel)) {
                return validatorType;
            } else {
                continue;
            }
        }
        return null;
    }
}
