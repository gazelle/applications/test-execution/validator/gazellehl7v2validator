package net.ihe.gazelle.hl7v3.validator.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import net.ihe.gazelle.validation.model.ValidatorUsageProvider;

import javax.validation.constraints.NotNull;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.kohsuke.MetaInfServices;

/**
 * <b>Class description</b>: ValidatorUsage
 * 
 * This class is used to be able to report statistics on the usage of the validator embedded in the validator
 * 
 * @author Anne-Gaëlle Bergé / IHE Europe
 * @version 1.0 - August 3rd 2012
 * 
 */

@Entity
@Name("validatorUsage")
@Table(name = "hl7v3_validator_usage", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "hl7v3_validator_usage_sequence", sequenceName = "hl7v3_validator_usage_id_seq", allocationSize = 1)
@MetaInfServices(ValidatorUsageProvider.class)
public class ValidatorUsage implements ValidatorUsageProvider, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1982913201847146575L;

	@Id
	@GeneratedValue(generator = "hl7v3_validator_usage_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "validation_date")
	@Temporal(TemporalType.DATE)
	private Date validationDate;

	@Column(name = "status")
	private String status;

	@Column(name = "type")
	private String type;

	@Column(name = "caller")
	private String caller;

	public ValidatorUsage() {

	}

	public ValidatorUsage(Date validationDate, String validationStatus, String validationType, String caller) {
		this.validationDate = validationDate;
		this.status = validationStatus;
		this.caller = caller;
		this.type = validationType;
	}

	public void save() {
		EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
		entityManager.merge(this);
		entityManager.flush();
	}

	public void setValidationDate(Date date) {
		this.validationDate = date;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	@Override
	public void newEntry(Date date, String status, String type, String caller) {
		ValidatorUsage usage = new ValidatorUsage(date, status, type, caller);
		usage.save();
	}

}
