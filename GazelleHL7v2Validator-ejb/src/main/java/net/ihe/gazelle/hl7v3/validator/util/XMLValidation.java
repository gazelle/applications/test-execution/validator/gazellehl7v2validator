package net.ihe.gazelle.hl7v3.validator.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>XMLValidation class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XMLValidation {

	private static Logger log = LoggerFactory.getLogger(XMLValidation.class);

	/** Constant <code>factoryBASIC</code> */
	protected static SAXParserFactory factoryBASIC;
	protected SAXParserFactory factory;
	protected String xsdLocation;

	static {
		try {
			factoryBASIC = SAXParserFactory.newInstance();
			factoryBASIC.setValidating(false);
			factoryBASIC.setNamespaceAware(true);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * <p>Constructor for XMLValidation.</p>
	 *
	 * @param customFactory a {@link javax.xml.parsers.SAXParserFactory} object.
	 * @param xsdLocation a {@link java.lang.String} object.
	 */
	public XMLValidation(SAXParserFactory customFactory, String xsdLocation) {
		this.factory = customFactory;
		this.xsdLocation = xsdLocation;
	}

	/**
	 * <p>initializeFactory.</p>
	 *
	 * @param xsdLocation a {@link java.lang.String} object.
	 * @return a {@link javax.xml.parsers.SAXParserFactory} object.
	 */
	public static SAXParserFactory initializeFactory(String xsdLocation) {
		SAXParserFactory customFactory = null;
		try {
			customFactory = SAXParserFactory.newInstance();
			customFactory.setNamespaceAware(true);
			SchemaFactory sfactory = SchemaFactory
					.newInstance("http://www.w3.org/2001/XMLSchema");
			Schema schema = sfactory.newSchema(new File(xsdLocation));
			customFactory.setSchema(schema);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return customFactory;
	}

	/**
	 * Parses the file using SAX to check that it is a well-formed XML file
	 *
	 * @param string a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.validation.DocumentWellFormed} object.
	 */
	public DocumentWellFormed isXMLWellFormed(String string) {
		return validateIfDocumentWellFormedXML(string, "");
	}

	/**
	 * <p>validateIfDocumentWellFormedXML.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param xsdpath a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.validation.DocumentWellFormed} object.
	 */
	protected DocumentWellFormed validateIfDocumentWellFormedXML(
			String message, String xsdpath) {
		DocumentWellFormed documentWellFormedResult = new DocumentWellFormed();
		return validXMLUsingXSD(message, xsdpath, factory, documentWellFormedResult);
	}
	
	/**
	 * <p>isXSDValid.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.validation.DocumentValidXSD} object.
	 */
	public DocumentValidXSD isXSDValid(String message)
	{
		if (message == null){
			return null;
		}
		else {
			return validXMLUsingXSD(message, xsdLocation, factory);
		}
	}

	/**
	 * <p>validXMLUsingXSD.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param xsdpath a {@link java.lang.String} object.
	 * @param factory a {@link javax.xml.parsers.SAXParserFactory} object.
	 * @return a {@link net.ihe.gazelle.validation.DocumentValidXSD} object.
	 */
	protected DocumentValidXSD validXMLUsingXSD(String message, String xsdpath, SAXParserFactory factory)
	{
		DocumentValidXSD documentValidXSDResult = new DocumentValidXSD();
		return validXMLUsingXSD(message, xsdpath, factory, documentValidXSDResult);
	}
	
	/**
	 * <p>validXMLUsingXSD.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param xsdpath a {@link java.lang.String} object.
	 * @param factory a {@link javax.xml.parsers.SAXParserFactory} object.
	 * @param documentValidResult a T object.
	 * @param <T> a T object.
	 * @return a T object.
	 */
	protected static <T extends DocumentValidXSD> T validXMLUsingXSD(
			String message, String xsdpath, SAXParserFactory factory,
			T documentValidResult) {
		List<ValidationException> exceptions = new ArrayList<ValidationException>();
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(
					message.getBytes("UTF-8"));
			exceptions = XSDValidator.validateUsingFactoryAndSchema(bais,
					xsdpath, factory);
		} catch (Exception e) {
			exceptions.add(handleException(e));
		}
		return extractValidationResult(exceptions, xsdpath, factory,
				documentValidResult);
	}

	/**
	 * <p>extractValidationResult.</p>
	 *
	 * @param exceptions a {@link java.util.List} object.
	 * @param xsdpath a {@link java.lang.String} object.
	 * @param factory a {@link javax.xml.parsers.SAXParserFactory} object.
	 * @param dv a T object.
	 * @param <T> a T object.
	 * @return a T object.
	 */
	protected static <T extends DocumentValidXSD> T extractValidationResult(
			List<ValidationException> exceptions, String xsdpath,
			SAXParserFactory factory, T dv) {

		dv.setResult("PASSED");

		if (exceptions == null || exceptions.size() == 0) {
			dv.setNbOfErrors("0");
			dv.setResult("PASSED");
			return dv;
		} else {
			Integer nbOfErrors = 0;
			Integer nbOfWarnings = 0;
			Integer exceptionCounter = 0;
			for (ValidationException ve : exceptions) {
				if (ve.getSeverity() == null) {
					ve.setSeverity("error");
				}
				exceptionCounter++;
				if ((ve.getSeverity() != null)
						&& (ve.getSeverity().equals("warning"))) {
					nbOfWarnings++;
				} else {
					nbOfErrors++;
				}
				XSDMessage xsd = new XSDMessage();
				xsd.setMessage(ve.getMessage());
				xsd.setSeverity(ve.getSeverity());
				if (StringUtils.isNumeric(ve.getLineNumber())) {
					xsd.setLineNumber(Integer.valueOf(ve.getLineNumber()));
				}
				if (StringUtils.isNumeric(ve.getColumnNumber())) {
					xsd.setColumnNumber(Integer.valueOf(ve.getColumnNumber()));
				}
				dv.getXSDMessage().add(xsd);
			}
			dv.setNbOfErrors(nbOfErrors.toString());
			dv.setNbOfWarnings(nbOfWarnings.toString());
			if (nbOfErrors > 0) {
				dv.setResult("FAILED");
			}
			return dv;
		}

	}

	private static ValidationException handleException(Exception e) {
		ValidationException ve = new ValidationException();
		ve.setLineNumber("0");
		ve.setColumnNumber("0");
		if (e != null && e.getMessage() != null) {
			ve.setMessage("error on validating : " + e.getMessage());
		} else if (e != null && e.getCause() != null
				&& e.getCause().getMessage() != null) {
			ve.setMessage("error on validating : " + e.getCause().getMessage());
		} else if (e != null) {
			ve.setMessage("error on validating. The exception generated is of kind : "
					+ e.getClass().getSimpleName());
		} else {
			ve.setMessage("Unexpected exception raised during the validation");
		}
		ve.setSeverity("error");
		return ve;
	}

}
