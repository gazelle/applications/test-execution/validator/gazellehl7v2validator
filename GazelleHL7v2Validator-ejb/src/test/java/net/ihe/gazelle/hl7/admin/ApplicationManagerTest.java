package net.ihe.gazelle.hl7.admin;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class ApplicationManagerTest {
    private static final String tosLink = "https://test.net";

    @Test
    public void getCguUrlTest_ok(){
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        assertEquals(tosLink,"https://test.net");
    }
    @Test
    public void getCguUrlTest_ko(){
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        Mockito.when(applicationManager.getTosLink()).thenReturn(tosLink);
        assertFalse(tosLink.equals("this is test string"));
    }
}
