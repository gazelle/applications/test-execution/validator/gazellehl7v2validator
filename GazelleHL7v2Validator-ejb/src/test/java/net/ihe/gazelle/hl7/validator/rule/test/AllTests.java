package net.ihe.gazelle.hl7.validator.rule.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(value = {INVSegmentInOULTest.class, OBXSegmentInLAWTest.class,
        PIDSegmentInOULTest.class, QPDSegmentInLAWTest.class, SACSegmentInLAWTest.class, OBRSegmentInOULTest.class, DatatypesInLAWTest.class, ORLO34RulesInLAWTest.class})
public class AllTests {

}
