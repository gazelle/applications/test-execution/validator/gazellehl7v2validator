package net.ihe.gazelle.hl7.validator.rule.test;

import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.rules.law.INVSegmentInOUL;

import org.junit.Test;

import ca.uhn.hl7v2.model.Message;

public class INVSegmentInOULTest {

	private static final INVSegmentInOUL rule = new INVSegmentInOUL();

	@Test
	public void testINVSegmentOptionalAndPresent() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||Q|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "INV|Titi|OK|CO\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r" + "SID|1^1^SR|123|456\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/INV[0]"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void testINVSegmentNotSupportedAndPresent() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "INV|Titi|OK|CO\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r" + "SID|1^1^SR|123|456\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull(rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/INV[0]"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void testINVSegmentNotSupportedAndNotPresent() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r" + "SID|1^1^SR|123|456\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/INV[0]"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void testINVSegmentOptionalAndNotPresent() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||Q|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r"
					+ "OBX|1|NM|123-1231^1.2.3^LN||123^1.2.3^LN|cm||L|||F|\r" + "SID|1^1^SR|123|456\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull(rule.test(message, "OUL_R22/SPECIMEN[0]/CONTAINER[0]/INV[0]"));
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}
}
