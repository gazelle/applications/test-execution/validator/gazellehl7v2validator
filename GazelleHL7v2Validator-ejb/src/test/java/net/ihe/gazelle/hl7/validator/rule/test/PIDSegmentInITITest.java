package net.ihe.gazelle.hl7.validator.rule.test;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.validation.ValidationException;
import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.core.IHEValidationContext;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.hl7.validator.rules.iti.*;
import net.ihe.gazelle.i18n.Msg;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PIDSegmentInITITest {
	public static Message parseStringMessage(String inMessage) throws HL7Exception {
		PipeParser parser = PipeParser.getInstanceWithNoValidation();
		String packageName = "net.ihe.gazelle.pam.fr.hl7v2.model.v25.message";
		return parser.parseForSpecificPackage(inMessage, packageName);
	}

	protected final static Logger log = LoggerFactory.getLogger(PIDSegmentInITITest.class);
	protected final static Msg msg = Msg.getMessages(PIDSegmentInITITest.class);

	private final static String A31 =
			"MSH|^~\\&|Gazelle|PAM_FR|PatientManager|PAM_FR|20210318151910||ADT^A31^ADT_A05|20210318151910|P|2.5^FRA^2.9||||||UNICODE UTF-8\r" +
			"EVN||20210318151910\r" +
			"PID|1||1900068^^^DDS&350000121&M^PI~260058815402330^^^DDS&1.2.250.1.213.1.4.8&ISO^INS||^MARIE-CECILE^^^^^D~DARK^JEANNE^JEANNE MARIE CECILE^^Mme^^L||19600530|F|||^^^^^FRA^BDL^^88154|||||S|||||||||1|||||N||VALI\r";

	private final static String A31_bis = "MSH|^~\\&|Interop|Berger-Levrault|UNKNOWN|UNKNOWN|202110271135||ADT^A31^ADT_A05|20211027113559221_13|P|2.5^FRA^2.10|||AL||FRA|8859/1|FR\r" +
			"EVN||202110271135|202110271135\r" +
			"PID|||13288^^^BL^PI^INTEROP~13288^^^BL^PN^INTEROP_PATNDO~250019560700266^^^ASIP-SANTE-INS-NIR&1.2.250.1.213.1.4.8&ISO^INS^^20211006||MARTINEAU^ISABELLE^ISABELLE-YVONNE MARGUERITE^^Mme^^L~LAGARDE^YVONNE^ISABELLE-YVONNE MARGUERITE^^Mme^^D||195001010000|F|||~|~|~|||M^Marie||||||||||FRA^^ISOnnnn||||N||VALI\r" +
			"PV1||N\r" +
			"ZFD|||N||INSI||CN|200206280000\r";


	private <T extends INSValidationRule> void invalid(Class<T> cls, String comment, String[] messagesToValidate, int invalids) {
		for (String messageToValidate:messagesToValidate) {
			try {
				Message message = parseStringMessage(messageToValidate);
				T rule = cls.newInstance();
				ValidationException[] exceptions = rule.test(message);
				log.info(msg.get("invalid",rule.assertionTag(),comment,messageToValidate.replace("\r","\n")));
				for (int i=0 ; i< invalids ; i++) {
					Assert.assertTrue(exceptions != null && exceptions[i].getMessage().startsWith(rule.assertionTag()));
				}
			} catch (Exception e) {
				Assert.fail("Cannot parse message");
			}
		}
	}
	private <T extends INSValidationRule> void invalid(Class<T> cls, String comment, String[] messagesToValidate) {
		invalid(cls, comment, messagesToValidate, 1);
	}
	<T extends INSValidationRule> void nominal(Class<T> cls, String[] messagesToValidate) {
		for (String messageToValidate:messagesToValidate) {
			try {
				Message message = parseStringMessage(messageToValidate);
				T rule = cls.newInstance();
				ValidationException[] exceptions = rule.test(message);
				Assert.assertTrue(exceptions == null || exceptions.length==0);
			} catch (Exception e) {
				Assert.fail("Cannot parse message");
			}
		}
	}
	private <T extends INSValidationRule> boolean containsRule(Class<T> cls,List<IHEValidationRule> rules) {
		for (IHEValidationRule rule:rules) {
			if (cls.isInstance(rule)) {
				return true;
			}
		}
		return false;
	}

	@Test
	public void testRetrieveRules() {
		String[] iti30 = new String[]{
				"2.16.840.1.113883.2.8.3.1.23", // ADT^A28^ADT_A05
				"2.16.840.1.113883.2.8.3.1.24", // ADT^A31^ADT_A05
				"2.16.840.1.113883.2.8.3.1.1", // ADT^A01^ADT_A01
				"2.16.840.1.113883.2.8.3.1.4", // ADT^A04^ADT_A01
				"2.16.840.1.113883.2.8.3.1.5" // ADT^A05^ADT_A05
		};
		INSValidationRules[] insrules = new INSValidationRules[] {
				new PID3RulesInITI(),
				new PID7RulesInITI(),
				new PID8RulesInITI(),
				new PID11RulesInITI()
		};
		for (INSValidationRules insrule:insrules) {
			for (String oid:iti30) {
				List<IHEValidationRule> rules = IHEValidationContext.SINGLETON.getIHEValidationRules(
						oid, insrule.getHl7Path());
				Assert.assertFalse(rules.isEmpty());
				Assert.assertTrue(containsRule(insrule.getClass(),rules));
			}
		}
	}

	@Test
	public void testINS001() {
		nominal(INS001RuleInITI.class,new String[]{A31});
		invalid(INS001RuleInITI.class,
				"PID-32 != VALI && INS 1.2.250.1.213.1.4.8 Renseigné",
				new String[]{
					A31.replace("VALI","VIDE")
		});
		invalid(INS001RuleInITI.class,
				"PID-32 != VALI && INS 1.2.250.1.213.1.4.9 Renseigné",
				new String[]{
					A31.replace("VALI","VIDE").replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.9")
		});
		invalid(INS001RuleInITI.class,
				"PID-32 != VALI && INS 1.2.250.1.213.1.4.10 Renseigné",
				new String[]{
					A31.replace("VALI","VIDE").replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.10")
		});
		invalid(INS001RuleInITI.class,
				"PID-32 != VALI && INS 1.2.250.1.213.1.4.11 Renseigné",
				new String[]{
					A31.replace("VALI","VIDE").replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.11")
		});
	}
	@Test
	public void testINS002() {
		nominal(INS002RuleInITI.class,new String[]{A31});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.8 && CX-1 vide.",
			new String[]{
				A31.replace("260058815402330","")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.8 && CX-1 longueur<15.",
			new String[]{
				A31.replace("260058815402330","26005881540233")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.8 && CX-1 longueur>15.",
			new String[]{
				A31.replace("260058815402330","2600588154023300")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.9 && CX-1 vide.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.9").replace("260058815402330","")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.9 && CX-1 longueur<15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.9").replace("260058815402330","26005881540233")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.9 && CX-1 longueur>15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.9").replace("260058815402330","2600588154023300")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.10 && CX-1 vide.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.10").replace("260058815402330","")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.10 && CX-1 longueur<15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.10").replace("260058815402330","26005881540233")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.10 && CX-1 longueur>15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.10").replace("260058815402330","2600588154023300")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.11 && CX-1 vide.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.11").replace("260058815402330","")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.11 && CX-1 longueur<15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.11").replace("260058815402330","26005881540233")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.11 && CX-1 longueur>15.",
			new String[]{
				A31.replace("1.2.250.1.213.1.4.8","1.2.250.1.213.1.4.11").replace("260058815402330","2600588154023300")
		});
		invalid(INS002RuleInITI.class,
			"CX-4-2 = 1.2.250.1.213.1.4.8 && CX-1 longueur>15 + CX-4-2 = 1.2.250.1.213.1.4.9 && CX-1 vide. (test avec plusieurs erreurs dans la même trame PID3)",
			new String[]{
				A31.replace("~260058815402330^^^DDS&1.2.250.1.213.1.4.8&ISO^INS","~2600588154023300^^^DDS&1.2.250.1.213.1.4.8&ISO^INS~^^^DDS&1.2.250.1.213.1.4.9&ISO^INS")
			},
			2);
	}
	@Test
	public void testINS003() {
		nominal(INS003RuleInITI.class,new String[]{A31});
		invalid(INS003RuleInITI.class,
			"PID-32 = VALI && pas d'occurrence de PID-5 avec XPN-7 = L (XPN-7 vide)",
			new String[]{
				A31.replace("Mme^^L","Mme^^")
		});
	}

	@Test
	public void testINS005() {
		nominal(INS005RuleInITI.class,new String[]{A31});
		invalid(INS005RuleInITI.class,
			"PID-32 = VALI && PID-7 vide",
			new String[]{
				A31.replace("|19600530|","||")
		});
	}
	@Test
	public void testINS006() {
		nominal(INS006RuleInITI.class,new String[]{A31});
		invalid(INS006RuleInITI.class,
			"PID-32 = VALI && PID-8 vide",
			new String[]{
				A31.replace("|F|","||")
		});
	}
	@Test
	public void testINS007() {
		nominal(INS007RuleInITI.class,new String[]{A31});
		invalid(INS007RuleInITI.class,
			"PID-32 = VALI && PID-8 = 'O'",
			new String[]{
				A31.replace("|F|","|O|")
		});
		invalid(INS007RuleInITI.class,
			"PID-32 = VALI && PID-8 = 'U'",
			new String[]{
				A31.replace("|F|","|U|")
		});
	}

	@Test
	public void testINS010() {
		nominal(INS010RuleInITI.class,new String[]{A31});
		invalid(INS010RuleInITI.class,
			"PID-11-7 = BDL && PID-11-9 non présent",
			new String[]{
				A31.replace("^88154|","|")
		});
		invalid(INS010RuleInITI.class,
			"PID-11-7 = BDL && PID-11-9 vide",
			new String[]{
				A31.replace("^88154|","^|")
		});
		invalid(INS010RuleInITI.class,
			"PID-11-7 = BDL && PID-11-9 longueur <5",
			new String[]{
				A31.replace("^88154|","^8815|")
		});
		invalid(INS010RuleInITI.class,
			"PID-11-7 = BDL && PID-11-9 longueur >5",
			new String[]{
				A31.replace("^88154|","^881540|")
		});
	}
	@Test
	public void testINS011() {
		nominal(INS011RuleInITI.class,new String[]{A31});
		invalid(INS011RuleInITI.class,
			"PID-32 = VALI && PID-5-1 vide pour PID-5-7 = L",
			new String[]{
				A31.replace("~DARK^JEANNE^","~^JEANNE^")
		});
	}
	@Test
	public void testINS012() {
		nominal(INS012RuleInITI.class,new String[]{A31});
		invalid(INS012RuleInITI.class,
			"PID-32 = VALI && PID-5-2 vide pour PID-5-7 = L",
			new String[]{
				A31.replace("~DARK^JEANNE^","~DARK^^")
		});
	}

	@Test
	public void testINS013() {
		nominal(INS013RuleInITI.class,
				new String[]{A31_bis});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && prénom avec un accent",
				new String[]{
						A31_bis.replace("ISABELLE","ISABELLé")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && prénom en minuscule",
				new String[]{
						A31_bis.replace("ISABELLE","isabelle")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && nom avec un accent",
				new String[]{
						A31_bis.replace("MARTINEAU","MàRTINEAU")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && autres prénoms avec un accent",
				new String[]{
						A31_bis.replace("YVONNE MARGUERITE","YVONNé MARGUERITE")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && autres prénoms en minuscule",
				new String[]{
						A31_bis.replace("YVONNE MARGUERITE","YVONNE marguerite")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && nom de jeune fille avec un accent",
				new String[]{
						A31_bis.replace("LAGARDE","LAGARDè")
				});
		invalid(INS013RuleInITI.class,
				"PID-32 = VALI && nom de jeune fille en minuscule",
				new String[]{
						A31_bis.replace("LAGARDE","lagarde")
				});
	}
}
