package net.ihe.gazelle.hl7.validator.rule.test;

import java.util.List;

import junit.framework.Assert;
import net.ihe.gazelle.hl7.validator.core.IHEValidationContext;
import net.ihe.gazelle.hl7.validator.rules.IHEValidationRule;
import net.ihe.gazelle.hl7.validator.rules.law.PIDSegmentInOUL;

import org.junit.Test;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.validation.ValidationException;

public class PIDSegmentInOULTest {

	private static final PIDSegmentInOUL rule = new PIDSegmentInOUL();

	@Test
	public void testRetrieveRules() {
		List<IHEValidationRule> rules = IHEValidationContext.SINGLETON.getIHEValidationRules(
				"1.3.6.1.4.12559.11.1.1.182", "OUL_R22/PATIENT/PID/PID-3");
		Assert.assertTrue(!rules.isEmpty());
	}

	@org.junit.Test
	public void testPID3NotNullAndOBR2NotNull() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||2131231213||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@org.junit.Test
	public void testPID3NullAndOBR2NotNull() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||56^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length > 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@org.junit.Test
	public void testPID3NullAndOBR2Null() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||\"\"|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			PIDSegmentInOUL rule = new PIDSegmentInOUL();
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@org.junit.Test
	public void testPID3NotNullAndOBR2Null() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||1231246546||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR||\"\"|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			PIDSegmentInOUL rule = new PIDSegmentInOUL();
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@org.junit.Test
	public void testOBR2Empty() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "PID|||1231246546||楚^小祥^^^^^L|徐^^^^^^M|19670110054800|M|||S 231^^东营市^^^CHN||||||||||||||||||||N\r"
					+ "PV1||C||||||7101^ESTRADA^JAIME^P^^DR|||||||||||1147^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN||||||||||||||||||||||||||||||||V\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR|||32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			PIDSegmentInOUL rule = new PIDSegmentInOUL();
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void testNoPatientGroup() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR|||32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			PIDSegmentInOUL rule = new PIDSegmentInOUL();
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}

	@Test
	public void testOUL25() {
		try {
			String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|MG|AM|20130326231317||OUL^R22^OUL_R22|20130326231317|P|2.5|||ER|AL||UNICODE UTF-8|||LAB-29^IHE\r"
					+ "SPM|1|1868&IHE_OM_OP&1.3.6.1.4.1.12559.11.1.2.2.4.2&ISO^1774&IHE_OM_OF&1.3.6.1.4.1.12559.11.1.2.2.4.3&ISO||SER^Serum^2.16.840.1.113883.12.70|||VENIP^Venipuncture^1.3.6.1.4.1.21367.100.1||||P^Patient (default if blank component value)^1.3.6.1.4.1.21367.100.1|||||AGG^Aggressive^1.3.6.1.4.1.21367.100.1|20121207120000|20121207120000||N||||||1\r"
					+ "SAC|||||SPID1\r"
					+ "OBR|||32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|LAB190^INR^1.3.6.1.4.1.21367.100.1||||||||||||3456^ROSZEK^JEANETTE^G^^DR|||||||||F|||3456^ROSZEK^JEANETTE^G^^DR\r"
					+ "ORC|SC|256^IHE_OM_ANALYZER_MGR^1.3.6.1.4.1.12559.11.1.2.2.4.4^ISO|32^IHE_OM_ANALYZER^1.3.6.1.4.1.12559.11.1.2.2.4.5^ISO|1869^IHE_OM_OP^1.3.6.1.4.1.12559.11.1.2.2.4.2^ISO|SC||||20121207120000|3456^ROSZEK^JEANETTE^G^^DR||3456^ROSZEK^JEANETTE^G^^DR|||||922229-20^IHE-LAB^1.3.6.1.4.1.21367.100.1\r";
			Message message = TestUtil.parseStringMessage(messageToValidate);
			PIDSegmentInOUL rule = new PIDSegmentInOUL();
			ValidationException[] exceptions = rule.test(message);
			Assert.assertTrue(exceptions.length == 0);
		} catch (Exception e) {
			Assert.fail("Cannot parse message");
		}
	}
}
