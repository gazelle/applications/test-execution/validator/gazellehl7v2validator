package net.ihe.gazelle.hl7.validator.rule.test;

import net.ihe.gazelle.hl7.validator.core.IHEValidationContext;
import net.ihe.gazelle.hl7.validator.rules.law.QPDSegmentInLAW;

import org.junit.Assert;
import org.junit.Test;

import ca.uhn.hl7v2.model.Message;

public class QPDSegmentInLAWTest {

	private static final IHEValidationContext validationContext = IHEValidationContext.SINGLETON;
	private static QPDSegmentInLAW rule = new QPDSegmentInLAW();

	@Test
	public void retrieveRuleForQPD3InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-3")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD4InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-4")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD5InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-5")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD6InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-6")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD7InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-7")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD8InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-8")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD9InQBP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.180",
                "QBP_Q11/QPD/QPD-9")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD3InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-3")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD4InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-4")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD5InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-5")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD6InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-6")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD7InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-7")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD8InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD/QPD-8")
				.isEmpty());
	}

	@Test
	public void retrieveRuleForQPD9InRSP() {
		Assert.assertFalse(validationContext.getIHEValidationRules("1.3.6.1.4.12559.11.1.1.177",
                "RSP_K11/QPD[0]/QPD-9(SAC-4: Primary (parent) Container Identifier)")
				.isEmpty());
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD3MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451|3937\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD3MandatoryAndPresent_2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by Isolate^IHE_LABTF|20130507152451|3937\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD3MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS && QPD-3 present
	 */
	@Test
	public void test_QBP_QPD3NotSupportedButPresent_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451|3937|123456\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_ALL
	 */
	@Test
	public void test_QBP_QPD3NotSupportedAndEmpty_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_ALL^Work Order Step All^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_RACK
	 */
	@Test
	public void test_QBP_QPD4MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_RACK^Work Order Step by rack^IHE_LABTF|20130507152451||12312\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-4"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_RACK
	 */
	@Test
	public void test_QBP_QPD4MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_RACK^Work Order Step by rack^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-4"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD4NotSupportedButPresent_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451|3937|123456\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-4"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}


	/**
	 * QPD-1.1 = WOS_ALL
	 */
	@Test
	public void test_QBP_QPD4NotSupportedAndEmpty_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_ALL^Work Order Step All^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-4"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_RACK
	 */
	@Test
	public void test_QBP_QPD5MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_RACK^Work Order Step by rack^IHE_LABTF|20130507152451||123158|123|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-5"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_RACK
	 */
	@Test
	public void test_QBP_QPD5MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_RACK^Work Order Step by rack^IHE_LABTF|20130507152451||123158||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No Exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-5"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = ALL_WOS
	 */
	@Test
	public void test_QBP_QPD5NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|ALL_WOS^All Work Order Step^IHE_LABTF|20130507152451||||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-5"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD5NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451|||123|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-5"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD6MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451||||1231231|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-6"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD6MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-6"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD6NotSupportedButPresent_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by Isolate^IHE_LABTF|20130507152451|3937|||12313|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-6"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}


	/**
	 * QPD-1.1 = WOS_ALL
	 */
	@Test
	public void test_QBP_QPD6NotSupportedAndEmpty_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_ALL^Work Order Step All^IHE_LABTF|20130507152451|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-6"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD7MandatoryAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451||||123|1^2\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-7"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD7MandatoryAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451||||1231|\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No Exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-7"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD7NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by isolate^IHE_LABTF|20130507152451|12312|||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-7"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1-1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD7NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by isolate^IHE_LABTF|20130507152451||123|||1^3\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-7"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_TRAY
	 */
	@Test
	public void test_QBP_QPD8RequiredOrEmptyAndPresent_1() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_TRAY^Work Order Step by tray^IHE_LABTF|20130507152451||123||||fiole\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-8"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_RACK
	 */
	@Test
	public void test_QBP_QPD8RequiredOrEmptyAndPresent_2() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_RACK^Work Order Step by rack^IHE_LABTF|20130507152451||||123||fiole\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-8"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD9RequiredAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by isolate^IHE_LABTF|20130507152451||||123||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-9"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS_BY_ISOLATE
	 */
	@Test
	public void test_QBP_QPD9RequiredAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS_BY_ISOLATE^Work Order Step by isolate^IHE_LABTF|20130507152451|||||||primary-1234\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-9"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD9NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451||||||fiole|primary-1234\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-9"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD9NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451||||||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-9"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD8NotSupportedAndEmpty() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451||||||\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-8"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * QPD-1.1 = WOS
	 */
	@Test
	public void test_QBP_QPD8NotSupportedAndPresent() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451||||||fiole\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNotNull("No exception", rule.test(message, "QBP_Q11/QPD[0]/QPD-8"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void test_QBP_NoTestForThisField() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER|IHE|OM_LAB_ANALYZER_MGR|IHE|20130507152451||QBP^Q11^QBP_Q11|20130507152451|P|2.5.1|||ER|AL||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451||||||fiole\r" + "RCP|I||R\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "QBP_Q11/QPD[0]/QPD-10"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void test_RSP() {
		String messageToValidate = "MSH|^~\\&|OM_LAB_ANALYZER_MGR|IHE|OM_LAB_ANALYZER|IHE|20130507152451||RSP^K11^RSP_K11|20130507152451|P|2.5.1||||||UNICODE UTF-8|||LAB-27^IHE\r"
				+ "MSA|AA|20130507152451\r"
				+ "QAK|20130507152451|OK|Q11^Work Order Step^IHE_LABTF\r"
				+ "QPD|WOS^Work Order Step^IHE_LABTF|20130507152451|3937\r";
		try {
			Message message = TestUtil.parseStringMessage(messageToValidate);
			Assert.assertNull("Exception raised", rule.test(message, "RSP_K11/QPD[0]/QPD-3"));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

}
