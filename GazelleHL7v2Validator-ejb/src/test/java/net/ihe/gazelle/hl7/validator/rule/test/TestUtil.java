package net.ihe.gazelle.hl7.validator.rule.test;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.PipeParser;

public class TestUtil {

	public static Message parseStringMessage(String inMessage) throws EncodingNotSupportedException, HL7Exception {
		PipeParser parser = PipeParser.getInstanceWithNoValidation();
		String packageName = "net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message";
		Message message = parser.parseForSpecificPackage(inMessage, packageName);
		return message;
	}
}
