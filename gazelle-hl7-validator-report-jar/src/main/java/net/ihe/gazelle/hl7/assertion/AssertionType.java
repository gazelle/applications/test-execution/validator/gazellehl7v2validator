package net.ihe.gazelle.hl7.assertion;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * <p>AssertionType enum.</p>
 *
 * @author abe
 * @version 1.0: 20/08/2019
 */
@XmlEnum
public enum AssertionType {

    @XmlEnumValue("VS Error")
    VS_ERROR("VS Error"),
    @XmlEnumValue(value = "Length")
    LENGTH("Length"),
    @XmlEnumValue(value = "Cardinality")
    CARDINALITY("Cardinality"),
    @XmlEnumValue(value = "Required element")
    REQUIRED_ELEMENT("Required element"),
    @XmlEnumValue(value = "Permitted element")
    PERMITTED_ELEMENT("Permitted element"),
    @XmlEnumValue(value = "Forbidden element")
    FORBIDDEN_ELEMENT("Forbidden element"),
    @XmlEnumValue(value = "Optional element")
    OPTIONAL_ELEMENT("Optional element"),
    @XmlEnumValue(value = "Datatype")
    DATATYPE("Datatype"),
    @XmlEnumValue(value = "Format")
    FORMAT("Format"),
    @XmlEnumValue(value = "Value")
    VALUE("Value"),
    //AssertionType GVT (affirmative)
    @XmlEnumValue(value = "Excluded From Validation")
    EXCLUDED_FROM_VALIDATION("Excluded From Validation"),
    @XmlEnumValue(value = "Constraint Success")
    CONSTRAINT_SUCCESS("Constraint Success"),
    @XmlEnumValue(value = "Content spec error")
    CONTENT_SPEC_ERROR("Content spec error"),
    @XmlEnumValue(value = "Content Success")
    CONTENT_SUCCESS("Content Success"),
    @XmlEnumValue(value = "CoConstraint Success")
    COCONSTRAINT_SUCCESS("CoConstraint Success"),
    @XmlEnumValue(value = "Constraint Spec Error")
    CONSTRAINT_SPEC_ERROR("Constraint Spec Error"),
    @XmlEnumValue(value = "Predicate spec error")
    PREDICATE_SPEC_ERROR("Predicate spec error"),
    @XmlEnumValue(value = "Predicate success")
    PREDICATE_SUCCESS("Predicate success"),
    @XmlEnumValue("PVS")
    PVS("PVS"),
    @XmlEnumValue("Empty VS")
    EMPTY_VS("Empty VS"),
    @XmlEnumValue("No Code Usage")
    NO_CODE_USAGE("No Code Usage"),
    @XmlEnumValue("Dynamic Value Set")
    DYNAMIC_VALUE_SET("Dynamic Value Set"),
    @XmlEnumValue("Undetermined Binding Strength")
    UNDETERMINED_BINDING_STRENGTH("Undetermined Binding Strength"),
    @XmlEnumValue("O-Usage")
    O_USAGE("O-Usage"),
    @XmlEnumValue("RVS")
    RVS("RVS"),
    @XmlEnumValue("Co-Constraint")
    CO_CONSTRAINT("Co-Constraint"),
    @XmlEnumValue("Single Code Success")
    SINGLE_CODE_SUCCESS("Single Code Success"),
    @XmlEnumValue("Slicing")
    SLICING("Slicing"),
    @XmlEnumValue(value = "Not AssertionType found")
    DEFAULT("Not AssertionType found");

    private String label;

    AssertionType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
