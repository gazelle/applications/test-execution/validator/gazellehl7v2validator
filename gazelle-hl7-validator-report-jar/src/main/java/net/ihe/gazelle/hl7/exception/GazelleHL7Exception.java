package net.ihe.gazelle.hl7.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.hl7.utils.PathBuilder;

/**
 * <p>Abstract GazelleHL7Exception class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.NONE)
public abstract class GazelleHL7Exception {

	/**
	 * if exception is located in an HL7 table, give the oid/#hl7TableID string
	 */
	@XmlElement(name = "HL7Table")
	private String hl7tableId;
	/**
	 * Path to the exception
	 */
	@XmlElement(name = "Location")
	private String hl7Path;
	/**
	 * value causing the exception
	 */
	@XmlElement(name = "Value")
	private String value;

	@XmlElement(name = "Description")
	private String description;

	@XmlElement(name = "Type")
	private GazelleErrorCode gazelleErrorCode;

	@XmlTransient
	private HL7Exception hl7Exception;

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 */
	public GazelleHL7Exception() {
		super();
	}

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public GazelleHL7Exception(String message, PathBuilder pathBuilder, String value) {
		this.hl7Exception = new HL7Exception(message);
		if (pathBuilder != null) {
			this.hl7Path = pathBuilder.toString();
		}
		this.value = value;
		this.description = getMessage();
	}

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param tableId a {@link java.lang.String} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public GazelleHL7Exception(String message, String tableId, PathBuilder pathBuilder, String inValue) {
		this.hl7Exception = new HL7Exception(message);
		this.hl7tableId = tableId;
		this.value = inValue;
		if (pathBuilder != null) {
			this.hl7Path = pathBuilder.toString();
		}
		this.description = getMessage();
	}

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param exception a {@link ca.uhn.hl7v2.HL7Exception} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public GazelleHL7Exception(HL7Exception exception, PathBuilder pathBuilder, String inValue) {
		this.hl7Exception = new HL7Exception(exception.getMessage());
		this.hl7Exception.setLocation(exception.getLocation());
		if (pathBuilder != null) {
			this.hl7Path = pathBuilder.toString();
		}
		this.setGazelleErrorCode(exception.getErrorCode());
		this.value = inValue;
		this.description = getMessage();
	}

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public GazelleHL7Exception(Throwable cause) {
		this.hl7Exception = new HL7Exception(cause);
		this.description = getMessage();
	}

	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param errorCode a {@link net.ihe.gazelle.hl7.exception.GazelleErrorCode} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public GazelleHL7Exception(String message, GazelleErrorCode errorCode, PathBuilder pathBuilder, String inValue) {
		this.hl7Exception = new HL7Exception(message);
		this.gazelleErrorCode = errorCode;
		if (pathBuilder != null) {
			this.hl7Path = pathBuilder.toString();
		}
		this.value = inValue;
		this.description = getMessage();
	}


	/**
	 * <p>Constructor for GazelleHL7Exception.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param errorCode a int.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public GazelleHL7Exception(String message, int errorCode, PathBuilder pathBuilder, String inValue) {
		this.hl7Exception = new HL7Exception(message);
		setGazelleErrorCode(errorCode);
		if (pathBuilder != null) {
			this.hl7Path = pathBuilder.toString();
		}
		this.value = inValue;
		this.description = getMessage();
	}

	public GazelleHL7Exception(String description, String classification, int codeIgamt, String path) {
		this.hl7Exception = new HL7Exception(description);
		setGazelleErrorCode(codeIgamt);
		if (path != null) {
			this.hl7Path = path;
		}
		this.value = classification;
		this.description = getMessage();
	}


	/**
	 * <p>getMessage.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMessage() {
		if (this.hl7Exception != null) {
			String message = this.hl7Exception.getMessage();
			int locationIndex = message.indexOf(": Segment:");
			if (locationIndex > 0) {
				message = message.substring(0, locationIndex);
			}
			return message;
		} else {
			return null;
		}
	}

	/**
	 * <p>Getter for the field <code>description</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getDescription(){
		return description;
	}

	/**
	 * <p>Setter for the field <code>hl7tableId</code>.</p>
	 *
	 * @param hl7tableId a {@link java.lang.String} object.
	 */
	public void setHl7tableId(String hl7tableId) {
		this.hl7tableId = hl7tableId;
	}

	/**
	 * <p>Getter for the field <code>hl7tableId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHl7tableId() {
		return hl7tableId;
	}

	/**
	 * <p>Setter for the field <code>hl7Path</code>.</p>
	 *
	 * @param hl7Path a {@link java.lang.String} object.
	 */
	public void setHl7Path(String hl7Path) {
		this.hl7Path = hl7Path;
	}

	/**
	 * <p>Getter for the field <code>hl7Path</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHl7Path() {
		if (this.hl7Path != null) {
			return hl7Path;
		} else if ((this.hl7Exception != null) && (this.hl7Exception.getLocation() != null)) {
			return this.hl7Exception.getLocation().toString();
		} else {
			return null;
		}
	}

	/**
	 * <p>Setter for the field <code>value</code>.</p>
	 *
	 * @param value a {@link java.lang.String} object.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * <p>Getter for the field <code>value</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <p>Getter for the field <code>gazelleErrorCode</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7.exception.GazelleErrorCode} object.
	 */
	public GazelleErrorCode getGazelleErrorCode() {
		if (this.gazelleErrorCode == null) {
			this.gazelleErrorCode = GazelleErrorCode.UNKNOWN;
		}
		return gazelleErrorCode;
	}

	/**
	 * <p>Setter for the field <code>gazelleErrorCode</code>.</p>
	 *
	 * @param gazelleErrorCode a {@link net.ihe.gazelle.hl7.exception.GazelleErrorCode} object.
	 */
	public void setGazelleErrorCode(GazelleErrorCode gazelleErrorCode) {
		if (gazelleErrorCode != null) {
			this.gazelleErrorCode = gazelleErrorCode;
		} else {
			this.gazelleErrorCode = GazelleErrorCode.UNKNOWN;
		}
	}

	/**
	 * <p>Setter for the field <code>gazelleErrorCode</code>.</p>
	 *
	 * @param code a int.
	 */
	public void setGazelleErrorCode(int code) {
		this.gazelleErrorCode = GazelleErrorCode.errorCodeFor(code);
	}

	/**
	 * <p>getFailureType.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFailureType() {

		return this.getGazelleErrorCode().getMessage();
	}


	/**
	 * <p>Getter for the field <code>hl7Exception</code>.</p>
	 *
	 * @return a {@link ca.uhn.hl7v2.HL7Exception} object.
	 */
	public HL7Exception getHl7Exception() {
		return hl7Exception;
	}

	/**
	 * <p>Setter for the field <code>hl7Exception</code>.</p>
	 *
	 * @param hl7Exception a {@link ca.uhn.hl7v2.HL7Exception} object.
	 */
	public void setHl7Exception(HL7Exception hl7Exception) {
		this.hl7Exception = hl7Exception;
	}

}
