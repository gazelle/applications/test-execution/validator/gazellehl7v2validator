package net.ihe.gazelle.hl7.utils;

import java.util.ArrayList;

/**
 * <p>PathBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class PathBuilder extends ArrayList<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String pathSeparator = "/";

	/**
	 * <p>Constructor for PathBuilder.</p>
	 */
	public PathBuilder() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (String string : this.toArray(new String[1])) {
			if (builder.length() > 0) {
				builder.append(pathSeparator);
			}
			builder.append(string);
		}
		return builder.toString();
	}

	/**
	 * <p>removeLastItem.</p>
	 */
	public void removeLastItem() {
		if (this.size() > 0) {
			this.remove(this.size() - 1);
		}
	}

	/**
	 * <p>replaceLastItem.</p>
	 *
	 * @param stringToAdd a {@link java.lang.String} object.
	 */
	public void replaceLastItem(String stringToAdd) {
		if (this.size() > 1) {
			this.set(this.size() - 1, stringToAdd);
		}
	}

	/**
	 * <p>addRep.</p>
	 *
	 * @param rep a int.
	 */
	public void addRep(int rep) {
		String lastItem = this.get(this.size() - 1);
		if (lastItem.contains("[")) {
			lastItem = lastItem.replaceFirst("\\[[0-9]*\\]", "[" + rep + "]");
		} else {
			lastItem = lastItem.concat("[" + rep + "]");
		}
		replaceLastItem(lastItem);
	}
}
