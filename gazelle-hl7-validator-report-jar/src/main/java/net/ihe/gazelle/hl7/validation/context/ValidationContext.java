package net.ihe.gazelle.hl7.validation.context;

import ca.uhn.hl7v2.parser.GenericParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

/**
 * <p>ValidationContext class.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */

@XmlRootElement(name = "ValidationContext")
@XmlAccessorType(XmlAccessType.NONE)
public class ValidationContext {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationContext.class);
    public static final String ER7 = "VB"; /*returned by hapi when encoding is default (ER7)*/
    public static final String XML = "XML";

    @XmlElement(name = "ProfileOID")
    private String profileOid;

    @XmlElement(name = "Profile")
    private ProfileDefinition profileDefinition;

    @XmlElement(name = "ValidationOptions")
    private ValidationOptions validationOptions;

    @XmlTransient
    private String encoding;

    public ValidationContext() {
        this.validationOptions = new ValidationOptions();
        this.encoding = null;
    }

    public String getProfileOid() {
        return profileOid;
    }

    public void setProfileOid(String profileOid) {
        this.profileOid = profileOid;
    }

    public ProfileDefinition getProfileDefinition() {
        return profileDefinition;
    }

    public void setProfileDefinition(ProfileDefinition profileDefinition) {
        this.profileDefinition = profileDefinition;
    }

    public ValidationOptions getValidationOptions() {
        return validationOptions;
    }

    public void setValidationOptions(ValidationOptions validationOptions) {
        this.validationOptions = validationOptions;
    }

    public String getEncoding(String messageToValidate) {
        if (this.encoding == null) {
            GenericParser parser = GenericParser.getInstanceWithNoValidation();
            parser.setPipeParserAsPrimary();
            this.encoding = GenericParser.getInstanceWithNoValidation().getEncoding(messageToValidate);
        }
        return encoding;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public static ValidationContext createFromString(String xmlValidationContext) {
        ByteArrayInputStream is = new ByteArrayInputStream(xmlValidationContext.getBytes(Charset.forName("UTF-8")));
        try {
            JAXBContext jc = JAXBContext.newInstance(ValidationContext.class);
            Unmarshaller u = jc.createUnmarshaller();
            ValidationContext validationContext = (ValidationContext) u.unmarshal(is);
            return validationContext;
        } catch (Exception e) {
            LOG.error("Unable to unmarshall validation context: " + e.getMessage());
            e.printStackTrace();
            return new ValidationContext();
        }
    }


    public ValidationType getLength() {
        return this.validationOptions.getLength();
    }

    public ValidationType getDataValue() {
        return this.validationOptions.getDataValue();
    }

    public ValidationType getDatatype() {
        return this.validationOptions.getDatatype();
    }

    public ValidationType getMessageStructure() {
        return this.validationOptions.getMessageStructure();
    }

    public boolean isEr7() {
        return ER7.equals(this.encoding);
    }
}
