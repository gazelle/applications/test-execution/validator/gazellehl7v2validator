package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.exception.GazelleHL7Exception;
import net.ihe.gazelle.hl7.utils.PathBuilder;
import ca.uhn.hl7v2.HL7Exception;

/**
 * <p>Error class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "Error")
@XmlAccessorType(XmlAccessType.NONE)
public class Error extends GazelleHL7Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7091157656274797386L;

	/**
	 * <p>Constructor for Error.</p>
	 */
	public Error() {
		super();
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public Error(String message, PathBuilder pathBuilder, String value) {
		super(message, pathBuilder, value);
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param tableId a {@link java.lang.String} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public Error(String message, String tableId, PathBuilder pathBuilder, String inValue) {
		super(message, tableId, pathBuilder, inValue);
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param exception a {@link ca.uhn.hl7v2.HL7Exception} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public Error(HL7Exception exception, PathBuilder pathBuilder, String inValue) {
		super(exception, pathBuilder, inValue);
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param cause a {@link java.lang.Throwable} object.
	 */
	public Error(Throwable cause) {
		super(cause);
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param errorCode a {@link net.ihe.gazelle.hl7.exception.GazelleErrorCode} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public Error(String message, GazelleErrorCode errorCode, PathBuilder pathBuilder, String inValue) {
		super(message, errorCode, pathBuilder, inValue);
	}

	/**
	 * <p>Constructor for Error.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param errorCode a int.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public Error(String message, int errorCode, PathBuilder pathBuilder, String inValue) {
		super(message, errorCode, pathBuilder, inValue);
	}

}
