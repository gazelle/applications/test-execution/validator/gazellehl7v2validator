package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.ihe.gazelle.hl7.exception.GazelleHL7Exception;
import net.ihe.gazelle.hl7.utils.PathBuilder;
import ca.uhn.hl7v2.HL7Exception;

/**
 * <p>GazelleProfileException class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "ProfileException")
@XmlAccessorType(XmlAccessType.NONE)
public class GazelleProfileException extends GazelleHL7Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3827218270945165049L;

	/**
	 * <p>Constructor for GazelleProfileException.</p>
	 */
	public GazelleProfileException() {
		super();
	}

	/**
	 * <p>Constructor for GazelleProfileException.</p>
	 *
	 * @param exception a {@link ca.uhn.hl7v2.HL7Exception} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param inValue a {@link java.lang.String} object.
	 */
	public GazelleProfileException(HL7Exception exception, PathBuilder pathBuilder, String inValue) {
		super(exception, pathBuilder, inValue);
		// TODO Auto-generated constructor stub
	}
	public GazelleProfileException(String description, String classification, int codeIgamt, String path){
		super(description, classification, codeIgamt, path);
	}

	/**
	 * <p>Constructor for GazelleProfileException.</p>
	 *
	 * @param message a {@link java.lang.String} object.
	 * @param pathBuilder a {@link net.ihe.gazelle.hl7.utils.PathBuilder} object.
	 * @param value a {@link java.lang.String} object.
	 */
	public GazelleProfileException(String message, PathBuilder pathBuilder, String value) {
		super(message, pathBuilder, value);
	}

}
