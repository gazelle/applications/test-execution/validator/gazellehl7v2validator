package net.ihe.gazelle.hl7.validator.report;

import org.jboss.seam.faces.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * <p>HL7v2ValidationReport class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "SummaryResults")
@XmlAccessorType(XmlAccessType.NONE)
public class HL7v2ValidationReport {

	private static Logger log = LoggerFactory.getLogger(HL7v2ValidationReport.class);

	@XmlElement(name = "ValidationResultsOverview")
	private ValidationResultsOverview overview;

	@XmlElement(name = "ValidationCounters")
	private ValidationCounters counters;

	@XmlElement(name = "Resources")
	private UsedResources resources;

	@XmlElement(name = "ValidationResults")
	private ValidationResults results;

	public HL7v2ValidationReport(){

	}

	/**
	 * <p>Getter for the field <code>overview</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview} object.
	 */
	public ValidationResultsOverview getOverview() {
		if (this.overview == null){
			this.overview = new ValidationResultsOverview();
		}
		return overview;
	}

	/**
	 * <p>Setter for the field <code>overview</code>.</p>
	 *
	 * @param overview a {@link net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview} object.
	 */
	public void setOverview(ValidationResultsOverview overview) {
		this.overview = overview;
	}

	/**
	 * <p>Getter for the field <code>counters</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.ValidationCounters} object.
	 */
	public ValidationCounters getCounters() {
		if (this.counters == null) {
			this.counters = new ValidationCounters();
		}
		return counters;
	}

	/**
	 * <p>Setter for the field <code>counters</code>.</p>
	 *
	 * @param counters a {@link net.ihe.gazelle.hl7.validator.report.ValidationCounters} object.
	 */
	public void setCounters(ValidationCounters counters) {
		this.counters = counters;
	}

	/**
	 * <p>Getter for the field <code>resources</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.UsedResources} object.
	 */
	public UsedResources getResources() {
		return resources;
	}

	/**
	 * <p>Setter for the field <code>resources</code>.</p>
	 *
	 * @param resources a {@link net.ihe.gazelle.hl7.validator.report.UsedResources} object.
	 */
	public void setResources(UsedResources resources) {
		this.resources = resources;
	}

	/**
	 * <p>Setter for the field <code>resources</code>.</p>
	 *
	 * @param resources a {@link java.util.List} object.
	 */
	public void setResources(List<UsedResource> resources) {
		this.resources = new UsedResources();
		this.resources.setResources(resources);
	}

	/**
	 * <p>Getter for the field <code>results</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.ValidationResults} object.
	 */
	public ValidationResults getResults() {
		if (this.results == null) {
			this.results = new ValidationResults();
		}
		return results;
	}

	/**
	 * <p>Setter for the field <code>results</code>.</p>
	 *
	 * @param results a {@link net.ihe.gazelle.hl7.validator.report.ValidationResults} object.
	 */
	public void setResults(ValidationResults results) {
		this.getCounters().setNbOfAssertions(results.getReportCounter());
		this.getCounters().setNbOfErrors(results.getErrorCounter());
		this.getCounters().setNbOfWarnings(results.getWarningCounter());
		this.getCounters().setNbOfExceptions(results.getExceptionCounter());
		this.results = results;
	}

	/** {@inheritDoc}
	 * @param pathToReportXsl*/
	public String toString(String pathToReportXsl) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			JAXBContext context = JAXBContext.newInstance(HL7v2ValidationReport.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.FALSE);
			if (pathToReportXsl != null) {
				m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml-stylesheet type=\"text/xsl\" href=\"" + pathToReportXsl
						+ "\"?>");
			}
			m.marshal(this, baos);
		} catch (JAXBException e) {
			log.error("Cannot convert the validation report into XML, report will be empty: {}", e.getMessage());
		}
		try {
			return baos.toString("UTF-8");
		}catch(UnsupportedEncodingException e){
			log.warn("UTF-8 encoding is not supported, relying on default encoding: " + Charset.defaultCharset().displayName(), e);
			return baos.toString();
		}
	}

	/**
	 * <p>createFromXml.</p>
	 *
	 * @param xmlReport a {@link java.lang.String} object.
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport} object.
	 */
	public static HL7v2ValidationReport createFromXml(String xmlReport){
		ByteArrayInputStream is = new ByteArrayInputStream(xmlReport.getBytes(Charset.forName("UTF-8")));
		return createFromXml(is);
	}

	/**
	 *
	 * @param xmlReport
	 * @return a {@link net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport} object.
	 */


	public static HL7v2ValidationReport createFromXml(InputStream xmlReport){

		try {
			JAXBContext jc = JAXBContext.newInstance(HL7v2ValidationReport.class);
			Unmarshaller u = jc.createUnmarshaller();
			HL7v2ValidationReport object = (HL7v2ValidationReport) u.unmarshal(xmlReport);
			return object;
		}catch(Exception e){
			log.error("Unable to unmarshall validation report");
			return null;
		}
	}

	public void creatFromXmlTest(InputStream xmlReport){
		HL7v2ValidationReport object = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(HL7v2ValidationReport.class);
			Unmarshaller u = jc.createUnmarshaller();
			object = (HL7v2ValidationReport) u.unmarshal(xmlReport);


		}catch(Exception e){
			log.error("Unable to unmarshall validation report");
		}
		this.replaceNoExistingValue(object);
	}
	public void createFromXmlTest(String xmlReport){
		ByteArrayInputStream is = new ByteArrayInputStream(xmlReport.getBytes(Charset.forName("UTF-8")));
		this.creatFromXmlTest(is);
	}

	public void replaceNoExistingValue(HL7v2ValidationReport data){

		if(this.getOverview().getMessageOid()==null) this.getOverview().setMessageOid(data.getOverview().getMessageOid());
		if(this.getOverview().getProfileOid()==null) this.getOverview().setProfileOid(data.getOverview().getProfileOid());
		if(this.getOverview().getProfileRevision()==null) this.getOverview().setProfileRevision(data.getOverview().getProfileRevision());

		if(this.getOverview().getProfileOid()==null) this.getOverview().setProfileOid(data.getOverview().getProfileOid());

		if(this.getOverview().getValidationDateTime()==null) this.getOverview().setValidationDateTime(data.getOverview().getValidationDateTime());

		this.getOverview().setValidationAbortedReason(data.getOverview().getValidationAbortedReason());


		if(this.getOverview().getValidationServiceVersion()==null) {
			this.getOverview().setValidationServiceVersion(data.getOverview().getValidationServiceVersion());
		}
		this.setResources(data.getResources());

		this.getOverview().setValidationStatus(data.getOverview().getValidationStatus());
		this.setResults(data.getResults());


	}

	/**
	 * <p>computeResults.</p>
	 *
	 * Compute result of the Hl7V2 Report depending on counters
	 */
	public void computeResult() {

		if (this.getResults().getExceptionCounter() > 0) {
			this.getOverview().setValidationStatus(ValidationStatus.ABORTED);
		} else if (this.getResults().getErrorCounter() > 0) {
			this.getOverview().setValidationStatus(ValidationStatus.FAILED);
		} else {
			this.getOverview().setValidationStatus(net.ihe.gazelle.hl7.validator.report.ValidationStatus.PASSED);
		}

	}
}
