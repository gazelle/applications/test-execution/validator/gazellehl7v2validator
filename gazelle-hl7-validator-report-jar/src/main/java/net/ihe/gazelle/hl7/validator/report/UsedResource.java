package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>UsedResource class.</p>
 *
 * @author abe
 * @version 1.0: 20/08/2019
 */
@XmlRootElement(name = "Resource")
@XmlAccessorType(XmlAccessType.NONE)
public class UsedResource {

    @XmlElement(name = "oid")
    protected String oid;

    @XmlElement(name = "revision")
    protected String revision;

    public UsedResource(){};

    public UsedResource(String oid, String revision){
        this.oid = oid;
        this.revision = revision;
    }
}
