package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * <p>UsedResources class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UsedResources {

	@XmlElement(name = "Resource")
	private List<UsedResource> resources;

	/**
	 * <p>Getter for the field <code>resources</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<UsedResource> getResources() {
		return resources;
	}

	/**
	 * <p>Setter for the field <code>resources</code>.</p>
	 *
	 * @param resources a {@link java.util.List} object.
	 */
	public void setResources(List<UsedResource> resources) {
		this.resources = resources;
	}
}
