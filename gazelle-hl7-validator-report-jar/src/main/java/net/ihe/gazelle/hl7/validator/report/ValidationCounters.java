package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>ValidationCounters class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "ValidationCounters")
@XmlAccessorType(XmlAccessType.NONE)
public class ValidationCounters {

	@XmlElement(name = "NrOfValidationErrors")
	private Integer nbOfErrors;

	@XmlElement(name = "NrOfValidationWarnings")
	private Integer nbOfWarnings;

	@XmlElement(name = "NrOfValidatedAssertions")
	private Integer nbOfAssertions;

	@XmlElement(name = "NrOfValidationExceptions")
	private Integer nbOfExceptions;

	/**
	 * <p>Getter for the field <code>nbOfErrors</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getNbOfErrors() {
		return nbOfErrors;
	}

	/**
	 * <p>Setter for the field <code>nbOfErrors</code>.</p>
	 *
	 * @param nbOfErrors a {@link java.lang.Integer} object.
	 */
	public void setNbOfErrors(Integer nbOfErrors) {
		this.nbOfErrors = nbOfErrors;
	}

	/**
	 * <p>Getter for the field <code>nbOfWarnings</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getNbOfWarnings() {
		return nbOfWarnings;
	}

	/**
	 * <p>Setter for the field <code>nbOfWarnings</code>.</p>
	 *
	 * @param nbOfWarnings a {@link java.lang.Integer} object.
	 */
	public void setNbOfWarnings(Integer nbOfWarnings) {
		this.nbOfWarnings = nbOfWarnings;
	}

	/**
	 * <p>Getter for the field <code>nbOfAssertions</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getNbOfAssertions() {
		return nbOfAssertions;
	}

	/**
	 * <p>Setter for the field <code>nbOfAssertions</code>.</p>
	 *
	 * @param nbOfAssertions a {@link java.lang.Integer} object.
	 */
	public void setNbOfAssertions(Integer nbOfAssertions) {
		this.nbOfAssertions = nbOfAssertions;
	}

	/**
	 * <p>Getter for the field <code>nbOfExceptions</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getNbOfExceptions() {
		return nbOfExceptions;
	}

	/**
	 * <p>Setter for the field <code>nbOfExceptions</code>.</p>
	 *
	 * @param nbOfExceptions a {@link java.lang.Integer} object.
	 */
	public void setNbOfExceptions(Integer nbOfExceptions) {
		this.nbOfExceptions = nbOfExceptions;
	}

}
