package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <p>ValidationResultsOverview class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@XmlRootElement(name = "ValidationResultsOverview")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationResultsOverview {

    private final String SDF_DATE = "yyyy, MM dd";
    private final String SDF_TIME = "hh:mm (aa)";
    /**
     * Constant <code>TOOL_NAME="Gazelle HL7 Validator"</code>
     */
    public static final String TOOL_NAME = "Gazelle HL7 Validator";
    @XmlElement(name = "Disclaimer")
    public static final String DISCLAIMER = "The GazelleHL7v2Validator is an experimental system. IHE-Europe assumes no responsibility whatsoever "
            + "for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or "
            + "any other characteristic. We would appreciate acknowledgement if the service is used. "
            + "Bug tracking service is available at http://gazelle.ihe.net/jira/browse/HLVAL";

    @XmlTransient
    private Date validationDateTime;

    @XmlElement(name = "ProfileOID")
    private String profileOid;

    @XmlElement(name = "ProfileRevision")
    private String profileRevision;

    @XmlElement(name = "MessageOID")
    private String messageOid;

    @XmlElement(name = "ValidationAbortedReason")
    private String validationAbortedReason;

    @XmlElement(name = "ValidationTestResult")
    private ValidationStatus validationStatus;

    @XmlElement(name = "ValidationServiceVersion")
    private String validationServiceVersion;

    /**
     * <p>Constructor for ValidationResultsOverview.</p>
     */
    public ValidationResultsOverview() {
        this.validationDateTime = new Date();
    }

    /**
     * <p>Constructor for ValidationResultsOverview.</p>
     *
     * @param profileOid a {@link java.lang.String} object.
     */
    public ValidationResultsOverview(String profileOid) {
        this.profileOid = profileOid;
        this.validationDateTime = new Date();
    }

    public void setValidationServiceVersion(String validationServiceVersion) {
        this.validationServiceVersion = validationServiceVersion;
    }


    /**
     * <p>Getter for the field <code>validationDateTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getValidationDateTime() {
        return validationDateTime;
    }

    /**
     * <p>Setter for the field <code>validationDateTime</code>.</p>
     *
     * @param validationDateTime a {@link java.util.Date} object.
     */
    public void setValidationDateTime(Date validationDateTime) {
        this.validationDateTime = validationDateTime;
    }

    /**
     * <p>getValidationDate.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @XmlElement(name = "ValidationDate")
    public String getValidationDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE, Locale.ENGLISH);
        return sdf.format(validationDateTime);
    }

    /**
     * <p>getValidationTime.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @XmlElement(name = "ValidationTime")
    public String getValidationTime() {
        SimpleDateFormat sdf = new SimpleDateFormat(SDF_TIME, Locale.ENGLISH);
        return sdf.format(validationDateTime);
    }

    /**
     * <p>getValidationServiceVersion.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    public String getValidationServiceVersion() {
        return this.validationServiceVersion;
    }

    /**
     * <p>getValidationServiceName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    @XmlElement(name = "ValidationServiceName")
    public String getValidationServiceName() {
        return TOOL_NAME;
    }

    /**
     * <p>Getter for the field <code>profileOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProfileOid() {
        return profileOid;
    }

    /**
     * <p>Setter for the field <code>profileOid</code>.</p>
     *
     * @param profileOid a {@link java.lang.String} object.
     */
    public void setProfileOid(String profileOid) {
        this.profileOid = profileOid;
    }

    /**
     * <p>Getter for the field <code>profileRevision</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProfileRevision() {
        return profileRevision;
    }

    /**
     * <p>Setter for the field <code>profileRevision</code>.</p>
     *
     * @param profileRevision a {@link java.lang.String} object.
     */
    public void setProfileRevision(String profileRevision) {
        this.profileRevision = profileRevision;
    }

    /**
     * <p>Getter for the field <code>messageOid</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageOid() {
        return messageOid;
    }

    /**
     * <p>Setter for the field <code>messageOid</code>.</p>
     *
     * @param messageOid a {@link java.lang.String} object.
     */
    public void setMessageOid(String messageOid) {
        this.messageOid = messageOid;
    }

    /**
     * <p>getDisclaimer.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    public String getDisclaimer() {
        return DISCLAIMER;
    }

    /**
     * <p>getValidationTestResult.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    public String getValidationTestResult() {
        if(validationStatus != null)
        {
            return validationStatus.getStatus();
        }else {
            return null;
        }

    }

    /**
     * <p>Getter for the field <code>validationAbortedReason</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValidationAbortedReason() {
        return validationAbortedReason;
    }

    /**
     * <p>Setter for the field <code>validationAbortedReason</code>.</p>
     *
     * @param validationAbortedReason a {@link java.lang.String} object.
     */
    public void setValidationAbortedReason(String validationAbortedReason) {
        this.validationAbortedReason = validationAbortedReason;
    }

    /**
     * <p>Getter for the field <code>validationStatus</code>.</p>
     *
     * @return a {@link ValidationStatus} object.
     */
    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    /**
     * <p>Setter for the field <code>validationStatus</code>.</p>
     *
     * @param validationStatus a {@link ValidationStatus} object.
     */
    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }
}








