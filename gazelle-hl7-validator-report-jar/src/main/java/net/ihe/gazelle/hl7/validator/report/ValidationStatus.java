package net.ihe.gazelle.hl7.validator.report;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * <p>ValidationStatus class.</p>
 *
 * @author abe
 * @version 1.0: 19/04/19
 */
@XmlEnum
public enum ValidationStatus {
    @XmlEnumValue("ABORTED")
    ABORTED(1, "ABORTED"),
    @XmlEnumValue("INVALID REQUEST")
    INVALID_REQUEST(2, "INVALID REQUEST"),
    @XmlEnumValue("FAILED")
    FAILED(3, "FAILED"),
    @XmlEnumValue("PASSED")
    PASSED(4, "PASSED");

    private int level;
    private String status;

    ValidationStatus(int i, String status) {
        this.level = i;
        this.status = status;
    }

    public int getLevel() {
        return this.level;
    }

    public String getStatus() {
        return this.status;
    }

}
