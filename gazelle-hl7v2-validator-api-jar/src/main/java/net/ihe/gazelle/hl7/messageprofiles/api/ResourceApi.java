package net.ihe.gazelle.hl7.messageprofiles.api;

/**
 * <p>ResourceApi class.</p>
 *
 * @author abe
 * @version 1.0: 20/08/2019
 */

public interface ResourceApi {
    byte[] getContent();

    String getOid();

    Integer getWeight();

    String getRevision();
}
