package net.ihe.gazelle.hl7.validator.api;

import net.ihe.gazelle.hl7.messageprofiles.api.ProfileApi;
import net.ihe.gazelle.hl7.validation.context.ValidationContext;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;

/**
 * <p>GenericHL7Validator class.</p>
 *
 * @author abe
 * @version 1.0: 09/07/19
 */

public abstract class GenericHL7Validator {

    private ProfileApi selectedProfile;
    private ValidationContext validationContext;
    private String messageToValidate;

    public GenericHL7Validator(){

    }

    public GenericHL7Validator(ProfileApi profile, ValidationContext validationContext, String message){
        this.selectedProfile = profile;
        this.validationContext = validationContext;
        this.messageToValidate = message;
    }

    public abstract void validateMessage(HL7v2ValidationReport validationReport);


    public ProfileApi getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(ProfileApi selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    public ValidationContext getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(ValidationContext validationContext) {
        this.validationContext = validationContext;
    }

    public String getMessageToValidate() {
        return messageToValidate;
    }

    public void setMessageToValidate(String messageToValidate) {
        this.messageToValidate = messageToValidate;
    }

}
