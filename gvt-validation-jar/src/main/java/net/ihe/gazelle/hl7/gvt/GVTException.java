package net.ihe.gazelle.hl7.gvt;

public class GVTException extends Exception {
    public GVTException(String errorMessage) {super(errorMessage);}
}
