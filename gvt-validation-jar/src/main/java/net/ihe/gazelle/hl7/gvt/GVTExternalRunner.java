package net.ihe.gazelle.hl7.gvt;


import gov.nist.validation.report.Report;
import hl7.v2.profile.BindingStrength;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.jboss.seam.contexts.Contexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <p>GVTExternalRunner class.</p>
 *
 * @author abe
 * @version 1.0: 21/08/2019
 */

public final class GVTExternalRunner {

    private static Logger log = LoggerFactory.getLogger(GVTExternalRunner.class);

    // -profileDirectory declares the path to the XML file which represents the validator
    private static final String OPT_PROFILE_DIRECTORY = "profileDirectory";
    // -message declares the file path for reading the message
    private static final String OPT_MESSAGE = "message";
    // -help print the help
    private static final String OPT_HELP = "help";


    private static boolean helpPrinted = false;
    private static String profileDirectory;
    private static String messagePath;

    private GVTExternalRunner() {
    }

    public static void main(String[] args) {
        Options optionsFromCommandLine = getOptions();
        // Get parameters from the command line
        try {
            getParameters(args, optionsFromCommandLine);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            printHelp(optionsFromCommandLine, e);
        }
        // When parameters are OK: execute the validation and print the report on standard output
        if (!helpPrinted) {

            // 1. read the HL7 message from the hl7MessagePath
            String hl7message = null;
            try {
                hl7message = getMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // 2. instantiate GVTValidator class
            GVTValidator validator = new GVTValidator(profileDirectory, hl7message);

            // 3. print report to standard output (System.out.println, log.info)

            HL7v2ValidationReport reporter = new HL7v2ValidationReport();

            validator.validateMessage(reporter);

            System.out.println(reporter.toString(null));
        }
    }


    private static String getMessage() throws Exception {
        return new String(Files.readAllBytes(Paths.get(messagePath)), StandardCharsets.UTF_8);
    }

    private static Options getOptions() {
        Options commandLineOptions = new Options();
        commandLineOptions.addOption(new Option(OPT_HELP, "print this message"));
        Option validator = new Option(OPT_PROFILE_DIRECTORY, true,
                "path to the directory where the IGAMT files are stored for this profile");
        validator.setRequired(true);
        commandLineOptions.addOption(validator);
        Option output = new Option(OPT_MESSAGE, true, "path to the file where the message shall be read");
        output.setRequired(false);
        commandLineOptions.addOption(output);
        return commandLineOptions;
    }

    private static void getParameters(String[] args, Options commandLineOptions) throws Exception {
        CommandLineParser parser = new PosixParser();

        // parse the command line arguments
        CommandLine line = null;
        try {
            line = parser.parse(commandLineOptions, args);
        } catch (ParseException e) {
            throw e;
        }
        if (line.hasOption(OPT_HELP)) {
            printHelp(commandLineOptions, null);
            helpPrinted = true;
        } else {
            if (line.hasOption(OPT_PROFILE_DIRECTORY)) {
                profileDirectory = line.getOptionValue(OPT_PROFILE_DIRECTORY);
            } else {
                throw new Exception(OPT_PROFILE_DIRECTORY + " is a required parameter");
            }
            if (line.hasOption(OPT_MESSAGE)) {
                messagePath = line.getOptionValue(OPT_MESSAGE);
            } else {
                throw new Exception(OPT_MESSAGE + " is a required parameter");
            }
        }
    }

    private static void printHelp(Options options, Exception e) {
        // automatically generate the help statement
        HelpFormatter formatter = new HelpFormatter();
        if (e != null) {
            log.error(e.getMessage());
        }
        if (!helpPrinted) {
            helpPrinted = true;
            formatter.printHelp("java -jar gvt-validation-jar-jar-with-dependencies.jar "
                    + "[-profileDirectory pathToIGAMTFiles -message pathToMessage", options);
        }
    }
}
