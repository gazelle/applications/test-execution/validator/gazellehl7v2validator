package net.ihe.gazelle.hl7.gvt;

import gov.nist.validation.report.Report;
import hl7.v2.profile.Profile;
import hl7.v2.profile.XMLDeserializer;
import hl7.v2.validation.FeatureFlags;
import hl7.v2.validation.SyncHL7Validator;
import hl7.v2.validation.coconstraints.CoConstraintValidationContext;
import hl7.v2.validation.coconstraints.DefaultCoConstraintValidationContext;
import hl7.v2.validation.content.ConformanceContext;
import hl7.v2.validation.content.DefaultConformanceContext;
import hl7.v2.validation.slicing.DefaultProfileSlicingContext;
import hl7.v2.validation.slicing.ProfileSlicingContext;
import hl7.v2.validation.vs.*;
import net.ihe.gazelle.hl7.messageprofiles.api.ProfileApi;
import net.ihe.gazelle.hl7.validation.context.ValidationContext;
import net.ihe.gazelle.hl7.validator.api.GenericHL7Validator;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResultsOverview;
import net.ihe.gazelle.hl7.validator.report.ValidationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;

public class GVTValidator extends GenericHL7Validator {

    private static final Logger LOG = LoggerFactory.getLogger(GVTValidator.class);
    private InputStream constraintsXML;
    private InputStream vsLibraryXML;
    private InputStream profileXML;
    private String profileDirectoryPath;
    private InputStream vsSpecificationsXML;
    private InputStream coConstraintsXML;
    private InputStream slicingsXML;
    private String id;
    private Profile profile;


    public GVTValidator(ProfileApi profile, ValidationContext validationContext, String message) {
        super(profile, validationContext, message);
    }

    public GVTValidator(String profileDirectoryPath, String message) {
        super(null, null, message);
        this.profileDirectoryPath = profileDirectoryPath;
    }

    /**
     * Perform validation of current message and calculate report without persisting.
     */
    @Override
    public void validateMessage(HL7v2ValidationReport reporter) {

        GVTtoGazelleValidationReportMapper gvTtoGazelleValidationReportMapper = new GVTtoGazelleValidationReportMapper();

        try {
            prepareInputsForValidators(reporter);
            Report gvtReport = validate();
            gvTtoGazelleValidationReportMapper.convertGVTReportToGazelleReport(gvtReport, reporter);
        } catch (Exception e) {
            if (reporter.getOverview() == null) {
                reporter.setOverview(new ValidationResultsOverview());
            }
            if (reporter.getOverview().getValidationAbortedReason() == null || reporter.getOverview().getValidationAbortedReason().isEmpty()) {
                reporter.getOverview().setValidationStatus(ValidationStatus.ABORTED);
                reporter.getOverview().setValidationAbortedReason(e.getMessage());
            }
        }

    }

    /*
     * creating InputStream based on its path
     */
    private void prepareInputsForValidators(HL7v2ValidationReport reporter) {
        try {
            setProfile(profileDirectoryPath.concat("/Profile.xml"));
        } catch (FileNotFoundException e) {
            reporter.getOverview().setValidationStatus(ValidationStatus.ABORTED);
            reporter.getOverview().setValidationAbortedReason(reporter.getOverview().getValidationAbortedReason() + " No Profile.xml File found");
        }
        try {
            setConstraints(profileDirectoryPath.concat("/Constraints.xml"));
        } catch (FileNotFoundException e) {
            reporter.getOverview().setValidationStatus(ValidationStatus.ABORTED);
            reporter.getOverview().setValidationAbortedReason(reporter.getOverview().getValidationAbortedReason() + " No Constraints.xml File found ");
        }
        try {
            setLibrary(profileDirectoryPath.concat("/ValueSets.xml"));
        } catch (FileNotFoundException e) {
            reporter.getOverview().setValidationStatus(ValidationStatus.ABORTED);
            reporter.getOverview().setValidationAbortedReason(reporter.getOverview().getValidationAbortedReason() + " No ValueSets.xml File found ");
        }
        try {
            setSpecifications(profileDirectoryPath.concat("/ValueSetBindings.xml"));
        } catch (FileNotFoundException e) {
            LOG.warn("ValueSetBindings.xml file is not found");
        }
        try {
            setCoConstraints(profileDirectoryPath.concat("/CoConstraints.xml"));
        } catch (FileNotFoundException e) {
            LOG.warn("CoConstraints.xml file is not found");
        }
        try {
            setSlicings(profileDirectoryPath.concat("/Slicings.xml"));
        } catch (FileNotFoundException e) {
            LOG.warn("Slicings.xml file is not found");
        }


    }

    /**
     * Setter of profile
     *
     * @param profilePath
     * @throws FileNotFoundException
     */
    private void setProfile(String profilePath) throws FileNotFoundException {
        File fp = new File(profilePath);
        this.profileXML = new FileInputStream(fp);
    }

    /**
     * Setter of Values Sets
     *
     * @param valueSetPath
     * @throws FileNotFoundException
     */
    private void setLibrary(String valueSetPath) throws FileNotFoundException {
        File fl = new File(valueSetPath);
        this.vsLibraryXML = new FileInputStream(fl);
    }

    /**
     * Setter specification
     *
     * @param valueSetBindingsPath
     * @throws FileNotFoundException
     */
    private void setSpecifications(String valueSetBindingsPath) throws FileNotFoundException {
        File fl = new File(valueSetBindingsPath);
        this.vsSpecificationsXML = new FileInputStream(fl);
    }

    /**
     * Setter contraints
     *
     * @param constraintsPath
     * @throws FileNotFoundException
     */
    private void setConstraints(String constraintsPath) throws FileNotFoundException {
        File fc = new File(constraintsPath);
        this.constraintsXML = new FileInputStream(fc);
    }

    /**
     * Setter co-constraints
     *
     * @param coConstraintsPath
     * @throws FileNotFoundException
     */
    private void setCoConstraints(String coConstraintsPath) throws FileNotFoundException {
        File fc = new File(coConstraintsPath);
        this.coConstraintsXML = new FileInputStream(fc);
    }

    /**
     * Setter slicings
     *
     * @param slicingsPath
     * @throws FileNotFoundException
     */
    private void setSlicings(String slicingsPath) throws FileNotFoundException {
        File fc = new File(slicingsPath);
        this.slicingsXML = new FileInputStream(fc);
    }

    /**
     * Perform validation
     *
     * @return
     * @throws Exception
     */
    protected Report validate() throws Exception {
        profile = getProfile();
        ConformanceContext constraints = getConformanceContext();
        ValueSetLibrary vsLibrary = getValueSetLibrary();
        ValueSetSpecification vsSpecification = getValueSetSpecifications();
        CoConstraintValidationContext coConstraints = getCoConstraintValidationContext();
        ProfileSlicingContext slicings = getSlicings();
        FeatureFlags featureFlags = new FeatureFlags(false); // false by default


        this.id = getID(profile);

        if (vsSpecification == null && coConstraints == null) {
            return new SyncHL7Validator(profile, vsLibrary, constraints).check(getMessageToValidate(), id);
        } else if (vsSpecification == null) {
            return new SyncHL7Validator(profile, vsLibrary, constraints, coConstraints).check(getMessageToValidate(), id);
        } else if (coConstraints == null) {
            return new SyncHL7Validator(profile, vsLibrary, constraints, vsSpecification).check(getMessageToValidate(), id);
        } else {
            return new SyncHL7Validator(profile, vsLibrary, constraints, vsSpecification, coConstraints, slicings, featureFlags).check(getMessageToValidate(), id);
        }
    }

    /**
     * load the profile
     *
     * @return
     */
    private Profile getProfile() throws GVTException {
        Profile profileTemp = null;
        if (this.profileXML != null) {
            try {
                profileTemp = XMLDeserializer.deserialize(this.profileXML).get();
            } catch (Error e) {
                throw new GVTException("Error with Profile.xml: " + e.getMessage());
            }
        }
        return profileTemp;

    }

    /**
     * Load the context of a profile
     *
     * @return
     */
    private ConformanceContext getConformanceContext() throws GVTException {
        ConformanceContext cc = null;
        if (this.constraintsXML != null) {
            try {
                cc = DefaultConformanceContext.apply(Arrays.asList(this.constraintsXML)).get();
            } catch (Error e) {
                throw new GVTException("Error with Constraints.xml: " + e.getMessage());
            }
        }
        return cc;
    }

    /**
     * Load the value set of the profile
     *
     * @return
     */
    private ValueSetLibrary getValueSetLibrary() throws GVTException {
        ValueSetLibrary vsl = null;
        if (this.vsLibraryXML != null) {
            try {
                vsl = ValueSetLibraryImpl.apply(this.vsLibraryXML).get();
            } catch (Error e) {
                throw new GVTException("Error with ValueSets.xml: " + e.getMessage());
            }
        }
        return vsl;
    }

    private ValueSetSpecification getValueSetSpecifications() throws GVTException {
        ValueSetSpecification vss = null;
        if (this.vsSpecificationsXML != null) {
            try {
                vss = DefaultValueSetSpecification.apply(this.vsSpecificationsXML).get();
            } catch (Error e) {
                throw new GVTException("Error with ValueSetBindings.xml: " + e.getMessage());
            }
        }
        return vss;
    }

    private CoConstraintValidationContext getCoConstraintValidationContext() throws GVTException {
        CoConstraintValidationContext cco = null;
        if (this.coConstraintsXML != null) {
            try {
                cco = DefaultCoConstraintValidationContext.apply(this.coConstraintsXML).get();
            } catch (Error e) {
                throw new GVTException("Error with CoConstraints.xml: " + e.getMessage());
            }
        }
        return cco;
    }

    private ProfileSlicingContext getSlicings() throws GVTException {
        ProfileSlicingContext psc = null;
        if (this.slicingsXML != null && this.profileXML != null) {
            try {
                psc = DefaultProfileSlicingContext.apply(this.slicingsXML, profile).get();
            } catch (Error e) {
                throw new GVTException("Error with Slicings.xml: " + e.getMessage());
            }
        }
        return psc;
    }

    /**
     * return the id of a profile
     *
     * @param profile
     * @return
     */
    private String getID(Profile profile) {
        return (profile != null ? profile.messages().mkString().substring(0, 24) : "");
    }
}
