package net.ihe.gazelle.hl7.gvt;

import gov.nist.validation.report.Entry;
import gov.nist.validation.report.Report;
import net.ihe.gazelle.hl7.assertion.AssertionType;
import net.ihe.gazelle.hl7.exception.GazelleErrorCode;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResults;

import java.util.List;
import java.util.Map;

public class GVTtoGazelleValidationReportMapper {

    /*
     * Definitions of Constant Values
     */
    private static final String ERROR = "Error";
    private static final String AFFIRMATIVE = "Affirmative";
    private static final String ALERT = "Alert";
    private static final String WARNING = "Warning";
    private static final String VALIDATION_NOTES = "Validation Notes";
    private static  final String INFORMATIONAL = "Informational";
    private static  final String CODE_NOT_FOUND = "Code Not Found";

    public GVTtoGazelleValidationReportMapper() {
        //Empty constructor
    }

    /**
     * Convert GVT report to Gazelle report
     *
     * @param reportGVT
     * @param hl7Report
     */
    public void convertGVTReportToGazelleReport(Report reportGVT, HL7v2ValidationReport hl7Report) {
        ValidationResults vr = hl7Report.getResults();

        Map<String, List<Entry>> map = reportGVT.getEntries();

        for (Map.Entry mapentry : map.entrySet()) {

            List<Entry> list = (List<Entry>) mapentry.getValue();

            for (Entry entry : list) {
                addValidationConstraint(entry, vr);
            }

        }
        hl7Report.setResults(vr);
        setReportStatus(hl7Report);
    }

    /**
     * Add Validation constraint
     *
     * @param entry
     * @param vr
     */
    private void addValidationConstraint(Entry entry, ValidationResults vr) {
        String entryClassification = entry.getClassification();
        String entryCategory = entry.getCategory();

        switch (entryClassification){
            case ERROR:
                addError(vr, entry);
                break;
            case ALERT:
                if (CODE_NOT_FOUND.equals(entryCategory)){
                    addWarning(vr, entry);
                } else {
                    addAssertion(vr, entry);
                }
                break;
            case WARNING:
                addWarning(vr,entry);
                break;
            case AFFIRMATIVE:
            case VALIDATION_NOTES:
            case INFORMATIONAL:
                addAssertion(vr,entry);
                break;
            default: //Do Nothing
        }
    }

    /**
     * Add a Warning to report
     *
     * @param vr
     * @param entry
     */
    private void addWarning(ValidationResults vr, Entry entry) {
        vr.addWarning(entry.getDescription(), getCodeIgamt(entry.getCategory()), entry.getPath());
    }

    /**
     * Add error to report
     *
     * @param vr
     * @param entry
     */
    private void addError(ValidationResults vr, Entry entry) {
        vr.addError(entry.getDescription(), getCodeIgamt(entry.getCategory()), entry.getPath());
    }

    /**
     * Add Profile Exception to report
     *
     * @param vr
     * @param entry
     */
    private void addProfileException(ValidationResults vr, Entry entry) {
        vr.addProfileException(entry.getDescription(), entry.getClassification(), getCodeIgamt(entry.getCategory()), entry.getPath());
    }

    /**
     * add assertion to report
     *
     * @param vr
     * @param entry
     */
    private void addAssertion(ValidationResults vr, Entry entry) {

        if ("PVS".equals(entry.getCategory())) {
            String temp = "PERMITTED_ELEMENT";
            AssertionType type = AssertionType.valueOf(temp);
            vr.addAssertion(entry.getDescription(), entry.getPath(), type);
        } else {
            String temp = getAssertion(entry.getCategory());
            AssertionType type = AssertionType.valueOf(temp);
            vr.addAssertion(entry.getDescription(), entry.getPath(), type);
        }
    }

    /**
     * update report
     *
     * @param report
     */
    private void setReportStatus(HL7v2ValidationReport report) {
        report.computeResult();
    }

    /*
     * Compare the category of an affirmative notification with the enumeration AssertionType to
     * return a string corresponding to the label to create an AssertionType to create the correct GazelleHL7Assertion
     */
    private String getAssertion(String typeAssertion) {

        for (AssertionType type : AssertionType.values()) {
            if (type.getLabel().equalsIgnoreCase(typeAssertion)) {
                String result = typeAssertion.toUpperCase();
                result = result.replace(' ', '_');
                result = result.replace('-', '_');
                return result;
            }
        }
        //Default type
        return "DEFAULT";
    }

    /*
     * Compare the category of the notification in the report of gvt with the enumeration GazelleErrorCode to
     * return the code used in the creation of a warning
     */
    private int getCodeIgamt(String typeError) {

        for (GazelleErrorCode err : GazelleErrorCode.values()) {
            if (err.getMessage().equalsIgnoreCase(typeError)) {
                return err.getCode();
            }
        }
        return 14;
    }
}
