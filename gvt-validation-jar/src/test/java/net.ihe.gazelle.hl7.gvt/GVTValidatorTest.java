package net.ihe.gazelle.hl7.gvt;


import ca.uhn.hl7v2.parser.GenericParser;
import net.ihe.gazelle.hl7.validation.context.ValidationContext;
import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import net.ihe.gazelle.hl7.validator.report.ValidationResults;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class GVTValidatorTest {

    private static final Logger LOG = Logger.getLogger(GVTValidatorTest.class);

    private static final String PATH_TO_MESSAGES_FOLDER = "src/test/resources/messageprofiles/";
    private static final String PATHPROFILE_IRELAND = PATH_TO_MESSAGES_FOLDER + "ADT_A03_ADT_A03/";

    // ----------------------------------------------------------------
    // ILW PATH PROFILE
    // ----------------------------------------------------------------
    private static final String PATHPROFILE_ILW = PATH_TO_MESSAGES_FOLDER + "ILW";
    private static final String PATHPROFILE_ILW_PROFILE_ERROR = PATHPROFILE_ILW + "_profile_error";
    private static final String PATHPROFILE_ILW_VALUE_SET_ERROR = PATHPROFILE_ILW + "_valueSet_error";
    private static final String PATHPROFILE_ILW_CONSTRAINTS_ERROR = PATHPROFILE_ILW + "_constraints_error";
    private static final String PATHPROFILE_ILW_COCONSTRAINTS_ERROR = PATHPROFILE_ILW + "_coconstraints_error";
    private static final String PATHPROFILE_ILW_VSB_ERROR = PATHPROFILE_ILW + "_valueSetBindings_error";
    private static final String PATHPROFILE_ILW_SLICINGS_ERROR = PATHPROFILE_ILW + "_slicings_error";
    private static final String PATH_TO_ILW_OK_MESSAGE = "src/test/resources/messages_examples/message_ilw.txt";

    // ----------------------------------------------------------------
    // CISIS PATH PROFILE
    // ----------------------------------------------------------------
    private static final String PATHPROFILE_CISIS = PATH_TO_MESSAGES_FOLDER + "ZAM^Z01^ZAM_Z01";
    private static final String PATHPROFILE_CISIS_ALERT_CNF = PATH_TO_MESSAGES_FOLDER + "ZAM^Z01_AlertCNF";
    private static final String PATHPROFILE_CISIS_NO_COCONSTRAINT = PATH_TO_MESSAGES_FOLDER + "ZAM^Z01^ZAM_Z01_no_coconstraint";
    private static final String PATHPROFILE_CISIS_NO_VALUE_SET_BINDINGS = PATH_TO_MESSAGES_FOLDER + "ZAM^Z01^ZAM_Z01_no_vsBinding";
    private static final String PATHPROFILE_CISIS_NO_COCONSTRAINT_NO_VALUE_SET_BINDINGS = PATH_TO_MESSAGES_FOLDER + "ZAM^Z01^ZAM_Z01_no_cco_no_vsb";
    private static final String PATH_TO_ZAM_OK_MESSAGE = "src/test/resources/messages_examples/message_zam.txt";
    private static final String PATH_TO_ZAM_NO_CODE_ERROR_MESSAGE = "src/test/resources/messages_examples/message_zam_no_code_error.txt";
    private static final String PATH_TO_ZAM_NO_CODE_WARNING_MESSAGE = "src/test/resources/messages_examples/message_zam_no_code_warning.txt";


    // ----------------------------------------------------------------
    // VALIDATION STATUS
    // ----------------------------------------------------------------

    private static final String PASSED_STATUS = "PASSED";
    private static final String ABORTED_STATUS = "ABORTED";
    private static final String FAILED_STATUS = "FAILED";

    // ----------------------------------------------

    private static final String xmLValidationContext = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?xml-stylesheet " +
            "type=\"text/xsl\" href=\"https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl\"?>\n" +
            "         <ValidationContext>\n" +
            "         <ProfileOID>1.3.6.1.4.1.12559.11.35.10.1.13</ProfileOID>\n" +
            "         <ValidationOptions>\n" +
            "\t         \t<MessageStructure>ERROR</MessageStructure>\n" +
            "\t         \t<Length>WARNING</Length>\n" +
            "\t         \t<DataType>ERROR</DataType>\n" +
            "\t         \t<DataValue>WARNING</DataValue>\n" +
            "         \t</ValidationOptions>\n" +
            "         \t<CharacterEncoding>UTF-8</CharacterEncoding>\n" +
            "         \t</ValidationContext>";

    private static final String xlmMESSAGECORRECT = "MSH|^~\\&|TOREX.HEALTHLINK.12|MATER MISERICORDIAE HOSPITAL^908^L||Smyth, Michael^987654^Medical" +
            " Council No|200805281125||ADT^A03|1633796|P|2.4\n" +
            "EVN||200805281125\n" +
            "PID|||0880289^^^MATER MISERICORDIAE HOSPITAL^MRN||Test Family Name^Test First Name^^^MR^^L||19000101|M|||Test " +
            "Address^SMITHFIELD^DUBLIN 7\n" +
            "PV1||I|||||||2820^KELL^MALCOLM^^^MR.|||||7||||||||||||||||||||||01|0|||||||200805260607|200805281125||||||A";

    //  ----------------------------------------------------------------
    // UTs for GVTValidator
    // -----------------------------------------------------------------

    @Test
    public void testValidateMessage() {
        try {
            String messageToValidate = "MSH|^~\\&|PAMSimulator|IHE|SYNEDRA_AIM|PACS_SYNEDRA|20200306141649||ADT^A01^ADT_A01|20200306141649|P|2" +
                    ".5||||||UNICODE UTF-8\n" +
                    "EVN||20200306141649||||20200306141648\n" +
                    "PID|||DDS-66428^^^DDS&1.3.6.1.4.1.12559.11.1.4.1" +
                    ".2&ISO^PI||Bauer^Angelica^^^^^L|Schmid^^^^^^M|19530602003435|F|||^^^^^AUT||||||PRO|AN9317^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2" +
                    ".5&ISO^AN|||||||||||||N\n" +
                    "PV1||I|2W^109^3^GERMAN_HOSPITAL|L|||6303^LOPEZ^GABRIELLA^^^DR|7505^RA^KYU^^^DR||C|||||||5101^NELL^FREDERICK^P^^DR||VN11522" +
                    "^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN|||||||||||||||||||||||||20200306141600|||||||V";

            String messageTovalidateTrue = "MSH|^~\\&|PAMSimulator|IHE|SYNEDRA_AIM|PACS_SYNEDRA|20200306141649||ADT^A01^ADT_A01|20200306141649|P|2" +
                    ".5||||||UNICODE UTF-8\r" +
                    "EVN||20200306141649||||20200306141648\r" +
                    "PID|||DDS-66428^^^DDS&1.3.6.1.4.1.12559.11.1.4.1" +
                    ".2&ISO^PI||Bauer^Angelica^^^^^L|Schmid^^^^^^M|19530602003435|F|||^^^^^AUT||||||PRO|AN9317^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2" +
                    ".5&ISO^AN|||||||||||||N\r" +
                    "PV1||I|2W^109^3^GERMAN_HOSPITAL|L|||6303^LOPEZ^GABRIELLA^^^DR|7505^RA^KYU^^^DR||C|||||||5101^NELL^FREDERICK^P^^DR||VN11522" +
                    "^^^IHEPAM&1.3.6.1.4.1.12559.11.1.2.2.5&ISO^VN|||||||||||||||||||||||||20200306141600|||||||V";
            String message = messageToValidate.replace('\n', '\r');
            // make sure we are using ER7 encoding
            GenericParser parser = GenericParser.getInstanceWithNoValidation();
            parser.setPipeParserAsPrimary();
            String encoding = GenericParser.getInstanceWithNoValidation().getEncoding(message);
            System.out.println(encoding);
            Assert.assertEquals(message, messageTovalidateTrue);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void TestCreatFile() {
        try {
            File temp = new File("Empty");

            try {
                temp = File.createTempFile("tempfile", ".txt");
                String tempFilePath = temp.getAbsolutePath();
                System.out.println(tempFilePath);
                temp.delete();
                Assert.assertNotSame("/tmp/", tempFilePath);
            } catch (Exception e) {
                Assert.fail(e.getMessage());
            }

        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    /*
     * <p> Uncomment the line "TestValidator.prepareInputsForValidators(reporter)" and set prepareInputsForValidators in public </p>
     */
    @Test
    public void TestSetConfiguration() {
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        try {
            GVTValidator TestValidator = new GVTValidator(PATHPROFILE_IRELAND, xlmMESSAGECORRECT);
            ValidationContext validationContext = new ValidationContext();

            validationContext = ValidationContext.createFromString(xmLValidationContext);

            ValidationResults results = new ValidationResults(validationContext.getLength(), validationContext.getDataValue(),
                    validationContext.getDatatype(), validationContext.getMessageStructure());
            reporter.setResults(results);

            reporter.getOverview().setMessageOid("test");
            reporter.getOverview().setValidationServiceVersion("test");

            Assert.assertNull(reporter.getOverview().getValidationAbortedReason());
        } catch (Exception e) {
            Assert.fail(e.getMessage());
            LOG.trace(reporter.getOverview().getValidationAbortedReason());
        }
    }

    // ----------------------------------------------------------------
    // Specific tests for GVT Validations
    // ----------------------------------------------------------------

    public void testGVTValidation(String profilePath, String messageToValidate, String exceptedStatus) {
        //Given
        GVTValidator gvtValidator = new GVTValidator(profilePath, messageToValidate);
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        //When
        gvtValidator.validateMessage(reporter);
        //Then
        Assert.assertEquals(exceptedStatus, reporter.getOverview().getValidationStatus().getStatus());
    }


    @Test
    public void testGVTValidationOkCase() throws Exception {
        String message = getResourceFileAsString(PATH_TO_ZAM_OK_MESSAGE);
        testGVTValidation(PATHPROFILE_CISIS, message, PASSED_STATUS);
    }

    @Test
    public void testGVTValidationNoCodeErrorCase() throws Exception {
        String message = getResourceFileAsString(PATH_TO_ZAM_NO_CODE_ERROR_MESSAGE);
        testGVTValidation(PATHPROFILE_CISIS, message, FAILED_STATUS);
    }

    @Test
    public void testGVTValidationNoCodeAlertToWarningCase() throws Exception {
        //Given
        String message = getResourceFileAsString(PATH_TO_ZAM_NO_CODE_WARNING_MESSAGE);
        GVTValidator gvtValidator = new GVTValidator(PATHPROFILE_CISIS_ALERT_CNF, message);
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        //When
        gvtValidator.validateMessage(reporter);
        //Then
        Assert.assertTrue(reporter.getCounters().getNbOfWarnings() > 0);
        Assert.assertEquals(PASSED_STATUS, reporter.getOverview().getValidationStatus().getStatus());

    }

    @Test
    public void testGVTValidationAbortedCase() throws Exception {
        String message = getResourceFileAsString(PATH_TO_ZAM_OK_MESSAGE);
        testGVTValidation("wrongPath", message, ABORTED_STATUS);
    }

    @Test
    public void testGVTValidationNoCoConstraint() throws Exception {
        String message = getResourceFileAsString(PATH_TO_ZAM_OK_MESSAGE);
        testGVTValidation(PATHPROFILE_CISIS_NO_COCONSTRAINT, message, PASSED_STATUS);
    }

    @Test
    public void testGVTValidationNoValueSetBindings() throws Exception {
        String message = getResourceFileAsString(PATH_TO_ZAM_OK_MESSAGE);
        testGVTValidation(PATHPROFILE_CISIS_NO_VALUE_SET_BINDINGS, message, PASSED_STATUS);
    }

    @Test
    public void testGVTValidationNoCoConstraintNoValueSetBindings() throws Exception {
        //Given
        String message = getResourceFileAsString(PATH_TO_ZAM_OK_MESSAGE);
        GVTValidator gvtValidator = new GVTValidator(PATHPROFILE_CISIS_NO_COCONSTRAINT_NO_VALUE_SET_BINDINGS, message);
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        //When
        gvtValidator.validateMessage(reporter);
        //Then
        Assert.assertEquals(PASSED_STATUS, reporter.getOverview().getValidationStatus().getStatus());
    }

    @Test
    public void testGVTValidationILWIValid() throws Exception {
        //Given
        String message = getResourceFileAsString(PATH_TO_ILW_OK_MESSAGE);
        GVTValidator gvtValidator = new GVTValidator(PATHPROFILE_ILW, message);
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        //When
        gvtValidator.validateMessage(reporter);
        //Then
        Assert.assertEquals(FAILED_STATUS, reporter.getOverview().getValidationStatus().getStatus());
    }

    //General method to test invalid resources error on Scala
    public void testGVTValidationILWResourceError(String profilePath, String resourceOnError) throws IOException {
        //Given
        String message = getResourceFileAsString(PATH_TO_ILW_OK_MESSAGE);
        GVTValidator gvtValidator = new GVTValidator(profilePath, message);
        HL7v2ValidationReport reporter = new HL7v2ValidationReport();
        //When
        gvtValidator.validateMessage(reporter);
        //Then
        Assert.assertTrue(reporter.getOverview().getValidationAbortedReason().contains("Error with " + resourceOnError + ".xml"));
    }

    @Test
    public void testGVTValidationILWIProfileError() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_PROFILE_ERROR, "Profile");
    }

    @Test
    public void testGVTValidationILWIValueSetError() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_VALUE_SET_ERROR, "ValueSets");
    }

    @Test
    public void testGVTValidationILWConstraintsError() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_CONSTRAINTS_ERROR, "Constraints");
    }

    @Test
    public void testGVTValidationILWICoConstraintsError() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_COCONSTRAINTS_ERROR, "CoConstraints");
    }

    @Test
    public void testGVTValidationILWInvalidValueSetBinding() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_VSB_ERROR, "ValueSetBindings");
    }

    @Test
    public void testGVTValidationILWInvalidSlicings() throws Exception {
        testGVTValidationILWResourceError(PATHPROFILE_ILW_SLICINGS_ERROR, "Slicings");
    }


    public String getResourceFileAsString(String filePath) throws IOException {
        File f = new File(filePath);
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        try (InputStream is = Files.newInputStream(f.toPath())) {
            for (int length; (length = is.read(buffer)) != -1; ) {
                result.write(buffer, 0, length);
            }
        }
        return result.toString(StandardCharsets.UTF_8.name());
    }

}
