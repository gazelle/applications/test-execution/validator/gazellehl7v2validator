package net.ihe.gazelle.hl7.gvt;

import net.ihe.gazelle.hl7.validator.report.HL7v2ValidationReport;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class GVTtoGazelleValidationReportMapperTest {

    private final String PATHPROFILE_IRELAND = "src/test/resources/messageprofiles/ADT_A03_ADT_A03/";
    private final String PATHPROFILE_INTEROP = "src/test/resources/messageprofiles/ZAM^Z01_AlertCNF/";

    private final String ER7INCORRECTMESSAGE = "MSH|^~\\&|TOREX.HEALTHLINK.12|MATER MISERICORDIAE HOSPITAL^908^L||Smyth, Michael^987654^Medical" +
            " Council No|200805281125||ADT^A03|1633796|P|2.4\n" +
            "EVN||200805281125\n" +
            "PID|||0880289^^^MATER MISERICORDIAE HOSPITAL^MRN||Test Family Name^Test First Name^^^MR^^L||19000101|M|||Test " +
            "Address^SMITHFIELD^DUBLIN 7\n" +
            "PV1||I|||||||2820^KELL^MALCOLM^^^MR.|||||7||||||||||||||||||||||01|0|||||||200805260607|200805281125||||||A";

    private GVTValidator gvtValidator;

    private final HL7v2ValidationReport hl7v2ValidationReport = new HL7v2ValidationReport();

    @Test
    public void testConvertGVTReportToGazelleReportAbortedWithout_VS() {
        String PATHPROFILE_IRELAND_WITHOUT_VS = "src/test/resources/messageprofiles/ADT_A03_ADT_A03_withoutVS/";
        gvtValidator = new GVTValidator(PATHPROFILE_IRELAND_WITHOUT_VS, ER7INCORRECTMESSAGE);
        gvtValidator.validateMessage(hl7v2ValidationReport);
        Assert.assertEquals("ABORTED", hl7v2ValidationReport.getOverview().getValidationTestResult());
    }

    @Test
    public void testConvertGVTReportToGazelleReportFailed() {
        gvtValidator = new GVTValidator(PATHPROFILE_IRELAND, ER7INCORRECTMESSAGE);
        gvtValidator.validateMessage(hl7v2ValidationReport);
        Assert.assertEquals("FAILED", hl7v2ValidationReport.getOverview().getValidationTestResult());
    }

    @Test
    public void testConvertGVTReportToGazelleReportNoCodeWarningPassed() {
        String ZAMNOCODEMESSAGE = "MSH|^~\\&|PFI-X|Organisation-X|SIL-Y|labo|202106060933||ZAM^Z01^ZAM_Z01|017|P|2.6|||||FRA|UNICODE UTF-8|||2.0^CISIS_CDA_HL7_V2\n" +
                "EVN||20211005152908\n" +
                "OBX|1|CWE|ACK_RECEPTION_DMP^Accusé de réception DMP^AckMétierZAM|015|Z^^HL70136||||||F|";
        gvtValidator = new GVTValidator(PATHPROFILE_INTEROP, ZAMNOCODEMESSAGE);
        gvtValidator.validateMessage(hl7v2ValidationReport);
        Assert.assertEquals("PASSED", hl7v2ValidationReport.getOverview().getValidationTestResult());
    }

    @Test
    public void testConvertGVTReportToGazelleReportPassed() {
        String ER7CORRECTMESSAGE = "MSH|^~\\&|TOREX.HEALTHLINK.12^Discharge notification message^L|908^MATER MISERICORDIAE HOSPITAL^L||6^Care Doc^L|200805281125||ADT^A03^ADT_A03|1633796|P|2.4\n" +
                "EVN||200805281125\n" +
                "PID|||0880289^^^MATER MISERICORDIAE HOSPITAL^MR||Test Family Name^Test First Name^^^MR^^L||19000101|M|||Test Address^SMITHFIELD^DUBLIN 7\n" +
                "PV1||I|||||||2820^KELL^MALCOLM^^^DR|||||7||||||||||||||||||||||01|0|||||||200805260607|200805281125||||||A";
        gvtValidator = new GVTValidator(PATHPROFILE_IRELAND, ER7CORRECTMESSAGE);
        gvtValidator.validateMessage(hl7v2ValidationReport);
        Assert.assertEquals("PASSED", hl7v2ValidationReport.getOverview().getValidationTestResult());
    }
}
